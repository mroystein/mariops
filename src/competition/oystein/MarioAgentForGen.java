/* Some thoughts on the Mario implementation. What is to be gained from training?
 * 1. Should the states be rough and abstract? Yes. How abstract? It should be 
 *    abstract enough that the number of state-action pairs is exceedingly large.
 *    But if it is too abstract, we may lose the approximate Markov property. 
 *    (e.g. if the defined Mario state lacks "Mario's position", then suppose
 *    two original scenes, one with Mario on high platform, the other wiht Mario 
 *    on low platform, and other parameters the same. They have the same abstract
 *    state S. But S x Action A -> undetermined for the two scenes.
 *       With that said, we hope given many trials and a large state space the
 *    effect is not affecting us.
 *  
 * 2. Learning for specific actions (keystrokes) or movement preferences?
 *    Learning for keystrokes seems to be hard, but can be tolerated. Consider we
 *    can first hard-code the preferences, and modify the reward function to "unit
 *    learn" the keystroke combo. For example, we could define first learning unit
 *    to be "advance", and set reward to be large for every step going rightward.
 *    Then we train the "search" unit, etc.
 *      After the units complete, we face the problem that given a scene, what is
 *    the task to carry out. This can be completed using a higher-level QTable, or
 *    simply estimate the reward given by carrying out each task, and pick the
 *    best-rewarded.
 *        I think the latter approach is easier, but possibly contain bugs. Let's see
 *    whether is will become a problem.
 * 
 * 3. How to let Mario advance?
 *    -given a scene, abstract to Mario state
 *    -construct a QTable AdvanceTable, containing State, Action pairs
 *    -each Action is a combination of keystrokes
 *    -the MDP is also learned, not predetermined?
 *    -the reward function: the number of steps rightward taken
 *    -possible problem: how to let Mario jump through gaps, platforms and enemies?
 *        -jump until necessary? could give negative rewards for unnecessary jumps
 *    -the Mario state should contain "complete" information about the scene
 *        -idea: "poles", where the Mario should be jumping off and how far?
 * 
 * */

package competition.oystein;

import ch.idsia.agents.Agent;
import ch.idsia.agents.LearningAgent;
import ch.idsia.benchmark.mario.engine.sprites.Mario;
import ch.idsia.benchmark.mario.environments.Environment;
import ch.idsia.benchmark.tasks.BasicTask;
import ch.idsia.benchmark.tasks.LearningTask;
import ch.idsia.benchmark.tasks.MarioCustomSystemOfValues;
import ch.idsia.tools.EvaluationInfo;
import ch.idsia.tools.MarioAIOptions;
import competition.oystein.controller.MarioAction;
import competition.oystein.controller.MyHumanAgentDebug;
import competition.oystein.controller.helpers.MyUtils;
import competition.oystein.controller.helpers.Utils;
import competition.oystein.controller.states.*;
import competition.oystein.controller.states.IState.RewardMethod;
import competition.oystein.gui.SimpleSwingGraphGUI;
import competition.oystein.ps.PSMario;
import competition.oystein.ps.PSMarioExtenable;
import competition.oystein.ps.v3.PS_Generalization;
import competition.oystein.ps.v3.PS_GeneralizationWActionComp;
import competition.reinforcment.ILearningAlgorithm;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


public class MarioAgentForGen implements LearningAgent {

    public static int[] successSeeds = {-390653533,-2131073684,1796097696,1132148668,587584254,-2029651743,867463949};
    public static int[] failSeeds = {-1070788919,};

    public static Random random = new Random(System.currentTimeMillis());

    public static boolean PRINT_EVAL_INFO = true;
    public static PlayMode playMode = PlayMode.TRAIN;

    private String name;

    // Training options, task and quota.
    private MarioAIOptions options;
    private LearningTask learningTask;
//    private final PSMario ps;
    PSGenState currentState;

//    private ILearningAlgorithm learner;
//    PS_Generalization ps;
    PS_GeneralizationWActionComp ps;

    // Fields for the Mario Agent


    // The type of phase the Agent is in.
    // INIT: initial phase
    // LEARN: accumulatively update the Qtable
    private enum Phase {
        INIT, LEARN, EVAL
    }


    private Phase currentPhase = Phase.INIT;

    private int learningTrial = 0;


//    public PSMario getPs() {
//        return ps;
//    }
    public ILearningAlgorithm getLearnger() {
        return null;
    }

    public MarioAgentForGen() {
        setName("PS agent");
//        currentState = new State(false);
//         currentState = new PSGenStateSmall(RewardMethod.PS_REWARD);
        currentState = new PSGenState(RewardMethod.PS_REWARD);
        resetPSGEN();

    }

    public void resetPSGEN(){

        MarioAction[] values =    MarioAction.BASIC_ACTIONS_MARIO;//MarioAction.values();
        int[][] actions = new int[values.length][6];
        for (int i = 0; i < actions.length; i++) {
            for (int j = 0; j < actions[i].length; j++) {
                actions[i][j]=values[i].getAction()[j]?1:0;
            }
        }
//        ps = new PS_Generalization(0.0005,actions,5);
        ps = new PS_GeneralizationWActionComp(0.0005,actions,5);
    }

    @Override
    public String toString() {
        return ps.toString()+ " " + currentState.getName();
//        return ps.toString() + " " + currentState.getName();
    }

    public MarioAgentForGen(PSMario ps, RewardMethod rewardMethod) {
//        setName("PS agent");
//        currentState = new DynamicState(rewardMethod);
//        this.learner = ps;
    }

    public IState getCurrentState() {
        return currentState;
    }

    public MarioAgentForGen(IState stateClass, double gamma, double damperGlow) {
        this(stateClass, gamma, -1.0, damperGlow);
    }

    public MarioAgentForGen(IState stateClass, double gamma, double reward, double damperGlow) {
//        setName("PS agent");
//        currentState = stateClass;
//        this.learner = new PSMario(stateClass.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, gamma, reward, 1, damperGlow);
    }


    public MarioAgentForGen(PSMario ps, boolean simple) {
        setName("PS agent");
//
//        if (simple)
//            currentState = new State(false);
//        else currentState = new DynamicState();
//
//        this.learner = ps;
    }

    public MarioAgentForGen(ILearningAlgorithm learner, IState state) {
//        setName("PS agent");
//        currentState = state;
//        this.learner = learner;
    }
//    public PSMario getPs(){
//        return (PSMario) learner;
//    }

    String currStateStr, currActionName;
    int currActionNumber;

    @Override
    public boolean[] getAction() {
        // Transforms the best action number to action array.

//        int actionNumber = ps.getAction(currentState.getStateNumber());
//        int actionNumber =  learner.getAction(currentState.getStateNumber());
       int actionInt[] = ps.getAction(currentState.getState());

        boolean[] actionBool = new boolean[6];
        for (int i = 0; i < actionInt.length; i++) {
            actionBool[i]= actionInt[i] == 1;
        }

//        currStateStr = currentState.toString();
//        currActionName = MarioAction.getName(actionNumber);
//        currActionNumber = actionNumber;
//        System.out.println(s + "\t -> \t" + actionName);
//        double[] actions = ps.h[currentState.getStateNumber()];

        return actionBool;
    }

    /**
     * Importance of this function: the scene observation is THE RESULT after
     * performing some action given the previous state. Therefore we could get
     * information on:
     * 1. prev state x prev action -> current state.
     * 2. get the reward for prev state, prev action pair.
     * <p>
     * The reward function, however, is not provided and has to be customized.
     */
    @Override
    public void integrateObservation(Environment environment) {
        // Update the current state.
        currentState.update(environment);


        if (currentPhase == Phase.INIT && environment.isMarioOnGround()) {
            // Start learning after Mario lands on the ground.
//            ps.giveRewardAmount(0.0);
//            learner.update(0.0,currentState.getStateNumber());
            ps.giveReward(0);
            currentPhase = Phase.LEARN;
        } else if (currentPhase == Phase.LEARN) {
            // Update the Qvalue entry in the Qtable.


            double reward = currentState.calcReward();
            ps.giveReward(reward);
//            learner.update(currentState.calcReward(),currentState.getStateNumber());
//            ps.giveRewardAmount(reward);


        } else {
            ps.giveReward(0);
//            learner.update(0.0,currentState.getStateNumber());
//            ps.giveRewardAmount(0.0);
        }
    }

    ArrayList<Integer> timesUsed = new ArrayList<>();
    ArrayList<Integer> distances = new ArrayList<>();

    private void learnOnce() {

        if (PRINT_EVAL_INFO) {
            System.out.println("================================================");
            System.out.println("Trial: " + learningTrial);
        }

//        init();

        learningTask.runSingleEpisode(1);
//        learningTask.

        EvaluationInfo evaluationInfo =
                learningTask.getEnvironment().getEvaluationInfo();

        distances.add(evaluationInfo.distancePassedCells);
        if (evaluationInfo.timeSpent == 200 && PRINT_EVAL_INFO) {
            System.out.println("Ran out of time");
        }
        int timeUsed = evaluationInfo.timeSpent;

        int score = evaluationInfo.computeWeightedFitness();
        boolean died = evaluationInfo.marioStatus == Mario.STATUS_DEAD;

        if (died) {
            timeUsed*=-1;
//            timeUsed=201;
//            if (evaluationInfo.timeSpent == 200) timesUsed.add(201);
//            else timeUsed *= -1;
        }

        timesUsed.add(timeUsed);
        if (PRINT_EVAL_INFO) {
            System.out.println("Intermediate SCORE = " + score + " TimeUsed:" + timeUsed + " Distance:" + evaluationInfo.distancePassedPhys);
            System.out.println(evaluationInfo.toStringSingleLine());
        }
//        System.out.println(timesUsed.size()+" "+distances.size());
//        System.out.println(evaluationInfo.distancePassedCells);

        learningTrial++;
    }

    public void learnAndStopIfFailing(int epochs) {
        for (int i = 0; i < epochs; i++) {
            learnOnce();

            if (i > 3) {
                if (timesUsed.get(i - 3) >= 200 && timesUsed.get(i - 2) >= 200 && timesUsed.get(i - 1) >= 200 && timesUsed.get(i) >= 200) {
                    System.out.println("\tStopped training since the 2 last " + i + " and " + (i - 1) + " was ran out of time");
                    return;
                }
            }
        }
    }

    public boolean learnUntilSuccess(int maxEpoch){
        int nGamesNeedsToPass=20;
        for (int i = 0; i < maxEpoch; i++) {
            learnOnce();

            if(i>nGamesNeedsToPass) {
               double avg= distances.subList(distances.size() - nGamesNeedsToPass, distances.size()).stream().mapToInt(x -> x).average().getAsDouble();

                if(avg>=256-1) {
                    System.out.println(" Stopped at "+i+" due to "+nGamesNeedsToPass+"  successfull sequential runs.");
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void learn() {
        // for (int m = 0; m < 3; m++) {
        //options.setMarioMode(m);

        for (int i = 0; i < 1; i++) {
            learnOnce();
        }

        //}

    }

    public void learn(int epochs) {

        for (int i = 0; i < epochs; i++) {
            learnOnce();
            ps.countEdges();
        }

    }


    @Override
    public void init() {

    }

    @Override
    public void reset() {
        //dont used. gets called by Mario Game Engine
//            resetAgentForNewSameAgeent();
        //currentState = new S tate();
    }

    public void resetAgentForNewSameAgeent() {
//        learner.reset();
//        ps.reset();
        resetPSGEN();
        timesUsed.clear();
        distances.clear();
    }


    public void setOptions(MarioAIOptions options) {
        this.options = options;
    }

    /**
     * Gives access to the evaluator through learningTask.evaluate(Agent).
     */
    @Override
    public void setLearningTask(LearningTask learningTask) {
        this.learningTask = learningTask;
    }

    @Deprecated
    @Override
    public void setEvaluationQuota(long num) {
    }

    @Deprecated
    @Override
    public Agent getBestAgent() {
        return this;
    }

    @Override
    public void setObservationDetails(
            int rfWidth, int rfHeight, int egoRow, int egoCol) {
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Deprecated
    @Override
    public void newEpisode() {
    }

    @Deprecated
    @Override
    public void giveReward(float reward) {
    }

    // This function is completely bogus! intermediateReward is not properly given
    // either modify the intermediate reward calculation or ignore this function
    // and do reward update elsewhere. forexample when integrating observation.
    @Deprecated
    @Override
    public void giveIntermediateReward(float intermediateReward) {
        // TODO Auto-generated method stub
    }


    private enum PlayMode {
        HUMAN, PS, TRAIN, TEST, ACTION_LEARN, DEMO,BEST
    }

    public static void main(String[] args) {


        MarioAgentForGen agent;// = new MarioRSAgentSimple();
        MarioAIOptions marioAIOptions = new MarioAIOptions();

        marioAIOptions.setFPS(24);
        marioAIOptions.setVisualization(false);
        marioAIOptions.setLevelDifficulty(0);
//        marioAIOptions.set
//        marioAIOptions.setEnemies("off");
//        marioAIOptions.setGapsCount(false);
        marioAIOptions.setLevelRandSeed(0);

//        marioAIOptions.setFlatLevel(true);

        PSMario ps;
        IState state;
        BasicTask basicTask;
        PSMario psMario;
        PSMarioExtenable psMarioExtenable;
        switch (playMode) {
            case HUMAN:
//                marioAIOptions.setGameViewer(true);
//                playKeyboard(marioAIOptions);
                marioAIOptions.setTimeLimit(4000);
                marioAIOptions.setMarioInvulnerable(true);
                marioAIOptions.setLevelLength(5000);
                marioAIOptions.setVisualization(true);
                marioAIOptions.setAgent(new MyHumanAgentDebug());
                basicTask = new BasicTask(marioAIOptions);
                basicTask.setOptionsAndReset(marioAIOptions);
                basicTask.doEpisodes(3, true, 1);
                break;
            case PS:
                agent = new MarioAgentForGen();
                agent.setLearningTask(new LearningTask(marioAIOptions));
                marioAIOptions.setAgent(agent);
                marioAIOptions.setVisualization(true);
                basicTask = new BasicTask(marioAIOptions);
                basicTask.runSingleEpisode(4);
                break;
            case TRAIN:
                agent = new MarioAgentForGen();
                agent.setLearningTask(new LearningTask(marioAIOptions));
                marioAIOptions.setAgent(agent);



                agent.ps.saveToGraphvizformat(true,"mG-start.txt");
                agent.learn(10);


                System.out.println("Distances : "+agent.distances);
                double avg=0;
                for (double d: agent.distances) {
                        avg+=d;
                }
                avg/=agent.distances.size();
                System.out.println("Average :"+avg);

                agent.ps.countEdges();

                marioAIOptions.setVisualization(true);
                basicTask = new BasicTask(marioAIOptions);
                basicTask.runSingleEpisode(1);

                agent.ps.saveToGraphvizformat(false,"mG-end.txt");
                agent.ps.countEdges();
                break;



        }


//        System.out.println(agent.ps.toStringALL());
//
//        XYSeries seriesTimeUsed = new XYSeries("Time steps");
//        for (int i = 0; i < agent.timesUsed.size(); i++) {
//            seriesTimeUsed.add(i, agent.timesUsed.get(i));
//        }
//
//
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(seriesTimeUsed));


        //System.exit(0);

    }

    private static void playKeyboard(MarioAIOptions marioAIOptions) {
        marioAIOptions.setAgent(new MyHumanAgentDebug());
        final BasicTask basicTask = new BasicTask(marioAIOptions);
        basicTask.setOptionsAndReset(marioAIOptions);
        basicTask.doEpisodes(2, true, 1);
        System.exit(0);
    }


    private static void play2() {
        boolean PLAY_MYSELF = false;
        Agent agent = new MarioAgentForGen();

        int fps = 24;


        final MarioAIOptions marioAIOptions = new MarioAIOptions();

        marioAIOptions.setFPS(fps);
        marioAIOptions.setTimeLimit(Integer.MAX_VALUE);
//        marioAIOptions.set
        marioAIOptions.setEnemies("off");
        marioAIOptions.setLevelDifficulty(0);
        marioAIOptions.setFlatLevel(true);//makes it easy
        //marioAIOptions.setVisualization(false);
        marioAIOptions.setVisualization(true);

        if (PLAY_MYSELF) {
            playKeyboard(marioAIOptions);

        } else {
//            marioAIOptions.setAgent(new HumanKeyboardAgent());
            marioAIOptions.setAgent(agent);
        }
        final BasicTask basicTask = new BasicTask(marioAIOptions);

//        basicTask.reset(marioAIOptions);
        final MarioCustomSystemOfValues m = new MarioCustomSystemOfValues();
//        basicTask.runSingleEpisode(1);
        // run 1 episode with same options, each time giving output of Evaluation info.
        // verbose = false
        basicTask.doEpisodes(5, false, 1);
        System.out.println("\nEvaluationInfo: \n" + basicTask.getEnvironment().getEvaluationInfoAsString());
        System.out.println("\nCustom : \n" + basicTask.getEnvironment().getEvaluationInfo().computeWeightedFitness(m));


        System.exit(0);
    }


}
