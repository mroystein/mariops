package competition.oystein;

import ch.idsia.benchmark.tasks.BasicTask;
import ch.idsia.benchmark.tasks.LearningTask;
import ch.idsia.tools.MarioAIOptions;
import competition.oystein.controller.*;
import competition.oystein.controller.helpers.*;
import competition.oystein.controller.states.*;
import competition.oystein.gui.BarChartGUI;
import competition.oystein.gui.SimpleSwingGraphGUI;
import competition.oystein.ps.PSMario;
import competition.oystein.ps.PSMario2;
import competition.oystein.ps.PSMarioExtenable;
import competition.oystein.ps.v3.PS;
import competition.oystein.ps.v3.PS_Association;
import competition.reinforcment.*;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;

import java.awt.*;
import java.util.*;
import java.util.List;

import competition.oystein.controller.states.IState.RewardMethod;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Created by Oystein on 16/12/14.
 */
public class FindBestAgent {
    //    State currentState = new State(false);
    Random random = new Random(System.currentTimeMillis());
    private MarioAIOptions marioAIOptions;
    private String filename = "storedPSMarioConfigEnemies2.txt";

    public FindBestAgent() {
        marioAIOptions = new MarioAIOptions();
        // marioAIOptions.setAgent(agent);
        marioAIOptions.setFPS(24);
        marioAIOptions.setVisualization(false);
        marioAIOptions.setLevelDifficulty(0);
//        marioAIOptions.setEnemies("off");
        marioAIOptions.setGapsCount(false);
//        marioAIOptions.setFlatLevel(true);

        marioAIOptions.setLevelRandSeed(0);
        marioAIOptions.setMarioInvulnerable(false);


    }

    public void generalizationTest(){
        marioAIOptions.setLevelLength(5000);
        marioAIOptions.setTimeLimit(5000);
        IState state =  new PSGenState(RewardMethod.PS_REWARD);

        PS ps = new PS(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0005,-1,1,1.0,PS.PS_SIMPLE);
        MarioRSAgentSimple agent = new MarioRSAgentSimple(ps,state);

        int numAgents=20;
        int numGames = 40;
        XYSeries seriesNormal = trainAgentDistAndTime(numAgents,numGames,agent)[0];

        MarioAgentForGen agentForGen = new MarioAgentForGen();
        XYSeries seriesGen = trainAgentGene(numAgents,numGames,agentForGen)[0];

     List<XYSeries> list = new ArrayList<>();
        list.add(seriesNormal);
        list.add(seriesGen);

        String header = String.format("Distances average of %d agents ",numAgents);
        EventQueue.invokeLater(() ->  new SimpleSwingGraphGUI(list,header,"Game","Distance",0,260));

    }
    public XYSeries[] trainAgentGene(int numAgents, int numGames, MarioAgentForGen agent){
        marioAIOptions.setAgent(agent);
        agent.setLearningTask(new LearningTask(marioAIOptions));
        System.out.println("Training agentPs " + agent.toString());

        double[] avg = new double[numGames];
        double[] avgDist = new double[numGames];

        for (int i = 0; i < numAgents; i++) {


            agent.resetAgentForNewSameAgeent();//resets ps, and timesteplist
            agent.learn(numGames);
//            agent.learnAndStopIfFailing(numGames);

            for (int j = 0; j < numGames; j++) {
                if (j < agent.timesUsed.size()) {
                    avg[j] += agent.timesUsed.get(j);
                    avgDist[j] += agent.distances.get(j);
                } else {
                    avg[j] += 200;//failed, and stopped. add full penalty
                    avgDist[j] += 0;
                }
            }

        }
        double avgLast10 =0;
        for (int i = avg.length-10; i < avg.length; i++) {
            avgLast10+=avg[i]/(double) numAgents;
        }
        avgLast10/=10.0;
        System.out.println("\t " + agent.toString() + " avgDistLast10=" + avgLast10);
        XYSeries series = new XYSeries(agent.toString());
        XYSeries seriesDist = new XYSeries("" + agent.toString());
        //divide all by the number of agents.
        for (int i = 0; i < avg.length; i++) {
            series.add(i, avg[i] / (double) numAgents);
            seriesDist.add(i, avgDist[i] / (double) numAgents);
        }
        return new XYSeries[]{seriesDist, series};
    }


    private void visualizeAgent(PSMario psMario, IState state) {

        visualizeAgent(new MarioRSAgentSimple(psMario, state));
    }

    //    private void visualizeAgent(PSMario psMario) {
////        IState[] states = psMario.h.length>200? new DynamicState();
//        visualizeAgent(new MarioRSAgentSimple(psMario,state));
//    }
    private void visualizeAgent(MarioAIOptions marioAIOptions) {
        System.out.println("Vizualiing!!");
        marioAIOptions.setVisualization(true);
        marioAIOptions.setFPS(80);
        BasicTask basicTask = new BasicTask(marioAIOptions);
        basicTask.runSingleEpisode(1);
        marioAIOptions.setVisualization(false);
    }

    private void visualizeAgent(MarioRSAgentSimple agent) {
        System.out.println("Visualize : Best agent :" + agent.toString());
        marioAIOptions.setAgent(agent);
        marioAIOptions.setVisualization(true);
        marioAIOptions.setFPS(24);
        BasicTask basicTask = new BasicTask(marioAIOptions);
        basicTask.runSingleEpisode(10);

//        //test random levels
//        for (int i = 0; i < 5; i++) {
//            marioAIOptions.setLevelRandSeed(random.nextInt());
//            basicTask.setOptionsAndReset(marioAIOptions);
//            basicTask.runSingleEpisode(1);
//        }


        System.out.println(agent.getPs().toStringTopPerceptsAndActions(40, agent.getCurrentState()));
        System.out.println(agent.getPs().toStringTopPerceptPerceptsAndActions(25, agent.getCurrentState()));
    }


    public XYSeries trainAgent(int numAgents, int numGames, MarioRSAgentSimple agent) {
        return trainAgentDistAndTime(numAgents, numGames, agent)[1];
    }

    public XYSeries[] trainAgentDistAndTime(int numAgents, int numGames, MarioRSAgentSimple agent) {
        marioAIOptions.setAgent(agent);
        agent.setLearningTask(new LearningTask(marioAIOptions));
        System.out.println("Training agentPs " + agent.toString());

        double[] avg = new double[numGames];
        double[] avgDist = new double[numGames];

        for (int i = 0; i < numAgents; i++) {


            agent.resetAgentForNewSameAgeent();//resets ps, and timesteplist
            agent.learn(numGames);
//            agent.learnAndStopIfFailing(numGames);

            for (int j = 0; j < numGames; j++) {
                if (j < agent.timesUsed.size()) {
                    avg[j] += agent.timesUsed.get(j);
                    avgDist[j] += agent.distances.get(j);
                } else {
                    avg[j] += 200;//failed, and stopped. add full penalty
                    avgDist[j] += 0;
                }
            }

        }
        double avgLast10 =0;
        for (int i = avg.length-10; i < avg.length; i++) {
            avgLast10+=avg[i]/(double) numAgents;
        }
        avgLast10/=10.0;
        System.out.println("\t " + agent.toString() + " avgDistLast10=" + avgLast10);
        XYSeries series = new XYSeries(agent.toString());
        XYSeries seriesDist = new XYSeries("" + agent.toString());
        //divide all by the number of agents.
        for (int i = 0; i < avg.length; i++) {
            series.add(i, avg[i] / (double) numAgents);
            seriesDist.add(i, avgDist[i] / (double) numAgents);
        }
        return new XYSeries[]{seriesDist, series};
    }

    public XYSeries[] trainAgentWithEnemies(int numAgents, int numGamesInvurable, int numGamesNormal, MarioRSAgentSimple agent, int[] levelSeeds) {
        marioAIOptions.setAgent(agent);
//        agent.setLearningTask(new LearningTask(marioAIOptions));

        LearningTask learningTask = new LearningTask(marioAIOptions);
        System.out.println("Training agentPs " + agent.toString());

        double[] avg = new double[(numGamesInvurable + numGamesNormal) * levelSeeds.length];
        double[] avgDistance = new double[(numGamesInvurable + numGamesNormal) * levelSeeds.length];

        for (int i = 0; i < numAgents; i++) {

//            marioAIOptions.setMarioInvulnerable(true);
//            learningTask.reset(marioAIOptions);
            agent.resetAgentForNewSameAgeent();//resets ps, and timesteplist
//            agent.setLearningTask(learningTask);
//            agent.learn(numGamesInvurable);
//            visualizeAgent(agent);


            for (int lvlSeed : levelSeeds) {

                marioAIOptions.setMarioInvulnerable(true);
                marioAIOptions.setLevelRandSeed(lvlSeed);
                learningTask.reset(marioAIOptions);
                agent.setLearningTask(learningTask);
                agent.learn(numGamesInvurable);

                marioAIOptions.setMarioInvulnerable(false);
                marioAIOptions.setLevelRandSeed(lvlSeed);
                learningTask.reset(marioAIOptions);
                agent.setLearningTask(learningTask);
                agent.learn(numGamesNormal);

            }


            for (int j = 0; j < avg.length; j++) {
                avg[j] += agent.timesUsed.get(j);
                avgDistance[j] += agent.distances.get(j);
            }

        }

        //divide all by the number of agents.
        XYSeries series = new XYSeries(agent.toString());
        XYSeries seriesDistance = new XYSeries(agent.toString());
        //divide all by the number of agents.
        for (int i = 0; i < avg.length; i++) {
            series.add(i, avg[i] / (double) numAgents);
            seriesDistance.add(i, avgDistance[i] / (double) numAgents);
        }

        return new XYSeries[]{series, seriesDistance};
    }


    public XYSeries[] trainAgentsOneLoongLevel(int numAgents,int numGames, MarioRSAgentSimple agent) {
        marioAIOptions.setAgent(agent);
        marioAIOptions.setLevelLength(5000);
        marioAIOptions.setTimeLimit(4000);
//        marioAIOptions.setMarioInvulnerable(true);
//        marioAIOptions.setEnemies("off");
//        marioAIOptions.setLevelRandSeed(lvlSeed);
//        agent.setLearningTask(new LearningTask(marioAIOptions));

        LearningTask learningTask = new LearningTask(marioAIOptions);
        System.out.println("Training agentPs " + agent.toString());
        learningTask.reset(marioAIOptions);
        agent.setLearningTask(learningTask);

        double[] avgDistance = new double[numGames];
        double[] avgTime = new double[numGames];

        for (int i = 0; i < numAgents; i++) {
            agent.resetAgentForNewSameAgeent();//resets ps, and timesteplist
            agent.learn(numGames);



            for (int j = 0; j < avgDistance.length; j++) {
                avgDistance[j] += agent.distances.get(j);
                avgTime[j]+=agent.timesUsed.get(j);
            }

        }

        //divide all by the number of agents.
        XYSeries series = new XYSeries(agent.toString());
        XYSeries seriesDistance = new XYSeries(agent.toString());
        //divide all by the number of agents.
        for (int i = 0; i < avgDistance.length; i++) {
            series.add(i, avgTime[i] / (double) numAgents);
            seriesDistance.add(i, avgDistance[i] / (double) numAgents);
        }

        return new XYSeries[]{series, seriesDistance};
    }


    public XYSeries makeGrapgAvgSubTask(MarioRSAgentSimple agent, int numGames, int numAgents, int[] lvls) {
        List<Double> distances = new ArrayList<>();
        List<Double> timesUsed = new ArrayList<>();
        double[] distancesArr = new double[numGames * lvls.length];
        double[] timesUsedArr = new double[numGames * lvls.length];

        List<XYSeries> seriesListDistances = new ArrayList<>();
        List<XYSeries> seriesListTimes = new ArrayList<>();
        System.out.println("Training Agent " + agent.toString() + " ");
        for (int i = 0; i < numAgents; i++) {

//            System.out.println("---- Training agent " + i + " ");
            agent.resetAgentForNewSameAgeent();
            for (int lvl : lvls) {

                marioAIOptions.setLevelRandSeed(lvl);
                agent.setLearningTask(new LearningTask(marioAIOptions));
                agent.learn(numGames);
            }

//            System.out.println("Last distance " + agent.distances.get(agent.distances.size()-1));

            XYSeries series = new XYSeries("" + i);
            XYSeries seriesTimes = new XYSeries("" + i);
            if (agent.distances.size() != distancesArr.length) {
                System.err.println("Wrong distances..");
            }
//            if (agent.distances.get(agent.distances.size() - 1) < 256) {
////                System.out.println("Distances "+agent.distances);
////                visualizeAgent(marioAIOptions);
//            }
            double debugAvg = 0;
            for (int t = 0; t < distancesArr.length; t++) {
                if (agent.distances.size() != distancesArr.length) {
                    System.out.println("WTF");
                }
                debugAvg += agent.distances.get(t);
                distancesArr[t] += agent.distances.get(t);
                timesUsedArr[t] += agent.timesUsed.get(t);
                series.add(t, agent.distances.get(t));
                seriesTimes.add(t, agent.timesUsed.get(t));
            }
            seriesListDistances.add(series);
            seriesListTimes.add(seriesTimes);
            debugAvg /= (double) agent.distances.size();

            System.out.println(String.format(", %d [%.1f]", i, debugAvg));


        }
        for (int t = 0; t < distancesArr.length; t++) {
            distances.add(distancesArr[t] / numAgents);
            timesUsed.add(timesUsedArr[t] / numAgents);
        }
        XYSeries avgXYSeriesDistance = new XYSeries(agent.toString());
        for (int i = 0; i < distances.size(); i++) {
            avgXYSeriesDistance.add(i, distances.get(i));
        }
        System.out.println();

//        EventQueue.invokeLater(() ->   new SimpleSwingGraphGUI(seriesListDistances,"Distances for individuals "+agent.toString(),"Epoch","Distances",0,300));
//        EventQueue.invokeLater(() ->   new SimpleSwingGraphGUI(seriesListTimes,"Timesteps for individuals "+agent.toString(),"Epoch","Time",0,300));
        return avgXYSeriesDistance;
    }


    public void makeGraphAverageLearningTimeAgentsForPDF() {
        marioAIOptions.setLevelDifficulty(0);
//        marioAIOptions.setEnemies("off");
//        marioAIOptions.setLevelRandSeed(1);
        marioAIOptions.setMarioInvulnerable(false);


        HardCodedState state;// = new HardCodedState(RewardMethod.SIMPLE_WITH_PUNISH);
        ILearningAlgorithm learner;
        PSMario ps;// = new PSMarioExtenable(
        // state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, MarioAction.BASIC_ACTIONS, 0.0, -1.0, 1, 0.3);
        MarioRSAgentSimple agent;// = new MarioRSAgentSimple(psMarioExtenable, state);
        //marioAIOptions.setAgent(agent);
        // agent.setLearningTask(new LearningTask(marioAIOptions));

        int numAgents = 25;
        int numGames = 100;
        List<XYSeries> xySeriesList = new ArrayList<>();

//        double[] gammas = {0.0}; //proven no effect
        RewardMethod[] rewardMethods = {RewardMethod.SIMPLE_WITH_PUNISH
//                ,RewardMethod.PS_REWARD
        };
//        rewardMethods = RewardMethod.values();
        IState[] states = {

                new DynamicState(),
//                new HardCodedState(),
//                new HardCodedState2()
        };
        double[] damperGlows = {
//                0.2,
                0.5
//                ,0.8
        };

//        int[] lvls = {0, 1, 2, 3, 4, 5, 6,7,8,9
////                5,6
//        };
        int[] lvls = new int[10];
        for (int i = 0; i < lvls.length; i++) {
            lvls[i] = i;
        }
        boolean[] uniqueExitedClipsList = {true, false};

        String[] typeOfAgentList = {"PSMario",
                "PSMario2",
                "QLearning",
                "SARSA"
        };

        String marioOptionsStr = String.format("Diff=%s , lvl=%s (%s) ,enemies=%s", marioAIOptions.getLevelDifficulty(), marioAIOptions.getLevelRandSeed(), Arrays.toString(lvls), marioAIOptions.getEnemies().equals("off") ? "off" : "on");
        System.out.println("marioOptions " + marioOptionsStr);
        int counter = 0;
        for (RewardMethod rewardMethod : rewardMethods) {
            for (IState stateLoop : states) {
                for (double damperGlow : damperGlows) {
                    for (String typeOfAgent : typeOfAgentList) {
                        long startTime = System.currentTimeMillis();

                        stateLoop.setRewardMethod(rewardMethod);
                        System.out.println("---------- " + counter + " Agent(s) beeing trained ---------");
                        counter++;
//                state = new HardCodedState(rewardMethod);
                        switch (typeOfAgent) {
                            case "PSMario":
                                learner = new PSMario(stateLoop.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, damperGlow);
                                break;
                            case "PSMario2":
                                learner = new PSMario2(stateLoop.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, damperGlow);
                                break;
                            case "QLearning":
                                learner = new QLearning(stateLoop.getNumberOfStates(), MarioAction.TOTAL_ACTIONS);
                                break;
                            case "SARSA":
                                learner = new SARSALearning(stateLoop.getNumberOfStates(), MarioAction.TOTAL_ACTIONS);
                                break;


                            default:
                                System.out.println("Error. Using standard learner");
                                learner = new PSMario(stateLoop.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, damperGlow);
                                break;
                        }


//                        ps.uniqueExitedList =unquieExitedList;

                        agent = new MarioRSAgentSimple(learner, stateLoop);
                        marioAIOptions.setAgent(agent);
                        agent.setLearningTask(new LearningTask(marioAIOptions));

                        xySeriesList.add(makeGrapgAvgSubTask(agent, numGames, numAgents, lvls));
                        long ellasped = System.currentTimeMillis() - startTime;
                        System.out.println(ellasped + " ms : " + agent.toString());
//                        System.out.println(ps.toStringEvalAfter());

                    }
                }
            }
        }

        List<PairG<Double, String>> sortedList = new ArrayList<>();

        //print average
        for (int i = 0; i < xySeriesList.size(); i++) {
            XYSeries series = xySeriesList.get(i);

            StringBuilder sb = new StringBuilder();
            sb.append("Average of Agent " + i + " ");

            double avg = 0;
            for (int j = 0; j < series.getItemCount(); j++) {
                avg += series.getY(j).doubleValue();
            }
            int lastHalfLvl = numGames / 2;
            sb.append("\n");
            double totalAvgWindow = 0;
            for (int j = 0; j < lvls.length; j++) {
                int start = numGames * (j + 1) - lastHalfLvl;
                int end = start + lastHalfLvl;
                double avgWindow = 0;

                for (int k = start; k < end; k++) {
                    avgWindow += series.getY(k).doubleValue();
                }
                avgWindow /= (double) (end - start);
                totalAvgWindow += avgWindow;

                sb.append(String.format("\tlvl=%d[%d, %d] avg=%.2f\n", lvls[j], start, end, avgWindow));
            }
            totalAvgWindow /= ((double) lvls.length);
            sortedList.add(new PairG<>(totalAvgWindow, series.getKey().toString()));
            sb.append(String.format("\t Total avg las 50 each lvl %.2f", totalAvgWindow));
            avg /= (double) series.getItemCount();
            sb.append("\n avg=" + String.format("%.2f", avg) + " | " + series.getKey().toString());
            System.out.println(sb.toString());

        }

        sortedList.sort((o1, o2) -> o1.getE().compareTo(o2.getE()));
        sortedList.stream().forEach(System.out::println);
        new SimpleSwingGraphGUI(xySeriesList, "Average distances of " + numAgents + " agents (" + marioOptionsStr + ")", "Epoch", "Distance", 0, 260);
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(distances, agent.toString(), "Average distances of "+numAgents+" agents", "Epoch", "Distance", 0, 300, true));
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(timesUsed, agent.toString(), "Average time steps of "+numAgents+" agents", "Epoch", "Time", 0, 300, true));
//
    }


    public XYSeries findOptimalParamterGeneric(MarioRSAgentSimple agent, String parameter, double[] values) {
        marioAIOptions.setLevelDifficulty(0);
        marioAIOptions.setMarioInvulnerable(false);

        int numAgents = 25;
        int numGames = 100;
        List<XYSeries> xySeriesList = new ArrayList<>();

        int[] lvls = new int[10];
        for (int i = 0; i < lvls.length; i++) {
            lvls[i] = i;
        }
        //custom
        String marioOptionsStr = String.format("lvl=(%s)*%d games ,enemies=%s %s", Arrays.toString(lvls), numGames, marioAIOptions.getEnemies().equals("off") ? "off" : "on", agent.toString());
        System.out.println(parameter + ": " + Arrays.toString(values));
        double copyOfOriginalValue = getValueAgentSwitch(agent, parameter);

        XYSeries optimalSeries = new XYSeries("Optimal " + parameter + " (" + agent.toString() + ")");
        int counter = 0;
        for (double value : values) {
            counter++;
            long startTime = System.currentTimeMillis();

            setValueAgentSwitch(agent, parameter, value);

            marioAIOptions.setAgent(agent);
//            agent.setLearningTask(new LearningTask(marioAIOptions));
            XYSeries series = makeGrapgAvgSubTask(agent, numGames, numAgents, lvls);

            double avg = 0.0;
            for (int i = 0; i < series.getItemCount(); i++) {
                avg += series.getY(i).doubleValue();
            }
            avg /= (double) series.getItemCount();

            optimalSeries.add(value, avg);

            xySeriesList.add(series);

            long ellasped = System.currentTimeMillis() - startTime;
            System.out.println(String.format("\nA=%d %.2fs %s", counter, ((double) ellasped / 1000.0), agent.toString()));
        }
        setValueAgentSwitch(agent, parameter, copyOfOriginalValue);

        List<XYSeries> listOptimal = new ArrayList<>();
        listOptimal.add(optimalSeries);

//        new SimpleSwingGraphGUI(xySeriesList, "Average distances of " + numAgents + " agents ", "Epoch", "Distance", 0, 260);
//        new SimpleSwingGraphGUI(listOptimal, ""+parameter+" parameter. Avg of " + numAgents + " agents  "+marioOptionsStr, parameter, "Distance", 0, 260);
        return optimalSeries;
    }

    private void setValueAgentSwitch(MarioRSAgentSimple agent, String parameter, double value) {
        switch (parameter.toLowerCase()) {
            case "gamma":
                agent.getPs().gamma = value;
                break;
            case "damper glow":
                agent.getPs().damperGow = value;
                break;
            default:
                System.err.println("No known parameter!! " + parameter);
        }
    }

    private double getValueAgentSwitch(MarioRSAgentSimple agent, String parameter) {
        switch (parameter.toLowerCase()) {
            case "gamma":
                return agent.getPs().gamma;
            case "damper glow":
                return agent.getPs().damperGow;
            default:
                System.err.println("No known parameter!! " + parameter);
                return -1;
        }
    }

    public void findBestSarsaAndQlearner() {
        marioAIOptions.setLevelDifficulty(0);
//        marioAIOptions.setEnemies("off");
//        marioAIOptions.setLevelRandSeed(1);
        marioAIOptions.setMarioInvulnerable(false);


        HardCodedState state;// = new HardCodedState(RewardMethod.SIMPLE_WITH_PUNISH);
        ILearningAlgorithm learner;

        MarioRSAgentSimple agent;// = new MarioRSAgentSimple(psMarioExtenable, state);
        //marioAIOptions.setAgent(agent);
        // agent.setLearningTask(new LearningTask(marioAIOptions));

        int numAgents = 25;
        int numGames = 100;
        List<XYSeries> xySeriesList = new ArrayList<>();

//        double[] gammas = {0.0}; //proven no effect
        RewardMethod[] rewardMethods = {RewardMethod.SIMPLE_WITH_PUNISH
//                ,RewardMethod.PS_REWARD
        };
//        rewardMethods = RewardMethod.values();
        IState[] states = {

                new DynamicState(),
//                new HardCodedState(),
                new HardCodedState2()
        };


        int[] lvls = new int[6];
        for (int i = 0; i < lvls.length; i++) {
            lvls[i] = i;
        }

        String[] typeOfAgentList = {
//                "PSMario",
//                "PSMario2",
                "QLearning",
                "SARSA"
        };

        //How much agent should look forward
        double[] gammaList = {
//                0.0,
                0.1,
//                0.7, 0.9
        };
        //How much value old vs new experience matter
        double[] learningScaleList = {
//                1.0,
//                5.0,
//                10.0,
                50.0,
//                100.0
        };

        String marioOptionsStr = String.format("Diff=%s , lvl=%s (%s) ,enemies=%s", marioAIOptions.getLevelDifficulty(), marioAIOptions.getLevelRandSeed(), Arrays.toString(lvls), marioAIOptions.getEnemies().equals("off") ? "off" : "on");
        System.out.println("marioOptions " + marioOptionsStr);

        int numOfAgentsToBeTrained = rewardMethods.length * states.length * learningScaleList.length * gammaList.length * typeOfAgentList.length;
        System.out.println("Training " + numOfAgentsToBeTrained + " agents on " + lvls.length + " lvls with " + numGames + " games on each lvl.");
        int counter = 0;
        List<PairG<Double, String>> sortedList = new ArrayList<>();
        for (RewardMethod rewardMethod : rewardMethods) {
            for (IState stateLoop : states) {
                for (double learningScale : learningScaleList) {
                    for (double gamma : gammaList) {
                        for (String typeOfAgent : typeOfAgentList) {
                            long startTime = System.currentTimeMillis();

                            stateLoop.setRewardMethod(rewardMethod);
                            System.out.println("---------- " + counter + " Agent(s) beeing trained ---------");
                            counter++;
                            switch (typeOfAgent) {
                                case "QLearning":

                                    learner = new QLearning(stateLoop.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, gamma, learningScale);
                                    break;
                                case "SARSA":
                                    learner = new SARSALearning(stateLoop.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, gamma, learningScale);
                                    break;


                                default:
                                    System.out.println("Error. Using standard learner");
                                    learner = new PSMario(stateLoop.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, 0.5);
                                    break;
                            }


//                        ps.uniqueExitedList =unquieExitedList;

                            agent = new MarioRSAgentSimple(learner, stateLoop);
                            marioAIOptions.setAgent(agent);
                            agent.setLearningTask(new LearningTask(marioAIOptions));
                            XYSeries series = makeGrapgAvgSubTask(agent, numGames, numAgents, lvls);
                            double avg = 0;
                            for (int j = 0; j < series.getItemCount(); j++) {
                                avg += series.getY(j).doubleValue();
                            }
                            avg /= series.getItemCount();
                            xySeriesList.add(series);
                            sortedList.add(new PairG<>(avg, series.getKey().toString()));
                            long ellasped = System.currentTimeMillis() - startTime;
                            System.out.println(String.format("%.2fs %s", ((double) ellasped / 1000.0), agent.toString()));
//                        System.out.println(ps.toStringEvalAfter());

                        }
                    }
                }
            }
        }


        sortedList.sort((o1, o2) -> o1.getE().compareTo(o2.getE()));
        sortedList.stream().forEach(p -> System.out.println(String.format("%.1f %s", p.getE(), p.getK())));
//        int nPerGUI=10;
//        if(sortedList.size()>nPerGUI){
//            int start=0;
//            int end=start+nPerGUI;
//           while (end<sortedList.size()){
//
//                start=end;
//               end=start+nPerGUI;
//           }
//        }

        new SimpleSwingGraphGUI(xySeriesList, "Average distances of " + numAgents + " agents (" + marioOptionsStr + ")", "Epoch", "Distance", 0, 260);
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(distances, agent.toString(), "Average distances of "+numAgents+" agents", "Epoch", "Distance", 0, 300, true));
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(timesUsed, agent.toString(), "Average time steps of "+numAgents+" agents", "Epoch", "Time", 0, 300, true));
//
    }

    public XYSeries trainAgent(int numAgents, int numGames, PSMario psMario, RewardMethod rewardMethod) {

        MarioRSAgentSimple agent = new MarioRSAgentSimple(psMario, rewardMethod);
        return trainAgent(numAgents, numGames, agent.getPs(), rewardMethod);


    }

    public void test() {
        long startTime = System.currentTimeMillis();

        ArrayList<XYSeries> serieses = new ArrayList<>();
        int numAgents = 2;
//
        int numGamesInvurable = 200;//but will not get hurt
        int numGamesNormal = 200;
        int numGames = numGamesInvurable + numGamesNormal;
        int reflectionTime = 1;


        double[] gammas = {
//                0.01,
//                0.001,
                //              0.0001,
                // 0.0000001,
                //   0.00000001,
                0.0
        };
        double[] damperGlows = {
//                1.0,
//                0.990,
//                0.995,
//                0.990,
                // 0.90,
//                0.5,
                0.3
        };
//        Arrays.asLi
        double[] rewards = {
//                1.0,
                -1.0
        };// 1-> PS type, -1-> negative and postive reward

        IState[] states = {
//                new DynamicState(),
//                new HardCodedState(),
                new HardCodedState2()
        };

        RewardMethod[] rewardMethods =
                {
                        RewardMethod.SIMPLE_WITH_PUNISH
//                        ,RewardMethod.EXAMPLE_WITH_SMALLER_PUNISH
//                        , RewardMethod.EXAMPLE_WITH_PUNISH
                };


        HashMap<Double, Integer> hashGammasFail = new HashMap<>();
        for (double gammaLoop : gammas)
            hashGammasFail.put(gammaLoop, 0);

        HashMap<Double, Integer> hashdamperGlowFail = new HashMap<>();
        for (double damperGlowLoop : damperGlows)
            hashdamperGlowFail.put(damperGlowLoop, 0);
        HashMap<Double, Integer> hashrewardFail = new HashMap<>();
        for (double rewardLoop : rewards)
            hashrewardFail.put(rewardLoop, 0);

        HashMap<RewardMethod, Integer> hashrewardMethodFail = new HashMap<>();

        for (RewardMethod rewardMethod : rewardMethods)
            hashrewardMethodFail.put(rewardMethod, 0);


        ArrayList<ConfigResultsScore> configResultsScores = new ArrayList<>();

        int agentCount = gammas.length * damperGlows.length * rewards.length * rewardMethods.length * states.length;
        String headerName = "#Agents to test: " + agentCount + " #games: " + numGames + " #agents: " + numAgents + " -> " + (numGames * numAgents * agentCount) + " total games.";

        System.out.println(headerName);
        PSMario bestPS = null;
        StoredPSConfig bestStoredPSConfig = null;
        double bestAvg = 0;

        boolean[] psVersions = {true
                // ,false
        };

        int[] levelSeeds = {1};//new int[10];
//        for (int i = 0; i < levelSeeds.length; i++) {
//            levelSeeds[i]=random.nextInt(Integer.MAX_VALUE);
//        }
        System.out.println("LevelSeeds " + Arrays.toString(levelSeeds));

        boolean[] unuqueExitedList = {true, false};

        for (double gammaLoop : gammas) {
            for (double damperGlowLoop : damperGlows) {
                for (double rewardLoop : rewards) {
                    for (RewardMethod rewardMethod : rewardMethods) {
                        for (IState state : states) {
                            for (boolean unqueExited : unuqueExitedList) {


                                for (boolean psVersion : psVersions) {

                                    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
                                    ConfigResultsScore configResultsScoreAverage = null;
                                    String datasetName = "";
                                    for (int i = 0; i < numAgents; i++) {
                                        PSMario psMario;
                                        if (psVersion) {
                                            psMario = new PSMario(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, gammaLoop, rewardLoop, 1, damperGlowLoop);
                                            psMario.uniqueExitedList = unqueExited;
                                        } else {

                                            psMario = new PSMarioExtenable(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, MarioAction.BASIC_ACTIONS, gammaLoop, rewardLoop, 1, damperGlowLoop);
                                        }

                                        state.setRewardMethod(rewardMethod);


                                        MarioRSAgentSimple agent = new MarioRSAgentSimple(psMario, state);
                                        StoredPSConfig storedPSConfig = new StoredPSConfig(agent);

//                            XYSeries seriesTimeUsed = trainAgent(numAgents, numGames, agent);
                                        XYSeries[] series = trainAgentWithEnemies(1, numGamesInvurable, numGamesNormal, agent, levelSeeds);
                                        ConfigResultsScore configResultsScore = new ConfigResultsScore(series[0], series[1], storedPSConfig);

                                        if (configResultsScoreAverage == null)
                                            configResultsScoreAverage = configResultsScore;
                                        else configResultsScoreAverage.add(configResultsScore);

                                        agentCount++;
                                        configResultsScore.addInfoToDataSetForCharBar(dataset, i);

                                        if (configResultsScore.successes > bestAvg) {
                                            bestAvg = configResultsScore.successes;
                                            bestPS = psMario;
                                            bestStoredPSConfig = storedPSConfig;
                                        }

                                        datasetName = storedPSConfig.name;
                                    }
                                    configResultsScoreAverage.divideBy(numAgents);
                                    configResultsScores.add(configResultsScoreAverage);
                                    startBarCharGUI(dataset, datasetName);


                                }
                            }
                        }
                    }
                }
            }
        }
        long ellasped = System.currentTimeMillis() - startTime;
        System.out.println("Training done. Used  " + ellasped / 1000 + " seconds");

        System.out.println("------------- Gammas Fail count -------- out of " + gammas.length + " values");
        System.out.println(mapToString(hashGammasFail));

        System.out.println("------------- DamperGlow Fail count -------- out of " + damperGlows.length + " values");
        System.out.println(mapToString(hashdamperGlowFail));

        System.out.println("------------- Reward Fail count -------- out of " + rewards.length + " values");
        System.out.println(mapToString(hashrewardFail));

        System.out.println("------------- Reward Method Fail count -------- out of " + rewardMethods.length + " values");
        System.out.println(mapToString2(hashrewardMethodFail));


        Collections.sort(configResultsScores, ConfigResultsScore.byMostSuccesses);
        ArrayList<StoredPSConfig> storedPSConfigs = new ArrayList<>();

        //10 or less if the list is shorter.
        int itemsToShow = 20;
        itemsToShow = configResultsScores.size() < itemsToShow ? configResultsScores.size() : itemsToShow;

        ArrayList<XYSeries> seriesesDistance = new ArrayList<>();

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        System.out.println("-------------------- Done --------------------");
        System.out.println("Top configs: " + itemsToShow);
        for (int i = 0; i < itemsToShow; i++) {
            ConfigResultsScore crs = configResultsScores.get(i);
            System.out.println(crs);
            storedPSConfigs.add(crs.storedPSConfig);

            serieses.add(crs.seriesTimeUsed);
            seriesesDistance.add(crs.seriesDistance);
            crs.addInfoToDataSetForCharBar(dataset);
        }

        //failers
        System.out.printf("\n-------------------- Failers  %d --------------------", (configResultsScores.size() - itemsToShow));
        for (int i = itemsToShow; i < configResultsScores.size(); i++) {
            System.out.println(configResultsScores.get(i));
        }


//        StoredPSConfig.saveToFile(storedPSConfigs, filename);
//        PSMario psMario9 =new PSMario(ds.getNumberOfStates(), MarioAction.TOTAL_ACTIONS,0.0001,1,-1,0.990);
//        serieses.add(trainAgent(numAgents,numGames,psMario9));
//
//        PSMario psMario10 =new PSMario(currentState.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0001, -1.0, 1, 0.8);
//        serieses.add(trainAgent(numAgents,numGames,psMario10));

//        int best=201;
//        for (int i = 0; i < serieses.size(); i++) {
//            int lastScore =serieses.get(i).getY(serieses.get(i).getItemCount()-1).intValue();
//            if(lastScore)
//        }

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(seriesesDistance, headerName, "Game", "Distances", 0, 260));
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(serieses, headerName, "Game", "TimeUsed", -220, 220));
        EventQueue.invokeLater(() -> new BarChartGUI(dataset, headerName));
//
        if (numGames > 200) {
            DefaultCategoryDataset datasetLastRounds = new DefaultCategoryDataset();
            int start = numGames - 100;
            for (int i = 0; i < itemsToShow; i++) {
                configResultsScores.get(i).addInfoToDataSetForCharBar(datasetLastRounds, start, numGames);
            }
            EventQueue.invokeLater(() -> new BarChartGUI(datasetLastRounds, "Last 100 games. " + headerName));
        }
//
        visualizeAgent(bestPS, IState.getStateFromStateNum(bestStoredPSConfig.stateClass, bestStoredPSConfig.rewardMethod));


    }

    public void startBarCharGUI(DefaultCategoryDataset dataset, String datasetName) {
        EventQueue.invokeLater(() -> new BarChartGUI(dataset, datasetName));
    }


    private void parallel() {

        List<StoredPSConfig> configs = new ArrayList<>();

//        configs.parallelStream().forEach();
        configs.parallelStream().map(c -> new PSMario(1, 1, c)).map(ps -> new MarioRSAgentSimple(ps, RewardMethod.SIMPLE)).forEach(a -> a.learnAndStopIfFailing(20));

    }


    private void test2() {
        double gamma = 1.0E-5;
        double damperGlow = 1.0;

        int nGames = 200;
        //May have problems by reusing the states?? Need to check
        IState[] states = {
//                new DynamicState(),
                new HardCodedState()
        };
        double[] rewards = {
                -1.0
//                ,1.0
        };
        int numAgents = 10;

        ArrayList<XYSeries> serieses = new ArrayList<>();

        System.out.println("# Agents " + states.length * RewardMethod.values().length + " nGames=" + nGames);
        for (IState state : states) {
            for (RewardMethod rewardMethod : RewardMethod.values()) {


                state.setRewardMethod(rewardMethod);
                MarioRSAgentSimple agent = new MarioRSAgentSimple(state, gamma, damperGlow);
                marioAIOptions.setAgent(agent);
                agent.setLearningTask(new LearningTask(marioAIOptions));
                System.out.println("Training agent: " + agent);
                agent.learn(nGames);
                serieses.add(toXYSeriesFromList(agent.timesUsed, agent.toString()));

                //copy debug
                state.setRewardMethod(rewardMethod);
                agent = new MarioRSAgentSimple(state, gamma, damperGlow);
                marioAIOptions.setAgent(agent);
                agent.setLearningTask(new LearningTask(marioAIOptions));
                System.out.println("Training agent: " + agent);
                agent.learn(nGames);
                serieses.add(toXYSeriesFromList(agent.timesUsed, agent.toString() + " COPY"));

            }
        }
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(serieses));

    }

    private void testRealPS() {
        double gamma = 1.0E-5;
        double damperGlow = 1.0;

        int nGames = 40;
        //May have problems by reusing the states?? Need to check
        IState[] states = {
//                new DynamicState(),
                new HardCodedState()
        };


        List<ConfigResultsScore> configResultsScores = new ArrayList<>();

        ArrayList<XYSeries> serieses = new ArrayList<>();
        System.out.println("# Agents " + states.length * RewardMethod.values().length + " nGames=" + nGames);
        for (IState state : states) {
            for (RewardMethod rewardMethod : RewardMethod.values()) {


                for (int copy = 0; copy < 4; copy++) {
                    state.setRewardMethod(rewardMethod);
                    MarioRSAgentSimple agent = new MarioRSAgentSimple(state, gamma, -5.0, damperGlow);
                    marioAIOptions.setAgent(agent);
                    agent.setLearningTask(new LearningTask(marioAIOptions));

                    agent.learn(nGames);
                    String copyName = copy == 0 ? "" : " copy" + copy;

                    XYSeries series = toXYSeriesFromList(agent.timesUsed, agent.toString() + copyName);
                    serieses.add(series);
                    configResultsScores.add(new ConfigResultsScore(series, new StoredPSConfig(agent.getPs(), rewardMethod, state.getStateClass())));
                    System.out.println("Trained agent: " + agent + " Avg: " + configResultsScores.get(configResultsScores.size() - 1).getAverage());
                }


            }
        }
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(serieses));

        Collections.sort(configResultsScores);
        for (ConfigResultsScore crs : configResultsScores) {
            System.out.println(crs);
        }


    }

    private XYSeries toXYSeriesFromList(List<Integer> list, String name) {
        XYSeries series = new XYSeries(name);
        for (int i = 0; i < list.size(); i++) {
            series.add(i, list.get(i));
        }
        return series;
    }

    private void loadOldAgents() {
        int numAgents = 4;
        int numGames = 100;
        ArrayList<XYSeries> serieses = new ArrayList<>();

        ArrayList<StoredPSConfig> storedPSConfigs = StoredPSConfig.readFile(filename);
        System.out.println("# stored configs: " + storedPSConfigs.size());
        DynamicState ds = new DynamicState(RewardMethod.SIMPLE);

        ArrayList<ConfigResultsScore> configResultsScores = new ArrayList<>();

        IState[] states = {
//                new DynamicState(),
                new HardCodedState()
        };

        for (StoredPSConfig conf : storedPSConfigs) {

            for (IState state : states) {//forgot to save state in the file.. just do all again..
                state.setRewardMethod(conf.rewardMethod);
//                PSMario psMario = new PSMario(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, conf.gamma, conf.reward, 1, conf.damperGlow);

//                StoredPSConfig storedPSConfig = new StoredPSConfig(psMario, rewardMethod);

                MarioRSAgentSimple agent = new MarioRSAgentSimple(state, conf.gamma, conf.reward, conf.damperGlow);
                XYSeries series = trainAgent(numAgents, numGames, agent);

                configResultsScores.add(new ConfigResultsScore(series, conf));


//             PSMario psMario =new PSMario(ds.getNumberOfStates(), MarioAction.TOTAL_ACTIONS,gammaLoop,rewardLoop,1,damperGlowLoop);

//                PSMario psMario = new PSMario(ds.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, conf.gamma, conf.reward, 1, conf.damperGlow);
//
//                XYSeries seriesTimeUsed = trainAgent(numAgents, numGames, psMario, conf.rewardMethod);
//                configResultsScores.add(new ConfigResultsScore(seriesTimeUsed, conf));
            }

        }

        Collections.sort(configResultsScores);
        int itemsToShow = 10;
        //10 or less if the list is shorter.
        itemsToShow = configResultsScores.size() < 10 ? configResultsScores.size() : 10;

        System.out.println("-------------------- Done --------------------");
        System.out.println("Top configs: " + itemsToShow);
        for (int i = 0; i < itemsToShow; i++) {
            System.out.println(configResultsScores.get(i));
            serieses.add(configResultsScores.get(i).seriesTimeUsed);
        }


        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(serieses));
    }


    private double avgOfSeries(XYSeries series) {
        double avg = 0;
        for (int i = 0; i < series.getItemCount(); i++) {
            avg += series.getY(i).doubleValue();
        }
        return avg / series.getItemCount();
    }

    private String mapToString(HashMap<Double, Integer> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Double, Integer> entry : map.entrySet()) {
            sb.append(entry.getKey()).append(" : ").append(entry.getValue());
            sb.append("\n");
        }
        return sb.toString();
    }

    private String mapToString2(HashMap<RewardMethod, Integer> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<RewardMethod, Integer> entry : map.entrySet()) {
            sb.append(entry.getKey()).append(" : ").append(entry.getValue());
            sb.append("\n");
        }
        return sb.toString();
    }

    public void findOptimalParamterStart() {
//        IState state = new HardCodedState2();
        IState state = new DynamicState();
        PSMario2 learner = new PSMario2(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, 0.9);
        learner.uniqueExitedList = true;
        MarioRSAgentSimple agent = new MarioRSAgentSimple(learner, RewardMethod.PS_REWARD);


        int nPoints = 10;
        double[] damperGlows = new double[nPoints + 1];
        double stepSize = 1.0 / (double) (nPoints);
        for (int i = 1; i < damperGlows.length; i++) {
            damperGlows[i] = damperGlows[i - 1] + stepSize;
        }


//        findOptimalParamterGeneric(agent,"gamma",damperGlows);

        //both
        List<XYSeries> listOptimal = new ArrayList<>();

        listOptimal.add(findOptimalParamterGeneric(agent, "damper glow", damperGlows));

        listOptimal.add(findOptimalParamterGeneric(agent, "gamma", damperGlows));


        new SimpleSwingGraphGUI(listOptimal, "Optimal parameters", "parameter value", "Distance", 0, 260);


//        findOptimalParamter(agent);
    }


    public void compareLearningAlgorithms(List<ILearningAlgorithm> list, IState state, int numGames, int numAgents) {

        List<XYSeries> seriesList = new ArrayList<>();
        for (ILearningAlgorithm l : list) {
            MarioRSAgentSimple a = new MarioRSAgentSimple(l, state);
            XYSeries series = trainAgentDistAndTime(numAgents, numGames, a)[0];


            trainAgentWithEnemies(numAgents, 0, numGames, a, lvls0);
            seriesList.add(series);

        }

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(seriesList, "Distances", "game", "Distance", 0, 260));
    }

    public void v2Testing() {
        IState state = new DynamicState(RewardMethod.PS_REWARD);
        state = new HardCodedState(RewardMethod.PS_REWARD);
        List<ILearningAlgorithm> list = new ArrayList<>();
//        list.add(new QLearning(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.5, 2.0));


//        list.add(new RLearner(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.5,1.0,0.001,0.999,RLearner.Q_LEARNING,ILearningAlgorithm.E_GREEDY));
//        list.add(new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.2, .9, 0.001, 0.999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY));

//        list.add(new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.1, .5, 0.001, 0.999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY));
//        list.add(new PSMario(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, 0.9));
//        list.add(new PSMario(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, 0.5));
//        list.add(new PSMario(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, 0.99));
        list.add(new PSMario(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1, 1, 0.9f));
        list.add(new PS_Association(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0f,1,0.9f));

        int numGames = 100;
        int numAgents = 1;
        compareLearningAlgorithms(list, state, numGames, numAgents);


    }

    final int[] lvls0to10 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    final int[] lvls0to5 = {0, 1, 2, 3, 4, 5};
    final int[] lvls0to2 = {0, 1, 2};
    final int[] lvls0 = {0};


    public void bruteForceStates() {
        int numAgents = 1;
        int numGames=1000;
        int[] lvls = new int[10];
        for (int i = 0; i < lvls.length; i++) {
            lvls[i]=i;
        }

        RewardMethod rm = RewardMethod.PS_REWARD;
        IState[] states = {
                new DynamicState(rm),
//                new DynamicState2(rm),
                new DynamicState3(rm),
                new HardCodedState(rm),
//                new HardCodedState2(rm),
//                new HardCodedState3(rm)
        };
        List<XYSeries> xySeriesList = new ArrayList<>();
        List<XYSeries> xyTimeSeriesList = new ArrayList<>();
        List<Result> resultList = new ArrayList<>();
        for (IState state : states) {
            RLearner l = new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.1189, .8, 0.001, 0.9999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY);
            MarioRSAgentSimple agent = new MarioRSAgentSimple(l, state);

            XYSeries[] xySerieses = trainAgentWithEnemies(numAgents, 0, numGames, agent, lvls);
            XYSeries dist = xySerieses[1];
            double avg = MyUtils.average(dist);
            xyTimeSeriesList.add(xySerieses[0]);
            System.out.println(state.getName()+" "+avg);
            resultList.add(new Result(state.getName(),avg));
            xySeriesList.add(dist);
        }
        resultList.sort(Result.COMP_HIGHEST_FIRST);
        for (Result result : resultList) {
            System.out.println(result);
        }

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(xySeriesList, "Distances", "epoch", "Distance", 0, 260));
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(xyTimeSeriesList, "Time", "epoch", "Distance", 0, 260));
    }

    public void bestAgents(){
        int numAgents = 30;
        int numGames = 10;

        int numLvls=10;
        int[] lvls = new int[numLvls];
        for (int i = 0; i < lvls.length; i++) {
            lvls[i]=i;
//            lvls[i]=random.nextInt(500);
//            lvls[i+lvls.length/2]= lvls[i];
        }

        System.out.println("Lvls: : "+Arrays.toString(lvls));

        System.out.println(Arrays.toString(lvls));

        List<ILearningAlgorithm> learners = new ArrayList<>();
        List<ILearningAlgorithm> learners2 = new ArrayList<>();
        IState state = new DynamicState(RewardMethod.PS_REWARD);
        IState state2 = new DynamicState3(RewardMethod.PS_REWARD);

//        learners.add(new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.66, .9, 0.01, 0.999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY));
//        learners.add(new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.1, .8, 0.01, 0.999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY));
//        learners.add( new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.339));
        learners.add( new PS(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.339,PS.SOFTMAX));
        learners.add( new PS(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.339,PS.PS_SIMPLE));

//        learners2.add( new PS(state2.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.339,PS.SOFTMAX));
//        learners2.add( new PS(state2.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.339,PS.PS_SIMPLE));

//        learners.add( new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.339));
//        learners.add( new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,50,0.339));
//        learners.add( new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,10,0.339));

//        learners.add( new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,100,0.339));
//        learners.add( new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.5));
//
        List<Result> resultList = new ArrayList<>();
        List<XYSeries> xySeriesList = new ArrayList<>();
        for (ILearningAlgorithm learner : learners) {
            MarioRSAgentSimple agent = new MarioRSAgentSimple(learner, state);
            XYSeries[] xySerieses = trainAgentWithEnemies(numAgents, 0, numGames, agent, lvls);
            XYSeries dist = xySerieses[1];
            xySeriesList.add(dist);

            double avg = MyUtils.average(dist);

            resultList.add(new Result(learner.toString(), avg));

            double[] avgMid = getAvgMiddle(dist);
            System.out.println(agent.toString() + " avg dist =" + avg);
            System.out.printf("Average Distance : {total=%.1f, firstRun=%.1f, secondRun=%.1f} \n",avgMid[0],avgMid[1],avgMid[2]);
        }
        for (ILearningAlgorithm learner : learners2) {
            MarioRSAgentSimple agent = new MarioRSAgentSimple(learner, state2);
            XYSeries[] xySerieses = trainAgentWithEnemies(numAgents, 0, numGames, agent, lvls);
            XYSeries dist = xySerieses[1];
            xySeriesList.add(dist);
            double avg = MyUtils.average(dist);
            resultList.add(new Result(learner.toString(), avg));

            double[] avgMid = getAvgMiddle(dist);
            System.out.println(agent.toString() + " avg dist =" + avg);
            System.out.printf("Average Distance : {total=%.1f, firstRun=%.1f, secondRun=%.1f} \n", avgMid[0], avgMid[1], avgMid[2]);
        }


        Collections.sort(resultList, Result.COMP_HIGHEST_FIRST);

        resultList.forEach(System.out::println);

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(xySeriesList, "Distances", "Game", "Distance", 0, 260));


    }

    public static double[] getAvgMiddle(XYSeries series){
        double[] d= new double[3];
        int middle = series.getItemCount()/2;

        for (int i = 0; i < series.getItemCount(); i++) {
            double s = series.getY(i).doubleValue();
            d[0]+=s;
            if(i<middle)
                d[1]+=s;
            else d[2]+=s;

        }
       d[0]/=series.getItemCount();
        d[1]/=middle;
        d[2]/=middle;
        return d;
    }

    public void bruteForceParametersForRL() {



        int numAgents = 10;
        int numGames = 10;
        IState state = new DynamicState(RewardMethod.PS_REWARD);
//        state = new DynamicState3(RewardMethod.EXAMPLE_WITH_SMALLER_PUNISH);
        RLearner l = new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.1, .5, 0.01, 0.999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY);
        MarioRSAgentSimple agent = new MarioRSAgentSimple(l, state);
        List<Result> resultList = new ArrayList<>();
        List<XYSeries> xySeriesList = new ArrayList<>();
        StringBuilder sbPlot = new StringBuilder();
        sbPlot.append("# alpha gamma steps\n");
        for (double alpha : MyUtils.generateRange(0.01, 0.99, 10)) {
            for (double gamma : MyUtils.generateRange(0.1, 1.0, 10)) {

//                l.reset();
                l.setAlpha(alpha);
                l.setGamma(gamma);
                agent.reset();//Will reset learner also.

                XYSeries[] xySerieses = trainAgentWithEnemies(numAgents, 0, numGames, agent, lvls0to10);
                XYSeries dist = xySerieses[1];
                xySeriesList.add(dist);
                double avg = MyUtils.average(dist);
                sbPlot.append(alpha + " " + gamma + " " + avg + "\n");
                resultList.add(new Result(l.toString(), avg));
                System.out.println(agent.toString() + " avg dist =" + avg);


            }
            sbPlot.append("\n");
        }
        Collections.sort(resultList, Result.COMP_HIGHEST_FIRST);
//        Collections.sort(resultList);
        for (int i = 0; i < 20; i++) {
            System.out.println(resultList.get(i));
        }

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(xySeriesList, "Distances", "vite", "Distance", 0, 260));

        System.out.println(sbPlot.toString());

    }
    public void bruteForceParametersForPS() {

        int numAgents = 10;
        int numGames = 10;
        IState state = new DynamicState(RewardMethod.PS_REWARD);
//        state = new DynamicState3(RewardMethod.PS_REWARD);
        PSMario2 ps = new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.0);
//        RLearner l = new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.1, .5, 0.001, 0.999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY);
        MarioRSAgentSimple agent = new MarioRSAgentSimple(ps, state);
        List<Result> resultList = new ArrayList<>();
        List<XYSeries> xySeriesList = new ArrayList<>();
        StringBuilder sbPlot = new StringBuilder();
        sbPlot.append("# alpha gamma steps\n");
        for (double gamma : MyUtils.generateRange(0, 0.99, 10)) {
            for (double glow : MyUtils.generateRange(0.01, 1.0, 10)) {


//                l.reset();
                ps.gamma=gamma;
                ps.damperGow=glow;


                agent.reset();//Will reset learner also.

                XYSeries[] xySerieses = trainAgentWithEnemies(numAgents, 0, numGames, agent, lvls0to10);
                XYSeries dist = xySerieses[1];
//                dist.setKey(glow+"");
//                xySeriesList.add(dist);
                double avg = MyUtils.average(dist);
                sbPlot.append(gamma+" "+ glow + " " + avg + "\n");
                resultList.add(new Result(ps.toString(), avg));
                System.out.println(agent.toString() + " avg dist =" + avg);


            }
            sbPlot.append("\n");
        }
        Collections.sort(resultList, Result.COMP_HIGHEST_FIRST);
//        Collections.sort(resultList);
        for (int i = 0; i < 10; i++) {
            System.out.println(resultList.get(i));
        }

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(xySeriesList, "Distances", "vite", "Distance", 0, 260));

        System.out.println(sbPlot.toString());

    }

    public void bruteForcePSvsRL() {

        int numAgents = 1;
        int numGames = 3000;
        IState state = new DynamicState(RewardMethod.PS_REWARD);
        state = new DynamicState3(RewardMethod.PS_REWARD);
        PSMario2 ps = new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.1);
//        RLearner l = new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.1, .5, 0.001, 0.999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY);
       MarioRSAgentSimple agent;


        List<ILearningAlgorithm> learningAlgorithms = new ArrayList<>();
//        learningAlgorithms.add(new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.1));
        learningAlgorithms.add(new PS(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,-1,1,0.1));
        learningAlgorithms.add(new RLearner(state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.99, .8, 0.01, 0.999, RLearner.Q_LEARNING, ILearningAlgorithm.E_GREEDY));
        learningAlgorithms.add(new PSMarioExtenable(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,MarioAction.BASIC_ACTIONS,0.0,-1,1,0.1));
//        learningAlgorithms.add(new PSMario2(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,0.0,1,1,0.1));

        List<Result> resultList = new ArrayList<>();
        List<XYSeries> xySeriesList = new ArrayList<>();
        List<XYSeries> xySeriesTimeList = new ArrayList<>();
        StringBuilder sbPlot = new StringBuilder();
        sbPlot.append("# alpha gamma steps\n");
        for (ILearningAlgorithm l : learningAlgorithms) {
            agent = new MarioRSAgentSimple(l,state);
//            agent.reset();//Will reset learner also.

//            XYSeries[] xySerieses = trainAgentWithEnemies(numAgents, 0, numGames, agent, lvls0to10);

            XYSeries[] xySerieses = trainAgentsOneLoongLevel(numAgents,numGames,agent);
            XYSeries dist = xySerieses[1];

            xySeriesList.add(dist);
            xySeriesTimeList.add(xySerieses[0]);
            double avg = MyUtils.average(dist);
//            sbPlot.append( glow + " " + avg + "\n");
            resultList.add(new Result(l.toString(), avg));
            System.out.println(agent.toString() + " avg dist =" + avg);
        }

        Collections.sort(resultList, Result.COMP_HIGHEST_FIRST);
//        Collections.sort(resultList);
        int toPrint=resultList.size()<10?resultList.size():10;
        for (int i = 0; i < toPrint; i++) {
            System.out.println(resultList.get(i));
        }



        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(xySeriesList, "Distances", "epoch", "Distance", 0, 260));
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(xySeriesTimeList, "Time", "epoch", "Distance", 0, 260));

        System.out.println(sbPlot.toString());






        Scanner sc = new Scanner(System.in);
        System.out.println("Print gnuplot? y/n");
        if(sc.next().equals("y")){
            for (XYSeries series : xySeriesList) {
                System.out.println(series.getKey());
                System.out.println(MyUtils.xySeriesToGnuPlotStr(series));
            }
        }
        System.out.println("Store xyPlots?");
        if(sc.next().equals("y")){
            for (XYSeries aXySeriesList : xySeriesList) {
                MyUtils.writeToFile(aXySeriesList, aXySeriesList.toString());
            }
        }

    }



    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        FindBestAgent findBestAgent = new FindBestAgent();
//        findBestAgent.makeGraphAverageLearningTimeAgentsForPDF();
//        findBestAgent.findBestSarsaAndQlearner();

        // findBestAgent.findOptimalParamterStart();
//        findBestAgent.v2Testing();
//        findBestAgent.bruteForceParametersForRL();
//        findBestAgent.bruteForcePSvsRL();
//        findBestAgent.bruteForceStates();
//        findBestAgent.bruteForceParametersForPS();
//        findBestAgent.test();
//        findBestAgent.bestAgents();
        findBestAgent.generalizationTest();
//        findBestAgent.loadOldAgents();
//        findBestAgent.testRealPS();


        long ellasped = System.currentTimeMillis() - start;
        System.out.println("Done.. Can exit. Used " + ellasped / 1000 + " seconds");
//    marioAIOptions.setVisualization(true);
//    BasicTask basicTask = new BasicTask(marioAIOptions);
//    basicTask.runSingleEpisode(1);


        //System.exit(0);

    }
}
