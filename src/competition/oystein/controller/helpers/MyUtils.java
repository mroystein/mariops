package competition.oystein.controller.helpers;


import org.jfree.data.xy.XYSeries;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Oystein on 27.08.14.
 */
public class MyUtils {

    public static String toStr(double d) {
        return String.format("%.3f", d);
    }

    public static String toStrAsPropability(double d) {
        return String.format("%.0f%%", d * 100);
    }


    public static String xySeriesToGnuPlotStr(XYSeries series){
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        for (int i = 0; i < series.getItemCount(); i++) {
            sb.append(series.getX(i)).append(" ").append(series.getY(i)).append("\n");

        }


        return sb.toString();
    }

    /**
     * Generates a double array. with start and end inclusive
     *
     * @param start
     * @param end
     * @param numPoints
     * @return
     */
    public static double[] generateRange(double start, double end, int numPoints) {
        double[] a = new double[numPoints];
        double incre = (end - start) / (numPoints - 1);
        a[0] = start;
        for (int i = 1; i < a.length; i++)
            a[i] = a[i - 1] + incre;
        return a;
    }

    /**
     * Add 2 arrays together. Used for generating points for optimal parameter finding.
     * Also remove duplicates.
     * @param a
     * @param b
     * @return
     */
    public static double[] addArraysAndSort(double[] a, double[] b){
        HashSet<Double> set = new HashSet<>();
        for (double v:a)
            set.add(v);
        for (double v:b)
            set.add(v);

        double[] r = new double[set.size()];
        int c=0;
        for(double v : set)
            r[c++]=v;

        Arrays.sort(r);
        return r;
    }
    //mapRange(MIN_POSITION, MAX_POSITION, 0, normalizedIntervalSizePos - 1, position)

    /**
     *
     * @param a1 MinCurr
     * @param a2 MaxCurr
     * @param b1 MinNew
     * @param b2 MaxNew
     * @param s curr
     * @return
     */
    public static double mapRange(double a1, double a2, double b1, double b2, double s) {
        return b1 + ((s - a1) * (b2 - b1)) / (a2 - a1);
    }

    public static double average(XYSeries series) {
        return averageLastN(series, series.getItemCount());
    }

    public static double averageLastN(XYSeries series, int lastN) {
        double avg = 0;
        for (int i = series.getItemCount() - lastN; i < series.getItemCount(); i++) {
            avg += series.getY(i).doubleValue();
        }
        return avg / (double) lastN;
    }

    public static void main(String[] args) {

        double[] a = generateRange(0, 1, 5);
        double[] b = generateRange(0.0, 0.02, 5);
        double[] r = addArraysAndSort(a, b);

        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
        System.out.println("+");
        System.out.println(Arrays.toString(r)+" "+ r.length);
        System.out.println(r[0]==r[1]);



        Scanner sc = new Scanner(System.in);
        System.out.println("Press 1 to save, else 0");
        if(sc.nextInt()==1){
            System.out.println("Saved.");
        }else{
            System.out.println("Not saved.");
        }

//        XYSeries s = new XYSeries("LOL");
//        for (int i = 0; i < 10; i++) {
//            s.add(i, i * 3.0);
//        }
//        System.out.println(averageLastN(s, s.getItemCount()));
//        System.out.println(average(s));
    }
//    public static double average(XYSeries series){
//        double avg=0;
//        for (int i = 0; i < series.getItemCount(); i++) {
//            avg+=series.getY(i).doubleValue();
//        }
//        avg/=(double)series.getItemCount();
//        return avg;
//    }

    static Random r = new Random();

    /**
     * E_Greedy action selection.
     * epsilon selection for greedy must be implemented by yourself!.
     *
     * @param arr q or h values
     * @return best action
     */
    public static int eGreedy(double[] arr) {
        int selectedAction = -1;
        double maxQ = -Double.MAX_VALUE;
        int[] doubleValues = new int[arr.length];
        int maxDV = 0;

        for (int action = 0; action < arr.length; action++) {

            if (arr[action] > maxQ) {
                selectedAction = action;
                maxQ = arr[action];
                maxDV = 0;
                doubleValues[maxDV] = selectedAction;
            } else if (arr[action] == maxQ) {
                maxDV++;
                doubleValues[maxDV] = action;
            }
        }
        //take random of equally good actions
        if (maxDV > 0) {
            selectedAction = doubleValues[r.nextInt(maxDV + 1)];
        }

        return selectedAction;
    }


    /**
     * Safe softmax. if weights are larger than 700, e^720 overflows..
     * Runtime o(|a|)
     *
     * @param arr
     * @param a
     * @return
     */
    public static double softMax(double[] arr, double a) {

        return Math.exp((a - logSumOfExponentials(arr)));
    }


    public static double softMax(float[] arr, float a) {

        return Math.exp((a - logSumOfExponentials(arr)));
    }


    public static double logSumOfExponentials(float[] xs) {
        if (xs.length == 1) return xs[0];
        double max = maximum(xs);
        double sum = 0.0;
        for (int i = 0; i < xs.length; ++i)
            if (xs[i] != Double.NEGATIVE_INFINITY)
                sum += Math.exp(xs[i] - max);
        return max + Math.log(sum);
    }

    public static double logSumOfExponentials(double[] xs) {
        if (xs.length == 1) return xs[0];
        double max = maximum(xs);
        double sum = 0.0;
        for (int i = 0; i < xs.length; ++i)
            if (xs[i] != Double.NEGATIVE_INFINITY)
                sum += Math.exp(xs[i] - max);
        return max + Math.log(sum);
    }

    private static double maximum(double[] xs) {
        double currMax = Double.MIN_VALUE;
        for (double x : xs)
            if (x > currMax) currMax = x;
        return currMax;
    }
    private static float maximum(float[] xs) {
        float currMax = Float.MIN_VALUE;
        for (float x : xs)
            if (x > currMax) currMax = x;
        return currMax;
    }

    private static double maximumJava7(double[] xs) {
        double currMax = Double.MIN_VALUE;
        for (int i = 0; i < xs.length; i++) {

            if (xs[i] > currMax) {
                currMax = xs[i];
            }
        }
        return currMax;
    }

    public static double softMaxIgnoreNegative(double[] arr, double a) {
        if (a < 0)
            return 0.0;

        return Math.exp((a - logSumOfExponentialsIgnoreNegative(arr)));
    }

    public static double logSumOfExponentialsIgnoreNegative(double[] xs) {
        if (xs.length == 1) return xs[0];
        double max = maximum(xs);
        double sum = 0.0;
        for (int i = 0; i < xs.length; ++i)
            if (xs[i] != Double.NEGATIVE_INFINITY && xs[i] > 0)
                sum += Math.exp(xs[i] - max);

        return max + Math.log(sum);
    }


    public static void writeToFile(Serializable obj, String filename) {
        try {
          writeToFile(obj, createFile(filename));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void writeToFile(Serializable obj,File file) {
        try {
            FileOutputStream fout = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(obj);
            oos.close();
            System.out.println("Saved "+obj+" to file "+file.toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public static File createFile(String nameOnly){
        return new File(SER_FOLDER+""+nameOnly+".ser");
    }
    public static File createFile(int num){
        return createFile(""+num);
    }

    public static String SER_FOLDER="files/";

    public static Object readFile(String filename) {
        try {
            return readFile(createFile(filename));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    public static Object readFile(File file) {
        try {
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            return in.readObject();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }



    public static XYSeries getXYSeries(int fileNum){
        try {
            return (XYSeries) readFile(createFile(fileNum));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
