package competition.oystein.controller.helpers;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Oystein on 08/04/15.
 */
public class MatrixHash implements IMatrix {

    HashMap<Integer, double[]> map;

    int numStates;
    int numActions;

    public MatrixHash(int numStates, int numActions) {
        this.numStates = numStates;
        this.numActions = numActions;
        map = new HashMap<>(1000);
    }

    @Override
    public double[] getActions(int state) {
        double[] actions = map.get(state);
        if (actions == null) {
            actions = getRandomDoubleInitArr(new double[numActions]);
            map.put(state, actions);
        }
        return actions;
    }

    @Override
    public double getAction(int state, int action) {
        return getActions(state)[action];
    }

    @Override
    public void setValue(int state, int action, double value) {
        getActions(state)[action] = value;
    }

    @Override
    public void reset() {
        map.clear();
    }


    public static void main(String[] args) {
        MatrixHash matrix = new MatrixHash(4, 3);

        System.out.println(Arrays.toString(matrix.getActions(2)));
        matrix.setValue(2, 1, 2.0);
        System.out.println(Arrays.toString(matrix.getActions(2)));
    }
}
