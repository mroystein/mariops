package competition.oystein.controller.helpers;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Oystein on 16/02/15.
 */


public class Pair implements Serializable{


    public int x, y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair pair = (Pair) o;

        if (x != pair.x) return false;
        if (y != pair.y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    public static Comparator<Pair> byYHighestFirst = new Comparator<Pair>() {
        @Override
        public int compare(Pair o1, Pair o2) {
            return o2.y-o1.y;
        }
    };
}