package competition.oystein.controller.helpers;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;

import java.util.Comparator;

/**
* Created by Oystein on 19/02/15.
*/
public class ConfigResultsScore implements Comparable<ConfigResultsScore> {
    public XYSeries seriesTimeUsed;
    public XYSeries seriesDistance;
    public StoredPSConfig storedPSConfig;



    public int deaths = 0;
    public int outOfTime=0;
    public int successes=0;
    public double avgDistance=0;
    /* Average time used on successful runs! (Fails are not included)*/
    public double avgTimeUsed = 0;

    public ConfigResultsScore(XYSeries seriesTimeUsed, StoredPSConfig storedPSConfig) {
        this.seriesTimeUsed = seriesTimeUsed;
        this.storedPSConfig = storedPSConfig;

        compute();

    }

    public void addInfoToDataSetForCharBar(DefaultCategoryDataset dataset){
        dataSetAsPercept(dataset,storedPSConfig.name,successes,deaths,outOfTime,avgDistance,avgTimeUsed,seriesTimeUsed.getItemCount());

    }

    public void addInfoToDataSetForCharBar(DefaultCategoryDataset dataset,int version){
        dataSetAsPercept(dataset,storedPSConfig.name+""+version,successes,deaths,outOfTime,avgDistance,avgTimeUsed,seriesTimeUsed.getItemCount());

    }

    public void add(ConfigResultsScore other){
        if(seriesTimeUsed.getItemCount()!=other.seriesTimeUsed.getItemCount()){
            System.out.println("Error.. not adding correct");
        }

        deaths+=other.deaths;
        outOfTime+=other.outOfTime;
        successes+=other.successes;
        avgDistance+=other.avgDistance;
        avgTimeUsed+=other.avgTimeUsed;

    }
    public void divideBy(int nAgents){
        deaths/=nAgents;
        outOfTime/=nAgents;
        successes/=nAgents;
        avgDistance/=nAgents;
        avgTimeUsed/=nAgents;
    }
    public void addInfoToDataSetForCharBar(DefaultCategoryDataset dataset,int start,int end){
        int deaths=0,outOfTime=0,successes=0;
        double avgDistance=0,avgTimeUsed=0;
        for (int i = start; i < end; i++) {
            double time = seriesTimeUsed.getY(i).doubleValue();
            if(time<0)
                deaths++;
            else if (time>=201)
                outOfTime++;
            else {
                avgTimeUsed+=time;
                successes++;
            }

            avgDistance+=seriesDistance.getY(i).doubleValue();
        }
        avgDistance/=(double)(end-start);
        avgTimeUsed /= successes;
        String name = storedPSConfig.name;

//        dataset.addValue(successes,name,"Successes");
//        dataset.addValue(deaths, name, "Deaths");
//        dataset.addValue(outOfTime,name,"Out of Time");
//        dataset.addValue(avgDistance,name,"AvgDistance");

        dataSetAsPercept(dataset,name,successes,deaths,outOfTime,avgDistance,avgTimeUsed,(end-start));

    }

    private void dataSetAsPercept(DefaultCategoryDataset dataset,String name, int successes,int deaths,int outOfTime,double avgDistance,double avgTimeSuccess,int numGames){


        dataset.addValue((((double)successes/(double)numGames)*100.0),name,"Successes");
        dataset.addValue((((double)deaths/(double)numGames)*100.0), name, "Deaths");
        dataset.addValue((((double)outOfTime/(double)numGames)*100.0),name,"Out of Time");
        dataset.addValue(((avgDistance /(256.0))*100.0),name,"AvgDistance");
        dataset.addValue(((avgTimeSuccess /(200.0))*100.0),name,"AvgTime on succesfull runs");
    }

    private void compute(){

        for (int i = 0; i < seriesTimeUsed.getItemCount(); i++) {
            double time = seriesTimeUsed.getY(i).doubleValue();
            if(time<0)
                deaths++;
            else if (time>=201)
                outOfTime++;
              else {
                avgTimeUsed+=time;
                successes++;
            }

            avgDistance+=seriesDistance.getY(i).doubleValue();

        }
        avgDistance/=(double)seriesDistance.getItemCount();
        avgTimeUsed /= seriesTimeUsed.getItemCount();
    }

    public ConfigResultsScore(XYSeries seriesTimeUsed, XYSeries seriesDistance, StoredPSConfig storedPSConfig) {
        this.seriesTimeUsed = seriesTimeUsed;
        this.seriesDistance = seriesDistance;
        this.storedPSConfig = storedPSConfig;

        compute();
    }

    public double getAverage() {
        return avgTimeUsed;
    }



    @Override
    public String toString() {

        return String.format("%s , Deaths=%d, Sucesses=%d, oot=%d, avgDist=%.2f, avgTime=%.2f ", storedPSConfig,deaths,successes,outOfTime,avgDistance, avgTimeUsed);
    }

    @Override
    public int compareTo(ConfigResultsScore o) {
        if (getAverage() < o.getAverage())
            return -1;
        if (getAverage() > o.getAverage())
            return 1;
        return 0;
    }


    public static Comparator<ConfigResultsScore> byFewestDeaths = (o1, o2) -> o1.deaths-o2.deaths;
    public static Comparator<ConfigResultsScore> byMostSuccesses = (o1, o2) -> o2.successes-o1.successes;

}
