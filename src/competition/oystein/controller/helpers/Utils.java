package competition.oystein.controller.helpers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by Oystein on 11/02/15.
 */
public class Utils {


    public static int booleanToInt(boolean[] a) {
        int n = 0;
        for (boolean anA : a) {
            n = (n << 1) + (anA ? 1 : 0);
        }
        return n;
    }

    public static boolean[] intToBinary(int number, int base) {
        final boolean[] ret = new boolean[base];
        for (int i = 0; i < base; i++) {
            ret[base - 1 - i] = (1 << i & number) != 0;
        }
        return ret;
    }


    public static void saveObjectToFile(Serializable obj, String filename) {
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(filename+".ser", false);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.flush();
                    oos.close();
                    System.out.println("Object "+obj+" saved to file "+filename);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
