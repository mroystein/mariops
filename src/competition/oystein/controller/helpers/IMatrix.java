package competition.oystein.controller.helpers;

import java.util.Random;

/**
 * Created by Oystein on 18/03/15.
 */
public interface IMatrix {
    Random random = new Random(System.currentTimeMillis());


    public double[] getActions(int state);

    public double getAction(int state, int action);

    public void setValue(int state, int action, double value);


    default double[] getRandomDoubleInitArr(double[] a) {
        for (int i = 0; i < a.length; i++) {
            a[i] = getRandomDoubleInit();
        }
        return a;
    }

    default double getRandomDoubleInit() {
        return random.nextDouble();
    }

    public void reset();
}
