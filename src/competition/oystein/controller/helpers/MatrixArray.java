package competition.oystein.controller.helpers;

/**
 * Created by Oystein on 18/03/15.
 */
public class MatrixArray implements IMatrix{

    double[][] array;

    int numStates;
    int numActions;

    public MatrixArray(int numStates, int numActions) {
        this.numStates = numStates;
        this.numActions = numActions;

        array= new double[numStates][numActions];
        reset();
    }


    @Override
    public double[] getActions(int state) {
        return array[state];
    }

    @Override
    public double getAction(int state, int action) {
        return array[state][action];
    }

    @Override
    public void setValue(int state, int action, double value) {
        array[state][action]=value;
    }

    @Override
    public void reset() {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j]= getRandomDoubleInit();
            }
        }
    }
}
