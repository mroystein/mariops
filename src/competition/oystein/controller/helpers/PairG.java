package competition.oystein.controller.helpers;

/**
 * Created by Oystein on 08/04/15.
 */
public class PairG<E,K> {
    E e;
    K k;

    public PairG(E e, K k) {
        this.e = e;
        this.k = k;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PairG pairG = (PairG) o;

        if (e != null ? !e.equals(pairG.e) : pairG.e != null) return false;
        if (k != null ? !k.equals(pairG.k) : pairG.k != null) return false;

        return true;
    }

    @Override
    public String toString() {
        return e.toString()+" "+k.toString();
    }

    @Override
    public int hashCode() {
        int result = e != null ? e.hashCode() : 0;
        result = 31 * result + (k != null ? k.hashCode() : 0);
        return result;
    }

    public E getE() {
        return e;
    }

    public void setE(E e) {
        this.e = e;
    }

    public K getK() {
        return k;
    }

    public void setK(K k) {
        this.k = k;
    }
}
