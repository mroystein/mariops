package competition.oystein.controller.helpers;

import competition.oystein.MarioRSAgentSimple;
import competition.oystein.controller.states.IState.RewardMethod;
import competition.oystein.ps.PSMario;
import competition.reinforcment.MarioQAgent;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Oystein on 03/02/15.
 */
public class StoredPSConfig implements Serializable{
    private static final long serialVersionUID = -880814313068402640L;
    public double gamma;
    public double damperGlow;
    public double reward;
    public  RewardMethod rewardMethod;
    public int stateClass;
    public String stateClassStr;
    public String name;

    public StoredPSConfig(PSMario ps, RewardMethod rewardMethod, int stateClass) {
        this.rewardMethod = rewardMethod;
        this.stateClass = stateClass;
        this.gamma=ps.gamma;
        this.damperGlow=ps.damperGow;
        this.reward=ps.rewardSuccess;
        this.name=ps.toString();
    }

    public StoredPSConfig(MarioRSAgentSimple agent)
    {

        this.rewardMethod =  agent.getCurrentState().rewardMethod;
        this.stateClass =  agent.getCurrentState().getStateClass();
        this.stateClassStr =  agent.getCurrentState().getName();
        this.gamma=agent.getPs().gamma;
        this.damperGlow=agent.getPs().damperGow;
        this.reward=agent.getPs().rewardSuccess;
        this.name=agent.toString();
    }

    public StoredPSConfig(MarioQAgent agent)
    {

        this.rewardMethod =  agent.getCurrentState().rewardMethod;
        this.stateClass =  agent.getCurrentState().getStateClass();
        this.stateClassStr =  agent.getCurrentState().getName();
//        this.gamma=agent.getPs().gamma;
//        this.damperGlow=agent.getPs().damperGow;
//        this.reward=agent.getPs().rewardSuccess;
        this.name=agent.toString();
    }




    public static void saveToFile(ArrayList<StoredPSConfig> list,String filename){
        try {
            FileOutputStream fos = new FileOutputStream(new File(filename));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public String toString() {
        return "sPSConfig{" +
                "gamma=" + gamma +
                ", damperGlow=" + damperGlow +
                ", reward=" + reward +
                ", rewardMethod=" + rewardMethod +", State:"+stateClassStr+
                '}';
    }

    public static ArrayList<StoredPSConfig> readFile(String filename){
        try {
            FileInputStream fis = new FileInputStream(new File(filename));
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object obj = ois.readObject();
            if(!(obj instanceof ArrayList )){
                System.err.println("NOT A LIST");
            }
            return (ArrayList<StoredPSConfig>) obj;

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
