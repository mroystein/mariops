package competition.oystein.controller.helpers;

/**
 * Created by oystein on 23/02/15.
 */
public class ConfigResultScoreAverage {

    public int deaths,successes,outOfTime,avgDistance,avgTimeUsed;

    int numAgents;

    public ConfigResultScoreAverage(int numAgents) {
        this.numAgents = numAgents;
    }

    public void add(ConfigResultsScore score){
        deaths+=score.deaths;
        successes+=score.successes;
        outOfTime+=score.outOfTime;
        avgDistance+=score.avgDistance;
        avgTimeUsed+=score.avgTimeUsed;
    }
    public void compute(){
        deaths/=numAgents;
        successes/=numAgents;
        outOfTime/=numAgents;
        avgDistance/=numAgents;
        avgTimeUsed/=numAgents;
    }


}
