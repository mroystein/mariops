package competition.oystein.controller;

import ch.idsia.benchmark.mario.engine.sprites.Mario;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by Oystein on 10/12/14.
 */
public enum MarioAction {
    DO_NOTHING(0),

    LEFT(1, Mario.KEY_LEFT),
    RIGHT(2, Mario.KEY_RIGHT),
    JUMP(3, Mario.KEY_JUMP),
    FIRE(4, Mario.KEY_SPEED),

    LEFT_JUMP(5, Mario.KEY_LEFT, Mario.KEY_JUMP),
    RIGHT_JUMP(6, Mario.KEY_RIGHT, Mario.KEY_JUMP),

    LEFT_FIRE(7, Mario.KEY_LEFT, Mario.KEY_SPEED),
    RIGHT_FIRE(8, Mario.KEY_RIGHT, Mario.KEY_SPEED),

    JUMP_FIRE(9, Mario.KEY_JUMP, Mario.KEY_SPEED),

    LEFT_JUMP_FIRE(10, Mario.KEY_LEFT, Mario.KEY_JUMP, Mario.KEY_SPEED),
    RIGHT_JUMP_FIRE(11, Mario.KEY_RIGHT, Mario.KEY_JUMP, Mario.KEY_SPEED);



    // Update the total number when adding new actions.
    public static final int TOTAL_ACTIONS = 12;

    private final int actionNumber;
    private final boolean[] action;

    private MarioAction(int actionNumber, int... keys) {
        this.actionNumber = actionNumber;

        this.action = new boolean[6];
        for (int key : keys) {
            this.action[key] = true;
        }
    }

    public int getActionNumber() {
        return actionNumber;
    }

    public boolean[] getAction() {
        return action;
    }

    public static boolean[] getAction(int actionNumber) {

        return MarioAction.values()[actionNumber].getAction();
    }
    public static String getName(int actionNumber) {
       return  MarioAction.values()[actionNumber].name();
    }

    public static MarioAction getMarioAction(boolean[] action){
        for (MarioAction ma: MarioAction.values()){

            if(Arrays.equals(ma.getAction(), action))
                return ma;

        }
        if(action[0] && action[1])
            return null;
//        if(Arrays.equals(action,leftAndRight))
//            return null;

        System.out.println("Unkown action?? ");
        return null;

    }
    static boolean[] leftAndRight= {true,true,false, false,false,false};

    public static void main(String[] args) {
        for (MarioAction ma: MarioAction.values()){

            StringBuilder sb = new StringBuilder();
            for (boolean b: ma.getAction())
                sb.append(b?"1":"0");
            System.out.println(ma.getActionNumber()+" "+ sb.toString());

        }
    }
    public static  int[] BASIC_ACTIONS = {
            MarioAction.RIGHT.getActionNumber(),
            MarioAction.LEFT.getActionNumber(),
            MarioAction.JUMP.getActionNumber(),
            MarioAction.FIRE.getActionNumber()
    };

    public static  MarioAction[] BASIC_ACTIONS_MARIO = {
            MarioAction.RIGHT,
            MarioAction.LEFT,
            MarioAction.JUMP,
            MarioAction.FIRE
    };
    public static  int[] ALL_ACTIONS = {0,1,2,3,4,5,6,7,8,9,10,11
//            MarioAction.RIGHT.getActionNumber(),
//            MarioAction.LEFT.getActionNumber(),
//            MarioAction.JUMP.getActionNumber(),
//            MarioAction.FIRE.getActionNumber(),
//            LEFT_JUMP_FIRE.getActionNumber(),
//            RIGHT_JUMP.getActionNumber(),
//            LEFT_FIRE.getActionNumber(),
//            RIGHT_FIRE.getActionNumber(),
//            JUMP_FIRE.getActionNumber(),
//            LEFT_JUMP_FIRE.getActionNumber(),
//            RIGHT_JUMP_FIRE.getActionNumber(),
    };

    public static String getNameFromInt(int[] a){
        boolean[] b = toBool(a);

        MarioAction marioAction = getMarioAction(b);
        if(marioAction==null)
            return "Unknown";
        return marioAction.name();
    }

    public static boolean[] toBool(int[] a){
        boolean[] b = new boolean[a.length];
        for (int i = 0; i < a.length; i++) {
            if(a[i]==0)
                b[i]=false;
            else if(a[i]==1)
                b[i]=true;
            else
                System.err.println("huge error");

        }
        return b;
    }

}
