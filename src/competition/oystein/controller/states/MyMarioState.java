package competition.oystein.controller.states;

import ch.idsia.benchmark.mario.engine.GeneralizerLevelScene;
import ch.idsia.benchmark.mario.engine.sprites.Sprite;
import ch.idsia.benchmark.mario.environments.Environment;

/**
 * Created by Oystein on 11/12/14.
 */
public class MyMarioState {


    boolean canJump; //0,1
    boolean[] obstacles; // [0,1,2,3]
    boolean isStuck; //0,1


    private Environment environment;
    private byte[][] scene;
    private static final int MARIO_X = 9;
    private static final int MARIO_Y = 9;

    private double currReward = 0.0;

    private int dDistance;
    private int lastDistance;
    private int dElevation, lastElevation;
    private int stuckLength = 0;
    private boolean[][] front;

    public  int nBits = 14; // bool variables
    public int nStates = 1 << nBits;
    StaticState staticState = new StaticState();

    boolean withEnemies;

    public MyMarioState(boolean withEnemies) {
        this.withEnemies = withEnemies;
        if (withEnemies) {
            nBits = 14;

        } else {
            nBits = 6;
        }
        nStates = 1 << nBits;


        System.out.println("Number of states :" + nStates + " (" + nBits + " bits)");
        int lengthSight = 4;//enemies
        obstacles = new boolean[4];
        front = new boolean[2][lengthSight];
    }

    public int getStateNumber() {
        return staticState.getStateNumber();
    }


    public void update(Environment environment) {
        this.environment = environment;
        this.scene = environment.getMergedObservationZZ(1, 1);

        int distance = environment.getEvaluationInfo().distancePassedPhys;
        dDistance = distance - lastDistance;
        lastDistance = distance;
        if (dDistance <= 0) {
            int maxStuckLength = 10;//avoid HUGE minus
            if (stuckLength <= maxStuckLength)
                stuckLength++;
        } else {
            stuckLength = 0;
        }

        int elevation = Math.max(0,
                getDistanceToGround(MARIO_X - 1) - getDistanceToGround(MARIO_X));
        dElevation = Math.max(0, elevation - lastElevation);
        lastElevation = elevation;

        canJump = canJump();
        isStuck = stuckLength>1;

        for (int y = 0; y < obstacles.length; y++) {
            obstacles[y] = isObstacle(MARIO_X + 1, MARIO_Y - y + 1);
        }

        if (withEnemies)
            findEnemies();



        calcState();


    }

    private void calcState(){
        boolean[] state = new boolean[nBits];
        int n = 0;
        state[n++] = canJump;
        state[n++] = isStuck;

        for (int i = 0; i < obstacles.length; i++) {
            state[n++] = obstacles[i];
        }

        // boolean[] a = {canJump,isStuck,obstacles[0],obstacles[1],obstacles[2],obstacles[3]};

        if (withEnemies) {
            for (int i = 0; i < front.length; i++) {
                for (int j = 0; j < front[i].length; j++) {
                    state[n++] = front[i][j];

                }
            }
        }
        staticState.setState(state);
    }

    @Override
    public String toString() {
        return staticState.toString();
    }



    private void findEnemies() {
        for (int i = 0; i < front.length; i++) {
            int y = MARIO_Y - i;
            for (int j = 0; j < front[i].length; j++) {
                int x = MARIO_X + 1 + j;
                front[i][j] = isEnemy(x, y);

            }
        }

    }

    private boolean isEnemy(int x, int y) {
        return scene[y][x] == Sprite.KIND_GOOMBA || scene[y][x] == Sprite.KIND_SPIKY;
    }

    public double calcReward() {
        currReward = dDistance * 2 + dElevation * 2 - stuckLength;
        return currReward;
    }

    private int getDistanceToGround(int x) {
        for (int y = MARIO_Y + 1; y < scene.length; y++) {
            if (isGround(x, y)) {
                return Math.min(3, y - MARIO_Y - 1);
            }
        }
        return -1;
    }

    private boolean isGround(int x, int y) {
        return isObstacle(x, y) || scene[y][x] == GeneralizerLevelScene.BORDER_HILL;
    }

    private boolean isObstacle(int x, int y) {
        switch (scene[y][x]) {
            case GeneralizerLevelScene.BRICK:
            case GeneralizerLevelScene.BORDER_CANNOT_PASS_THROUGH:
            case GeneralizerLevelScene.FLOWER_POT_OR_CANNON:
            case GeneralizerLevelScene.LADDER:
                return true;
        }
        return false;
    }

    public boolean canJump() {
        return environment.isMarioAbleToJump() || !environment.isMarioOnGround();

    }


    public static void realTest() {
        State state = new State(false);
        for (int i = 0; i <= 1; i++) {
            state.canJump = i != 0;
            for (int j = 0; j <= 1; j++) {
                state.isStuck = j != 0;
                for (int k = 0; k < 16; k++) {
                    state.obstacles = StaticState.toBinary(k, 4);

                    System.out.println(state.toString());
                }

            }
        }


    }







    public static void main(String[] args) {
//        State state = new State(false);
//        test();
        realTest();
//
//        boolean[] a = toBinary(4, 6);
//
//        System.out.println(toStringBoolArr(a));

//        System.out.println(state.toInt(new boolean[]{true, false, true}));
//        test2();
//        System.out.println();
//
//       boolean[] aa = new boolean[state.nBits];
//        aa[2]=true;

//        aa[63]=true;
//       boolean[] aa2= toBinary(1,64);
//      System.out.println(state.toInt(aa));
//        System.out.println(state.toInt(aa2));


    }


}
