package competition.oystein.controller.states;

import ch.idsia.benchmark.mario.environments.Environment;

/**
 * Created by Oystein on 02/02/15.
 */
public class DynamicState3 extends IState {

    int nStates = 20;
    boolean[] state = new boolean[nStates];

    public DynamicState3(RewardMethod rewardMethod) {
        this.rewardMethod = rewardMethod;
    }


    @Override
    public int getNumberOfStates() {
        return 1<<nStates;
    }

    @Override
    public int getStateNumber() {
        return toInt(state);
    }

    @Override
    public String getName() {
        return "DS3 " + rewardMethod;
    }


    @Override
    public void update(Environment environment) {
        super.update(environment);


        state[0] = isEnemy(8, 7) || isEnemy(8, 8) || isEnemy(9, 7) || isEnemy(9, 8); //behind
        state[1] = isEnemy(10, 8) || isEnemy(10, 9) || isEnemy(11, 8) || isEnemy(11, 9);//under left
        state[2] = isEnemy(10, 10) || isEnemy(10, 11) || isEnemy(11, 10) || isEnemy(11, 11);//under front
        state[3] = isEnemy(10, 12) || isEnemy(10, 13) || isEnemy(11, 12) || isEnemy(11, 13);//under front 2


        state[4] = isEnemy(8, 10) || isEnemy(8, 11) || isEnemy(9, 10) || isEnemy(9, 11);// front
        state[5] = isEnemy(8, 12) || isEnemy(8, 13) || isEnemy(9, 12) || isEnemy(9, 13);// front 2
        state[6] = isEnemy(8, 14) || isEnemy(8, 15) || isEnemy(9, 14) || isEnemy(9, 15);// front 2

        state[7] = isEnemy(6, 8) || isEnemy(6, 9) || isEnemy(7, 8) || isEnemy(7, 8);// above left
        state[8] = isEnemy(6, 10) || isEnemy(6, 11) || isEnemy(7, 10) || isEnemy(7, 11);// above front

        state[9] = canJump();
        state[10] = stuckLength < 25;

        state[11] = isObstacle(8, 7) || isObstacle(8, 8) || isObstacle(9, 7) || isObstacle(9, 8); //behind
        state[12] = isObstacle(10, 8) || isObstacle(10, 9) || isObstacle(11, 8) || isObstacle(11, 9);//under left
        state[13] = isObstacle(10, 10) || isObstacle(10, 11) || isObstacle(11, 10) || isObstacle(11, 11);//under front
        state[14] = isObstacle(10, 12) || isObstacle(10, 13) || isObstacle(11, 12) || isObstacle(11, 13);//under front 2


        state[15] = isObstacle(8, 10) || isObstacle(8, 11) || isObstacle(9, 10) || isObstacle(9, 11);// front
        state[16] = isObstacle(8, 12) || isObstacle(8, 13) || isObstacle(9, 12) || isObstacle(9, 13);// front 2
        state[17] = isObstacle(8, 14) || isObstacle(8, 15) || isObstacle(9, 14) || isObstacle(9, 15);// front 2

        state[18] = isObstacle(6, 8) || isObstacle(6, 9) || isObstacle(7, 8) || isObstacle(7, 8);// above left
        state[19] = isObstacle(6, 10) || isObstacle(6, 11) || isObstacle(7, 10) || isObstacle(7, 11);// above front




    }

    @Override
    public int getStateClass() {
        return 69;
    }




    @Override
    public String toStringStateNumber(int stateNumber) {
        return "no";
    }

    @Override
    public String toString() {
        return "DS3";
    }




}
