package competition.oystein.controller.states;

import ch.idsia.benchmark.mario.environments.Environment;

/**
 * Created by Oystein on 02/02/15.
 */
public class DynamicState extends IState {

    /*
     |-X--|
     |-M--|
     |-M--|
     |----|
     */
    int boardWidth = 4;
    int boardHeight = 4;
    boolean[][] board;

    public DynamicState() {
        board = new boolean[boardHeight][boardWidth];
    }

    public DynamicState(RewardMethod rewardMethod) {
        board = new boolean[boardHeight][boardWidth];
        this.rewardMethod = rewardMethod;
    }


    @Override
    public int getNumberOfStates() {
        return 1 << boardHeight * boardWidth; // 2^(h*w)
    }

    @Override
    public int getStateNumber() {
        boolean[] state = new boolean[boardWidth * boardHeight];
        int x = 0;
        for (int i = 0; i < boardHeight; i++) {

            for (int j = 0; j < boardWidth; j++) {
                state[x++] = board[i][j];

            }
        }
        return toInt(state);
    }

    @Override
    public String getName() {
        return "DS " + rewardMethod;
    }


    @Override
    public void update(Environment environment) {
        super.update(environment);
        reveseUpdate();
//        for (int i = 0; i < boardHeight; i++) {
//
//            for (int j = 0; j < boardWidth; j++) {
//                int sceneH = 7 + i; // 7,8,9,10
//                int sceneW = 8 + j;
//
//                board[i][j] = isObstacle(sceneH, sceneW);// || isEnemy(sceneH,sceneW);
//            }
//        }
//
//        //No point in Mario pos , can use if for other info.
//        board[1][1] = canJump();
//        board[2][1] = stuckLength < 25;
//
//
//        //row above
//        board[0][0] = isEnemy(7, 10) || isEnemy(7, 11) || isEnemy(7, 12);
//        //same row and above (row 9 and 8)
//        board[1][0] = isEnemy(9, 10) || isEnemy(9, 11) || isEnemy(9, 12) || isEnemy(8, 10) || isEnemy(8, 11) || isEnemy(8, 12);
//        //row under
//        board[2][0] = isEnemy(10, 10) || isEnemy(10, 11) || isEnemy(10, 12);

    }

    @Override
    public int getStateClass() {
        return 1;
    }

    private void reveseUpdate(){
        for (int i = 0; i < boardHeight; i++) {
            for (int j = 0; j < boardWidth; j++) {
                int sceneH = 7 + i; // 7,8,9,10
                int sceneW = 8 + j;//8,9,10,11
                board[i][j] = isEnemy(sceneH,sceneW);
            }
        }

        //No point in Mario pos , can use if for other info.
        board[1][1] = canJump();
        board[2][1] = stuckLength < 25;

        //row above
        board[0][0] = isObstacle(7, 10) || isObstacle(7, 11) || isObstacle(7, 12);
        //same row and above (row 9 and 8)
        board[1][0] = isObstacle(9, 10) || isObstacle(9, 11) || isObstacle(9, 12) || isObstacle(8, 10) || isObstacle(8, 11) || isObstacle(8, 12);
        //row under
        board[2][0] = isObstacle(10, 10) || isObstacle(10, 11) || isObstacle(10, 12);
    }


    public boolean[][] reverseStateNumberToBoard(int stateNumber) {
        boolean[] state = State.toBinary(stateNumber, boardWidth * boardHeight);
        boolean[][] board = new boolean[boardHeight][boardWidth];
        int ind = 0;
        for (int i = 0; i < boardHeight; i++) {
            for (int j = 0; j < boardWidth; j++) {
                board[i][j] = state[ind++];
            }
        }
        return board;
    }

    @Override
    public String toStringStateNumber(int stateNumber) {
        return toString(reverseStateNumberToBoard(stateNumber), false);
    }

    @Override
    public String toString() {
        return toString(this.board, true) + getStateNumber();
    }

    public String toString(boolean[][] board, boolean showMario) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < board.length; i++) {
            sb.append("|");
            for (int j = 0; j < board[i].length; j++) {

                if ((i == 0 || i == 1 || i == 2) && j == 0) {
                    sb.append(board[i][j] ? "E" : " ");
                    continue;
                }

                if (j == 1 && (i == 1 || i == 2)) {
                    if (showMario)
                        sb.append("M");
                    else {
                        sb.append(i == 1 ? "J" : "S");
                    }
                    continue;
                }

                if (board[i][j])
                    sb.append("X");
                else sb.append(" ");
            }
            sb.append("|\n");

        }

        return sb.toString();
    }


}
