package competition.oystein.controller.states.old;

import ch.idsia.benchmark.mario.engine.GeneralizerLevelScene;
import ch.idsia.benchmark.mario.engine.sprites.Mario;
import ch.idsia.benchmark.mario.engine.sprites.Sprite;
import ch.idsia.benchmark.mario.environments.Environment;
import competition.oystein.controller.MarioAction;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Oystein on 10/12/14.
 */
public class MarioState {

    Random random = new Random(System.currentTimeMillis());
    private Environment environment;
    private byte[][] scene;

    private static final int MARIO_X = 9;
    private static final int MARIO_Y = 9;
    private int distance;
    private boolean isStuck;
    private byte[][] smallScene;


    final boolean[] RIGHT = MarioAction.RIGHT.getAction();
    final boolean[] RIGHT_JUMP = MarioAction.RIGHT_JUMP.getAction();
    final boolean[] NOTHING = MarioAction.DO_NOTHING.getAction();
    private int marioMode;
    private byte[][] enemies;


    public boolean[] getAction(){
        boolean[] attack = attackEnemyActionSet();
        if(!Arrays.equals(attack,NOTHING)) {
            return attack;
        }

        return passThroughActionSet();


    }
    private boolean isMarioOnGround(){
       return false;
    }


    private boolean[] attackEnemyActionSet(){
        //2 == FIRE
        if(marioMode!=environment.getMarioMode())
            System.out.println("Mode "+marioMode+" -> "+environment.getMarioMode());
        marioMode = environment.getMarioMode();
        boolean canFire=marioMode==2;


        enemies = environment.getEnemiesObservationZ(1);


        if(isEnemy(MARIO_X + 2, MARIO_Y)){
            if(canJump()){
                return MarioAction.RIGHT_JUMP_FIRE.getAction();
            }
        }



        return NOTHING;
    }
    private boolean isEnemy(int x, int y){
        return enemies[y][x]==Sprite.KIND_GOOMBA || enemies[y][x]==Sprite.KIND_SPIKY;
    }




    private boolean[] passThroughActionSet(){
        //gap
        float[] floatPos = environment.getMarioFloatPos();//[x][y]
//        System.out.println("x="+floatPos[0]);
        int newDistance = environment.getEvaluationInfo().distancePassedPhys;
        isStuck = false;
        if(distance==newDistance){
            isStuck=true;
        }
        distance=newDistance;

        smallScene = smallScene(7);
        if(isStuck)
        printSmallScene();


        for (int x = MARIO_X  ; x < MARIO_X+5 ; x++) {
            if(isGap(x,12)) {
                if(x==MARIO_X+4){
                    return MarioAction.RIGHT_FIRE.getAction();
                }
                System.out.println("GAP");
                if(canJump()){
                    return MarioAction.RIGHT_JUMP_FIRE.getAction();
                }else{
                    return RIGHT;
                }

            }
        }


        //wait for flowers
        for (int x = MARIO_X+1; x < MARIO_X+4; x++) {
            //don't look at the same level. a bug shows it as -93 (Flower)
            for (int y = MARIO_Y-1   ; y >MARIO_Y-4 ; y--) {
                    if(isFlower(x,y))
                    {
//                        System.out.println("FLOWER. wait");
                        return NOTHING;
                    }
            }
        }





        if(isObstacle(MARIO_X+2,MARIO_Y)){
            if(canJump()){
                return MarioAction.RIGHT_JUMP_FIRE.getAction();
            }
        }

        if(isStuck)
            return jumpSmallBlock2();


        return MarioAction.RIGHT.getAction();
    }




    boolean[][] currJumpSeq=null;

    boolean[][][] jumpSeq = {{RIGHT_JUMP,RIGHT,RIGHT},
            {RIGHT_JUMP,RIGHT_JUMP,RIGHT,RIGHT},{RIGHT_JUMP,RIGHT_JUMP,RIGHT_JUMP,RIGHT,RIGHT}};

    int jumpSeqIndex;

    private boolean[] jumpSmallBlock2(){

        //start a new sequence
        if(currJumpSeq==null&& random.nextInt(2)==0){//&& random.nextInt(5)==0
            int seqNum =random.nextInt(jumpSeq.length);
            System.out.println("Starting sequence");
            currJumpSeq=jumpSeq[seqNum];
        }
        //do sequence
        if(currJumpSeq!=null){
            //is end of sequence?
            if(jumpSeqIndex>=currJumpSeq.length) {
                currJumpSeq = null;
                jumpSeqIndex=0;
                return NOTHING;
            }

            //do sequence
            return currJumpSeq[jumpSeqIndex++];

        }
        return NOTHING;

    }






    private byte scene(int x, int y){
        return scene[y][x];
    }

    private void printSmallScene(){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < smallScene.length; i++) {
            for (int j = 0; j < smallScene[i].length; j++) {
                sb.append(smallScene[i][j]);
                if(j<smallScene[i].length-1)
                    sb.append("\t");
            }
            sb.append("\n");
        }
        System.out.println();
        System.out.println(sb);



    }

    private byte[][] smallScene(int width){

        int start = scene.length/2 - width/2;
        int end = start+width;

        byte[][] s = new byte[width][width];
//        System.out.println(start+" to "+end);
        for (int i = 0; i <s.length; i++) {
            for (int j = 0; j < s[i].length; j++) {
                s[i][j]=scene[i+start][j+start];
                if(i+start==MARIO_X && j+start==MARIO_Y)
                    s[i][j]=1;
            }

        }

        return s;

    }

    private boolean[] jumpIfPossible(){
        if(canJump())
            return  MarioAction.JUMP.getAction();
        return MarioAction.RIGHT.getAction();
    }

    public void update(Environment environment){
        this.environment=environment;
        this.scene = environment.getMergedObservationZZ(1,1);

    }


    public boolean isFlower(int x,int y){
//        String enemyName = Sprite.getNameByKind(scene[y][x]);
//
//        if(!enemyName.equals("Unknown")){
//            if(scene[y][x]==   Sprite.KIND_SPIKY)
//            System.out.println("enemy "+enemyName+" at "+x+","+y);
//        }
        return scene[y][x]==Sprite.KIND_SPIKY;
    }


    private boolean isGap(int x, int y){
        //191 is ground

        float mariofloatX = environment.getMarioFloatPos()[0];
       if(mariofloatX<65.0)
           return false;

        return scene[y][x]==0;
    }

    private boolean isObstacle(int x, int y) {

        switch(scene[y][x]) {
            case GeneralizerLevelScene.BRICK:
            case GeneralizerLevelScene.BORDER_CANNOT_PASS_THROUGH:
            case GeneralizerLevelScene.FLOWER_POT_OR_CANNON:
            case GeneralizerLevelScene.LADDER:
                return true;
        }
        return false;
    }
    private boolean isEnemyOld(int x, int y) {

        switch(scene[y][x]) {
            case Sprite.KIND_GOOMBA:
            case Sprite.KIND_GREEN_KOOPA:
                return true;
        }
        return false;
    }

    public boolean canJump(){
        return environment.isMarioAbleToJump() || !environment.isMarioOnGround();

    }



}
