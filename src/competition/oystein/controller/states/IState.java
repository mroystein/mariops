package competition.oystein.controller.states;

import ch.idsia.benchmark.mario.engine.GeneralizerLevelScene;
import ch.idsia.benchmark.mario.engine.sprites.Sprite;
import ch.idsia.benchmark.mario.environments.Environment;
import ch.idsia.benchmark.mario.environments.MarioEnvironment;
import competition.oystein.controller.helpers.Pair;

/**
 * Created by Oystein on 01/02/15.
 */
public abstract class IState {

    //    public abstract IState(boolean withEnemies);
    public Environment environment;
    public byte[][] scene;

    public int dDistance;
    public int lastDistance;
    public int dElevation, lastElevation;
    public int stuckLength = 0;

    public static final int MARIO_X = 9;
    public static final int MARIO_Y = 9;

    public int lastCollisionsWithCreatues = 0;
    public int collisionWithCreatures = 0;


    public RewardMethod rewardMethod = RewardMethod.SIMPLE;

    public abstract int getNumberOfStates();


    public abstract int getStateNumber();

    public abstract String getName();

    //    int distCounter=10;
    int[] lastDistances = new int[50];
    int counter = 0;

    float lastMarioPosFloatX, dMarioPosFloatX;

    /*Direction mario is facing -1=left, 1=right ? not confirmed */
    int facing;

    public void update(Environment environment) {
        this.environment = environment;
        this.scene = environment.getMergedObservationZZ(1, 1);

//        int counter=  environment.getEvaluationInfo()
        counter++;

        lastDistances[counter % lastDistances.length] = environment.getEvaluationInfo().distancePassedPhys;


        dMarioPosFloatX = environment.getMarioFloatPos()[0] - lastMarioPosFloatX;
        lastMarioPosFloatX = environment.getMarioFloatPos()[0];
        MarioEnvironment me = (MarioEnvironment) environment;
//        if(me.getMario().facing)
        facing = me.getMario().facing;

        int distance = environment.getEvaluationInfo().distancePassedPhys;
        dDistance = distance - lastDistance;
        lastDistance = distance;

        if (dDistance <= 0) {
            int maxStuckLength = 100;//avoid HUGE minus
            if (stuckLength <= maxStuckLength)
                stuckLength++;
        } else {
            stuckLength = 0;
        }

        int elevation = Math.max(0,
                getDistanceToGround(MARIO_X - 1) - getDistanceToGround(MARIO_X));
        dElevation = Math.max(0, elevation - lastElevation);
        lastElevation = elevation;


        collisionWithCreatures = environment.getEvaluationInfo().collisionsWithCreatures - lastCollisionsWithCreatues;

//        if(collisionWithCreatures!=lastCollisionsWithCreatues)
//            System.out.println(String.format("Collision went from %d -> %d ",lastCollisionsWithCreatues,collisionWithCreatures));

        lastCollisionsWithCreatues = environment.getEvaluationInfo().collisionsWithCreatures;


    }

    public abstract int getStateClass();

    public void setRewardMethod(RewardMethod rewardMethod) {
        this.rewardMethod = rewardMethod;
    }


    public abstract String toStringStateNumber(int stateNumber);


    public static IState getStateFromStateNum(int n, RewardMethod rewardMethod) {
        switch (n) {
            case 0:
                return new State(false);
            case 1:
                return new DynamicState(rewardMethod);
            case 2:
                return new HardCodedState(rewardMethod);
            case 3:
                return new HardCodedState2(rewardMethod);
            default:
                return null;
        }
    }

    public double calcReward() {

        switch (rewardMethod) {
            case SIMPLE:
                return calcRewardSIMPLE();
            case SIMPLE_WITH_PUNISH:
                return calcRewardSIMPLE_WITH_PUNISH();
            case LIKE_EXAMPLE:
                return calcRewardLIKE_EXAMPLE();
            case EXAMPLE_WITH_PUNISH:
                return calcRewardEXAMPLE_WITH_PUNISH();
            case EXAMPLE_WITH_SMALLER_PUNISH:
                return calcRewardEXAMPLE_WITH_SMALLER_PUNISH();
            case PS_REWARD:
                return psRewardMethod();


            default:
                System.out.println("error. no calc method");
                return calcRewardSIMPLE();

        }


    }

    public double calcRewardEXAMPLE_WITH_SMALLER_PUNISH() {
        int stuckPunish = 0;
        if (stuckLength > 26)
            stuckPunish = stuckLength - 25;


        return dDistance * 2
                + dElevation * 8
                - stuckPunish * 5 - collisionWithCreatures * 800;
    }

    private double psRewardMethod() {
        if (collisionWithCreatures > 0) {
            return -collisionWithCreatures * 3;
        }
        if (dMarioPosFloatX > 1)
            return dMarioPosFloatX;


        if (stuckLength > 50)
            return -1;

        if (dElevation > 0 && dMarioPosFloatX > -1)
            return 1;



        return -1;


//        //shit
//        int currentStep= counter%lastDistances.length;
//        int last10= ((counter-(lastDistances.length-1)+lastDistances.length))%lastDistances.length;
//
//        int currDist=lastDistances[currentStep];
//        int last10Dist = lastDistances[last10];
//
//        if(currDist-4<=last10Dist) {
//            if(dDistance>0)
//                return 1;
//            return -1;
//        }
//
//        int stuckPunish = 0;
//        if (stuckLength > 25)
//            return -1;
//
//
//
//        if(dDistance>1)
//            return 1;
//        if(dDistance<=0)
//            return -1;
//        if(dElevation>0)
//            return 1;
//
//        return 0;
    }

    public double calcRewardSIMPLE_WITH_PUNISH() {
        int stuckPunish = 0;
        if (stuckLength > 20)
            stuckPunish = stuckLength - 4;

//        if(collisionWithCreatures>0){
//            System.out.println("collison");
//        }

        return dDistance * 4 + dElevation * 2 - stuckPunish * 10 - collisionWithCreatures * 5000;
    }

    public double calcRewardSIMPLE() {
        int stuckPunish = 0;
        if (stuckLength > 20)
            stuckPunish = stuckLength - 4;

        return dDistance * 4 + dElevation * 2 - stuckPunish * 10;
    }

    public double calcRewardLIKE_EXAMPLE() {
        int stuckPunish = 0;
        if (stuckLength > 25)
            stuckPunish = stuckLength - 25;


        return dDistance * 2
                + dElevation * 8
                - stuckPunish * 20;
    }

    public double calcRewardEXAMPLE_WITH_PUNISH() {
        int stuckPunish = 0;
        if (stuckLength > 25)
            stuckPunish = stuckLength - 25;


        return dDistance * 2
                + dElevation * 8
                - stuckPunish * 20 - collisionWithCreatures * 800;
    }


    public boolean isEnemy(int x, int y) {
        return scene[x][y] == Sprite.KIND_GOOMBA || scene[x][y] == Sprite.KIND_SPIKY;
    }

    public boolean isGround(int x, int y) {
        return isObstacle(x, y) || scene[y][x] == GeneralizerLevelScene.BORDER_HILL;
    }

    public boolean isObstacle(int x, int y) {
        switch (scene[x][y]) {
            case GeneralizerLevelScene.BRICK:
            case GeneralizerLevelScene.BORDER_CANNOT_PASS_THROUGH:
            case GeneralizerLevelScene.FLOWER_POT_OR_CANNON:
            case GeneralizerLevelScene.LADDER:
                return true;
        }

        return false;
    }

    public boolean canJump() {
        return environment.isMarioAbleToJump() || !environment.isMarioOnGround();

    }

    private int getDistanceToGround(int x) {
        for (int y = MARIO_Y + 1; y < scene.length; y++) {
            if (isGround(x, y)) {
                return Math.min(3, y - MARIO_Y - 1);
            }
        }
        return -1;
    }

    public int toInt(boolean[] a) {
        int n = 0, l = a.length;
        for (int i = 0; i < l; ++i) {
            n = (n << 1) + (a[i] ? 1 : 0);
        }
        return n;
    }


    protected boolean isEnemy(Pair[] pairs) {
        for (Pair p : pairs)
            if (isEnemy(p.x, p.y)) return true;
        return false;
    }

    protected boolean isEnemy(int[] a) {
        return isEnemy(a[0], a[1]);
    }

    protected boolean isEnemy(int[][] a) {
        for (int[] b : a)
            if (isEnemy(b)) return true;
        return false;
    }

    protected boolean isObstacle(Pair[] pairs) {
        for (Pair p : pairs)
            if (isObstacle(p.x, p.y)) return true;
        return false;
    }

    protected boolean isObstacle(int[] a) {
        return isObstacle(a[0], a[1]);
    }

    protected boolean isObstacle(int[][] a) {
        for (int[] b : a)
            if (isObstacle(b)) return true;
        return false;
    }

    public static enum RewardMethod {
        SIMPLE, LIKE_EXAMPLE, EXAMPLE_WITH_PUNISH, PS_REWARD, EXAMPLE_WITH_SMALLER_PUNISH, SIMPLE_WITH_PUNISH

    }

}
