package competition.oystein.controller.states;

import ch.idsia.benchmark.mario.engine.GeneralizerLevelScene;
import ch.idsia.benchmark.mario.environments.Environment;

/**
 * Created by Oystein on 11/12/14.
 */
public class State extends IState {


    protected boolean canJump; //0,1
    protected boolean[] obstacles; // [0,1,2,3]
    protected boolean isStuck; //0,1


//    private Environment environment;
//    private byte[][] scene;


    private double currReward = 0.0;


    private boolean[][] front;

    public  int nBits = 14; // bool variables
    public int nStates = 1 << nBits;
    boolean[] state = new boolean[nStates];

    boolean withEnemies;

    public State(boolean withEnemies) {
        this.withEnemies = withEnemies;
        if (withEnemies) {
            nBits = 14;

        } else {
            nBits = 6;
        }
        nStates = 1 << nBits;
        state = new boolean[nBits];

        System.out.println("Number of states :" + nStates + " (" + nBits + " bits)");
        int lengthSight = 4;//enemies
        obstacles = new boolean[4];
        front = new boolean[2][lengthSight];
    }

    public int getStateNumber() {
        return getStateNumber(this.state);
    }

    @Override
    public String getName() {
        return "State";
    }


    public int getStateNumber(boolean[] state) {
        int n = 0;
        state[n++] = canJump;
        state[n++] = isStuck;

        for (int i = 0; i < obstacles.length; i++) {
            state[n++] = obstacles[i];
        }


        // boolean[] a = {canJump,isStuck,obstacles[0],obstacles[1],obstacles[2],obstacles[3]};

        if (withEnemies) {
            for (int i = 0; i < front.length; i++) {
                for (int j = 0; j < front[i].length; j++) {
                    state[n++] = front[i][j];

                }
            }
        }
        return toInt(state);
    }



    public void update(Environment environment) {
       super.update(environment);//sets scene and environment

        canJump = canJump();
        isStuck = stuckLength>3;

        for (int y = 0; y < obstacles.length; y++) {
            obstacles[y] = isObstacle(MARIO_X, MARIO_Y +y+1);
        }

        if (withEnemies)
            findEnemies();


    }

    @Override
    public int getStateClass() {
        return 0;
    }

    @Override
    public String toStringStateNumber(int stateNumber) {
        return stateToString(toBinary(stateNumber,nBits));
    }

    public String toStringRealEnv(){

        int width=8;
        int height=5;

        int startH=scene.length/2-height/2;
        int endH=scene.length/2+height/2;

        int startW=scene[0].length/2-width/2;
        int endW=scene[0].length/2+width/2;

        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        for (int i = startH; i <=endH; i++) {

            sb.append("|");
            for (int j = startW; j <= endW; j++) {

                if(i==j && i==9){
                    sb.append("M");
                }
                else if(isObstacle(i,j)){
                    sb.append("X");
                } else if(scene[i][j]==GeneralizerLevelScene.BREAKABLE_BRICK){
                    sb.append("B");
                }
                else {
                    sb.append(" ");
                }

            }
            sb.append("|\n");

        }


        return sb.toString();
    }

    @Override
    public String toString() {
        return stateToString(this.state);
    }

    public String stateToString(boolean[] state) {
        StringBuilder sb = new StringBuilder();

        sb.append(getStateNumber(state));
        sb.append(" ");
        sb.append(toStringBoolArr(state));
        sb.append(" ");
        if (state[0]) sb.append("Stuck.");
        if (state[1]) sb.append("CanJump.");
        sb.append("[");
        for (int i = 2; i < state.length; i++) {
            if (state[i])
                sb.append("X");
            else sb.append(" ");
        }
        sb.append("] ");
        return sb.toString();
    }

    private void findEnemies() {
        for (int i = 0; i < front.length; i++) {
            int y = MARIO_Y - i;
            for (int j = 0; j < front[i].length; j++) {
                int x = MARIO_X + 1 + j;
                front[i][j] = isEnemy(x, y);

            }
        }
        /*
        for (int i = 1; i < front.length; i++) {
            for (int j = 0; j < front[i].length; j++) {
                if(front[i][j])
                    System.out.println("ENEYMY");
            }

        }*/


    }



    public double calcRewardEXAMPLE_WITH_PUNISH() {
        double stuckPunish = stuckLength<15? 0.0:2.0;
//        stuckPunish=0.0;

        currReward = dDistance * 2 + dElevation * 1 - stuckPunish;
        return currReward;
    }





    public static void realTest() {
        State state = new State(false);
        for (int i = 0; i <= 1; i++) {
            state.canJump = i != 0;
            for (int j = 0; j <= 1; j++) {
                state.isStuck = j != 0;
                for (int k = 0; k < 16; k++) {
                    state.obstacles = toBinary(k, 4);

                    System.out.println(state.toString());
                }

            }
        }


    }


    public static String toStringBoolArr(boolean[] arr) {
        StringBuilder sb = new StringBuilder();
        for (boolean b : arr) {
            sb.append(b ? "1" : "0");
        }
        return sb.toString();
    }

    public static boolean[] toBinary(int number, int base) {
        final boolean[] ret = new boolean[base];
        for (int i = 0; i < base; i++) {
            ret[base - 1 - i] = (1 << i & number) != 0;
        }
        return ret;
    }

    public static void test() {
        State state = new State(false);
        for (int i = 0; i < state.nStates; i++) {
            boolean[] b = toBinary(i, 6);

            state.state = b;

            int n = state.toInt(state.state);
            System.out.println(n + ": " + toStringBoolArr(b) + " " + state.stateToString(state.state));
        }
    }



    public static void main(String[] args) {
//        State state = new State(false);
//        test();
        realTest();

        boolean[] a = toBinary(4, 6);

        System.out.println(toStringBoolArr(a));

//        System.out.println(state.toInt(new boolean[]{true, false, true}));
//        test2();
//        System.out.println();
//
//       boolean[] aa = new boolean[state.nBits];
//        aa[2]=true;

//        aa[63]=true;
//       boolean[] aa2= toBinary(1,64);
//      System.out.println(state.toInt(aa));
//        System.out.println(state.toInt(aa2));


    }


    @Override
    public int getNumberOfStates() {
        return nStates;
    }
}
