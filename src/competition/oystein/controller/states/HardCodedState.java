package competition.oystein.controller.states;

import ch.idsia.benchmark.mario.environments.Environment;
import competition.oystein.controller.helpers.Pair;
import competition.oystein.controller.helpers.Utils;

/**
 * Created by Oystein on 10/02/15.
 */
public class HardCodedState extends IState {

    int nBooleanVars = 10;
    boolean[] state = new boolean[nBooleanVars];

    public HardCodedState(RewardMethod rewardMethod) {
        this.rewardMethod=rewardMethod;
    }

    public HardCodedState() {
    }

    public String toString(boolean[] state){
        StringBuilder sb = new StringBuilder();
        if(state[0])
            sb.append("J");
        if(state[1])
            sb.append("S");


        sb.append("[");
        for (int i = 2; i <= 5; i++) {
            sb.append(state[i]?"E":" ");
        }
        sb.append("]");

        sb.append("[");
        for (int i = 6; i <= 9; i++) {
            sb.append(state[i]?"B":" ");
        }
        sb.append("]");

//        if(state[10])
//            sb.append("High block in front.");

        return sb.toString();
    }
    @Override
    public void update(Environment environment) {
        super.update(environment);

        int c = 0;
        state[c++] = canJump();
//        state[c++]= environment.isMarioOnGround();
//        state[c++]= environment.isMarioAbleToJump();

        state[c++] = stuckLength < 25;


        //row above
        state[c++] = isEnemy(front_above);
        //same row and above (row 9 and 8)
        state[c++] = isEnemy(front_marioRow1);
        state[c++] = isEnemy(front_marioRow2);
        //row under
        state[c++] = isEnemy(front_under);


        //obstacle
        state[c++] = isObstacle(front_above);
        state[c++] = isObstacle(front_marioRow1);
        state[c++] = isObstacle(front_marioRow2);
        state[c++] = isObstacle(front_under);

        //big block infront
//        state[c++] = isObstacle(9,10) && isObstacle(8,10)&& isObstacle(7,10);

    }

    @Override
    public int getStateClass() {
        return 2;
    }

    @Override
    public String toStringStateNumber(int stateNumber) {
        return toString(Utils.intToBinary(stateNumber, nBooleanVars));
    }

    @Override
    public String toString() {
        return toString(state);
    }

    @Override
    public int getNumberOfStates() {
        return 1 << nBooleanVars; //2^ (#boolean vars)
    }

    @Override
    public int getStateNumber() {
        return toInt(state);
    }

    @Override
    public String getName() {
        return "HCS "+rewardMethod;
    }





    Pair[] front_above = {new Pair(7, 10), new Pair(7, 11), new Pair(7, 12)};
    Pair[] front_marioRow1 = {new Pair(8, 10), new Pair(8, 11), new Pair(8, 12)};
    Pair[] front_marioRow2 = {new Pair(9, 10), new Pair(9, 11), new Pair(9, 12)};
    Pair[] front_under = {new Pair(10, 10), new Pair(10, 11), new Pair(10, 12)};



    public static void main(String[] args) {

        System.out.println(1 << 10);
    }
}
