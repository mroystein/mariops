package competition.oystein.controller.states;

import ch.idsia.benchmark.mario.environments.Environment;
import competition.oystein.controller.helpers.Pair;

import java.util.Arrays;

/**
 * Created by Oystein on 10/02/15.
 */
public class PSGenStateSmall extends IState {

    public int[] dimensions = {2, 4, 4, 4};
    int nNumStates;
    int[] state = new int[4];

    int[][] unqieStates;

    public PSGenStateSmall(RewardMethod rewardMethod) {
        this();
        this.rewardMethod = rewardMethod;
    }

    private void hack(){
        unqieStates = new int[128][4];
        int unqNumbs = 0;
//        PSGenState s = new PSGenState(RewardMethod.PS_REWARD);

        for (int i = 0; i < dimensions[0]; i++) {
            for (int j = 0; j < dimensions[1]; j++) {
                for (int k = 0; k < dimensions[2]; k++) {
                    for (int l = 0; l < dimensions[3]; l++) {

                            int[] arr = {i, j, k, l};
                            unqieStates[unqNumbs]=arr;
                            unqNumbs++;

                    }
                }
            }
        }
//        System.out.println("duplicates : "+duplicates);
        System.out.println(unqNumbs);
    }

    public PSGenStateSmall() {
        nNumStates = dimensions[0];
        for (int i = 1; i < dimensions.length; i++) {
            nNumStates *= dimensions[i];
        }
//        System.out.println("num Steas: "+nNumStates);
        hack();
    }


    @Override
    public void update(Environment environment) {
        super.update(environment);
        // 3, 2, 4,4
        //currDirection
//        state[0] = 0;
//        if (dDistance < 0)
//            state[0] = 1;
//        if (dDistance > 0)
//            state[0] = 2;

        state[0] = canJump() ? 1 : 0;

        //blocks, distance
        state[1] = distToObstacle();
        state[2] = heightObstacle();
        state[3] = distToEnemy();
//        state[4] =facing==-1?0:1;


        //big block infront
//        state[c++] = isObstacle(9,10) && isObstacle(8,10)&& isObstacle(7,10);

    }

    public int unqInt(int[] state) {
        int a = state[0];

        int pow=1;
        for (int i = 1; i < state.length; i++) {
            pow+=dimensions[i-1]*pow;
            a += state[i] * (dimensions[i - 1]);
        }
        return a;
    }

    public static void main(String[] args) {
        int[][] unqieStates = new int[128][5];
        int unqNumbs = 0;
        PSGenStateSmall s = new PSGenStateSmall(RewardMethod.PS_REWARD);
        int duplicates = 0;
        int[] freq = new int[s.nNumStates];
//        for (int i = 0; i < s.dimensions[0]; i++) {
//            for (int j = 0; j < s.dimensions[1]; j++) {
//                for (int k = 0; k < s.dimensions[2]; k++) {
//                    for (int l = 0; l < s.dimensions[3]; l++) {
//                        for (int m = 0; m < s.dimensions[4]; m++) {
//                            int[] arr = {i, j, k, l, m};
//                            unqieStates[unqNumbs]=arr;
//                            unqNumbs++;
//                            int unq = s.unqInt(arr);
//                            if (freq[unq] != 0) {
//                                System.err.println("Duplicate " + unq + " " + Arrays.toString(arr));
//                                duplicates++;
//                            } else {
//                                freq[unq]++;
//                            }
//
//                        }
//                    }
//                }
//            }
//        }
        System.out.println("duplicates : "+duplicates);
        System.out.println(unqNumbs);

    }

    private int heightObstacle() {
        if (isObstacle(front_marioRow1))
            return 1;
        if (isObstacle(front_marioRow2))
            return 2;
        if (isObstacle(front_above))
            return 3;
        return 0; //no obstacles
    }

    private int distToObstacle() {
        if (isObstacle(front_col1))
            return 1;
        if (isObstacle(front_col2))
            return 2;
        if (isObstacle(front_col3))
            return 3;
        return 0; //no obstacles
    }

    private int distToEnemy() {
        if (isEnemy(front_col1))
            return 1;
        if (isEnemy(front_col2))
            return 2;
        if (isEnemy(front_col3))
            return 3;
        return 0; //no obstacles
    }


    Pair[] front_col1 = {new Pair(7, 10), new Pair(8, 10), new Pair(9, 10)};
    Pair[] front_col2 = {new Pair(7, 11), new Pair(8, 11), new Pair(9, 11)};
    Pair[] front_col3 = {new Pair(7, 12), new Pair(8, 12), new Pair(9, 12)};
    Pair[] front_col4 = {new Pair(7, 13), new Pair(8, 13), new Pair(9, 13)};


    @Override
    public int getStateClass() {
        return 9;
    }

    @Override
    public String toStringStateNumber(int stateNumber) {
        return "";
    }

    @Override
    public String toString() {

        return "";
    }

    @Override
    public int getNumberOfStates() {
        return nNumStates;

    }

    @Override
    public int getStateNumber() {
        for (int i = 0; i < unqieStates.length; i++) {
            if(Arrays.equals(unqieStates[i],state)) return i;
        }
        return -1;
    }

    public int[] getState() {
        return state;
    }

    @Override
    public String getName() {
        return "Generali for PS " + rewardMethod;
    }


    Pair[] front_above = {new Pair(7, 10), new Pair(7, 11), new Pair(7, 12)};
    Pair[] front_marioRow1 = {new Pair(8, 10), new Pair(8, 11), new Pair(8, 12)};
    Pair[] front_marioRow2 = {new Pair(9, 10), new Pair(9, 11), new Pair(9, 12)};
    Pair[] front_under = {new Pair(10, 10), new Pair(10, 11), new Pair(10, 12)};


}
