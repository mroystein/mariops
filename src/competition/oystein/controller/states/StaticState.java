package competition.oystein.controller.states;

/**
 * Created by Oystein on 15/12/14.
 */
public class StaticState {

    int nBits = 6;
    boolean[] state;
    int stateNumber;

    public StaticState() {
    }

    public StaticState(int stateNumber) {
        setStateNumber(stateNumber);
    }

    public StaticState(boolean[] state) {
        setState(state);
    }

    public void setStateNumber(int stateNumber) {
        this.stateNumber = stateNumber;
        state = toBinary(stateNumber, nBits);
    }

    public void setState(boolean[] state) {
        this.state = state;
        this.stateNumber = toInt(state);
    }

    public boolean[] getState() {
        return state;
    }

    public int getStateNumber() {
        return stateNumber;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append(stateNumber);
        sb.append(" ");
        sb.append(toStringBoolArr(state));
        sb.append(" ");
        if (state[0]) sb.append("Stck=Ye.");
        else  sb.append("Stck=No.");
        if (state[1]) sb.append("jmp=Ye.");
        else sb.append("jmp=No.");
        sb.append("[");
        for (int i = 2; i < state.length; i++) {
            if (state[i])
                sb.append("X");
            else sb.append(" ");
        }
        sb.append("] ");
        return sb.toString();


    }

    public static String toStringBoolArr(boolean[] arr) {
        StringBuilder sb = new StringBuilder();
        for (boolean b : arr) {
            sb.append(b ? "1" : "0");
        }
        return sb.toString();
    }

    public int toInt(boolean[] a) {
        int n = 0, l = a.length;
        for (int i = 0; i < l; ++i) {
            n = (n << 1) + (a[i] ? 1 : 0);
        }
        return n;
    }


    public static boolean[] toBinary(int number, int base) {
        final boolean[] ret = new boolean[base];
        for (int i = 0; i < base; i++) {
            ret[base - 1 - i] = (1 << i & number) != 0;
        }
        return ret;
    }

}
