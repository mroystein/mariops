package competition.oystein.controller.states;

import ch.idsia.benchmark.mario.environments.Environment;
import competition.oystein.controller.helpers.Pair;
import competition.oystein.controller.helpers.Utils;
import competition.oystein.ps.PSMario;

/**
 * Created by Oystein on 10/02/15.
 */
public class HardCodedState2 extends IState {

    int nBooleanVars = 17;
    boolean[] state = new boolean[nBooleanVars];

    public HardCodedState2(RewardMethod rewardMethod) {
        this.rewardMethod = rewardMethod;
    }

    public HardCodedState2() {
    }


    public String toString(boolean[] state) {
        StringBuilder sb = new StringBuilder();
        for (boolean b : state)
            sb.append(b ? "1" : "0");

        return sb.toString();
    }

    @Override
    public void update(Environment environment) {
        super.update(environment);

        int c = 0;
//        state[c++] = canJump();
        state[c++] = environment.isMarioOnGround();
        state[c++] = environment.isMarioAbleToJump();
        state[c++] = stuckLength < 25;


        //row above
        state[c++] = isEnemy(nA);
        state[c++] = isEnemy(nB);
        state[c++] = isEnemy(nC);

        state[c++] = isEnemy(nD);
        state[c++] = isEnemy(nE);
        state[c++] = isEnemy(nF);

        state[c++] = isEnemy(nG);


        //obstacle

//        state[c++] = isObstacle(front_row_marioRow1);
//        state[c++] = isObstacle(front_row_marioRow2);
//        state[c++] = isObstacle(front_row_above);

        state[c++] = isObstacle(nA);
        state[c++] = isObstacle(nB);
        state[c++] = isObstacle(nC);

        state[c++] = isObstacle(nD);
        state[c++] = isObstacle(nE);
        state[c++] = isObstacle(nF);

        state[c++] = isObstacle(nG);

        //big block infront
//        state[c++] = isObstacle(9,10) && isObstacle(8,10)&& isObstacle(7,10);

    }

    @Override
    public int getStateClass() {
        return 3;
    }

    @Override
    public String toStringStateNumber(int stateNumber) {
        return toString(Utils.intToBinary(stateNumber, nBooleanVars));
    }

    @Override
    public String toString() {
        return toString(state);
    }

    @Override
    public int getNumberOfStates() {
        return 1 << nBooleanVars; //2^ (#boolean vars)
    }

    @Override
    public int getStateNumber() {
        return toInt(state);
    }

    @Override
    public String getName() {
        return "HCSv2 " + rewardMethod;
    }


    Pair[] front_col1_top = {new Pair(7, 10), new Pair(8, 10)};
    Pair[] front_col1_bottom = {new Pair(9, 10), new Pair(10, 10)};


    Pair[] front_col1 = {new Pair(7, 10), new Pair(8, 10), new Pair(9, 10), new Pair(10, 10)};
    Pair[] front_col2 = {new Pair(7, 11), new Pair(8, 11), new Pair(9, 11), new Pair(10, 11)};
    Pair[] front_col3 = {new Pair(7, 12), new Pair(8, 12), new Pair(9, 12), new Pair(10, 12)};
    Pair[] front_col4 = {new Pair(7, 13), new Pair(8, 13), new Pair(9, 13), new Pair(10, 13), new Pair(11, 13)};

    Pair[] back_col1 = {new Pair(7, 9), new Pair(8, 9), new Pair(9, 9), new Pair(10, 9)};

    Pair[] front_row_above = {new Pair(7, 10), new Pair(7, 11), new Pair(7, 12)};
    Pair[] front_row_marioRow1 = {new Pair(8, 10), new Pair(8, 11), new Pair(8, 12)};
    Pair[] front_row_marioRow2 = {new Pair(9, 10), new Pair(9, 11), new Pair(9, 12)};
    Pair[] front_row_under = {new Pair(10, 10), new Pair(10, 11), new Pair(10, 12)};


    int[][] nA = {{8, 10}, {9, 10}};
    int[][] nB = {{10, 9}, {10, 10},
            {11, 9}, {11, 10}};
    int[][] nC = {{6, 9}, {6, 10}, {7, 9}, {7, 10}};

    int[][] nD = {{8, 11}, {9, 11}};

    int[][] nG = {{6, 11}, {6, 12},
            {7, 11}, {7, 12}};

    int[][] nE = {{10, 11}, {10, 12},
            {11, 11}, {11, 12}};

    int[][] nF = {{8, 12}, {8, 13},
            {9, 12}, {9, 13}};

    public static void main(String[] args) {

        getHowManyExitedGlips(0.9);
    }


    public static void getHowManyExitedGlips(double damperGlow) {
        double d = 1.0;
        int n = 1000000000;
        for (int i = 0; i < n; i++) {

            // g[i][j] = g[i][j] - damperGow * g[i][j];
            if (d > PSMario.DAMPER_GLOW_LOW)
                System.out.println(i + " : " + d);
            else {
                System.out.println("BROKE at " + i);
                break;
            }
            d -= damperGlow * d;


        }
    }

    private static void damperGlowTesting() {
        System.out.println(9.0e-3);
        System.out.println(9.0e-4);
        System.out.println(9.0e-5);

        double glowQuit = 0.00000000000000000001d;

        double glowQuit2 = 0.0;//0.001d;

        double d = 0.90;
        d = 0.1;
        double g = 1.0;

        int n = 1000000000;
        for (int i = 0; i < n; i++) {
            if (g > 0.01)
                System.out.println(i + " : " + g);
            g = g - d * g;
            if (g < glowQuit2) {
                System.out.println("BROKE at " + i);
                break;
            }


        }
        System.out.println("loop done " + n);
        System.out.println(g);
    }
}
