package competition.oystein.controller;

import ch.idsia.agents.controllers.human.HumanKeyboardAgent;
import ch.idsia.benchmark.mario.environments.Environment;
import competition.oystein.controller.states.HardCodedState;
import competition.oystein.controller.states.IState;

/**
 * Created by Oystein on 01/02/15.
 */
public class MyHumanAgentDebug extends HumanKeyboardAgent {

    IState state ;
    public MyHumanAgentDebug() {
        super();
        state = new HardCodedState();
        System.out.println("My Human");
    }



    String prevState;

    @Override
    public void integrateObservation(Environment environment) {

        state.update(environment);
//        System.out.println("Reward: "+state.calcReward());

        String currState =state.toString();
        if(!currState.equals(prevState)) {
//            System.out.println(currState);
            prevState=currState;



            System.out.println(state.toString());


//            String dist = String.format("dpp:%d , cells:%d",environment.getEvaluationInfo().distancePassedPhys,environment.getEvaluationInfo().distancePassedCells);
//            System.out.println(dist);
//            System.out.println(state.toStringRealEnv());
        }




        super.integrateObservation(environment);
    }
}
