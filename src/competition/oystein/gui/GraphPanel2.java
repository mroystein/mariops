package competition.oystein.gui;

import competition.oystein.ps.v3.ClipGen;
import competition.oystein.ps.v3.EdgeGen;
import org.jgraph.JGraph;
import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.DirectedGraph;
import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedMultigraph;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

public class GraphPanel2 {
    private static final long serialVersionUID = 3256444702936019250L;
    public static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
    //public static final Dimension DEFAULT_SIZE = new Dimension(530, 320);

    public static final Color COLOR_COMP_NODE = Color.decode("#213177");
    public static final Color COLOR_PERC_NODE = Color.decode("#218777");
    public static final Color COLOR_ACTION_NODE = Color.decode("#F53357");
    private ArrayList<ArrayList<ClipGen>> clipLayers;
    //    private ECM ecm;
    private int WIDHT;
    private int HEIGHT;

//    private final Clip[] percClips;
//    private final ActuatorClip[] actionClips;
ListenableGraph<ClipGen, MyWeightedEdge> g;

    //
    private JGraphModelAdapter<ClipGen, MyWeightedEdge> jgAdapter;
    private JGraph jgraph;

    public GraphPanel2(ArrayList<ArrayList<ClipGen>> clipLayers, int WIDHT, int HEIGHT) {
        this.clipLayers = clipLayers;
//        this.ecm = ecm;
        this.WIDHT = WIDHT;
        this.HEIGHT = HEIGHT;

//        this.percClips = ecm.perceptClips;
//        this.actionClips = ecm.actuatorClips;
        generateGraph();
    }




    private void generateGraph() {
        //new ListenableDirectedMultigraph<>(MyWeightedEdge.class);
        // create a JGraphT graph
        //ListenableDirectedMultigraph<Clip,MyWeightedEdge> ldm= new ListenableDirectedMultigraph<>(MyWeightedEdge.class);
        //ListenableGraph<Clip,MyWeightedEdge> g = ldm;

        /*ListenableGraph<ClipGen, MyWeightedEdge> g =
                new ListenableDirectedMultigraph<>(
                        MyWeightedEdge.class);*/

        g =
                new ListenableDirectedMultigraph<>(
                        MyWeightedEdge.class);

        // create a visualization using JGraph, via an adapter
        jgAdapter = new JGraphModelAdapter<>(g);

        jgraph = new JGraph(jgAdapter);


        adjustDisplaySettings(jgraph);


        for (ArrayList<ClipGen> clipLayer : clipLayers) {
            for (ClipGen clipGen : clipLayer) {
                g.addVertex(clipGen);

            }
        }

//        for (ClipGen clip : clipLayers.get(0)) {
//            g.addVertex(clip);
//        }
//        for (ClipGen clip : clipLayers.get(clipLayers.size()-1)) {
//            g.addVertex(clip);
//        }



        for (ArrayList<ClipGen> clipLayer : clipLayers) {

            for (ClipGen clipGen : clipLayer) {

                for (EdgeGen e : clipGen.edges) {

                    MyWeightedEdge myWeightedEdge = new MyWeightedEdge(e);
                    g.addEdge(e.start, e.end, myWeightedEdge);


                }
            }
        }

        int widthCount=100;
        int heightCount=100;
        int row=0;
        int col=0;
        for (ArrayList<ClipGen> clipLayer : clipLayers) {
            col=0;
            row++;
            for (ClipGen clipGen : clipLayer) {
                col++;
                Color color = COLOR_PERC_NODE;
                if(clipGen.isActionClip)
                    color=COLOR_ACTION_NODE;
                if(clipGen.isWildCardClip)
                    color=COLOR_COMP_NODE;

                positionVertexAt(clipGen, widthCount*col+10, heightCount*row+20, color);

            }
        }

//        for (ClipGen clip : clipLayers.get(0)) {
//
//            for (EdgeGen e : clip.edges) {
//
//                MyWeightedEdge myWeightedEdge = new MyWeightedEdge(e);
//                g.addEdge(e.start, e.end, myWeightedEdge);
//
//
//            }
//        }

//
//        for (ClipGen clip : clipLayers.get(0)) {
//            positionVertexAt(clip, widthCount, 10, COLOR_PERC_NODE);
//            widthCount+=30;
//        }

//        positionVertexAt(percClips[0], 10, 10, COLOR_PERC_NODE);
//        positionVertexAt(percClips[1], WIDHT - 150, 10, COLOR_PERC_NODE);
//
//        positionVertexAt(percClips[2], 10, 370, COLOR_PERC_NODE);
//        positionVertexAt(percClips[3], WIDHT - 150, 370, COLOR_PERC_NODE);


//        int middleX = (WIDHT - 150) / 2;
//
//        for (ClipGen clip : clipLayers.get(clipLayers.size()-1)) {
//            positionVertexAt(clip, widthCount, 10, COLOR_PERC_NODE);
//            widthCount+=30;
//        }

//        positionVertexAt(actionClips[0], middleX, 10, COLOR_ACTION_NODE);
//        positionVertexAt(actionClips[1], middleX, 120, COLOR_ACTION_NODE);

//


    }


    public void updateEdges() {


//        for (Clip perceptClip : ecm.perceptClips) {
//
//            for (Edge e : perceptClip.edges) {
//
//                if (!g.containsEdge(e.start, e.end))
//                    g.addEdge(e.start, e.end, new MyWeightedEdge(e));
//
//            }
//
//
//        }
    }

    List<ClipGen> oldCompClips = new ArrayList<>();

    public void update() {
//        System.out.println(ecm.toString());
//
//        for (CompositionClip clip : ecm.compClips) {
//            if (!oldCompClips.contains(clip)) {
//                oldCompClips.add(clip);
//                g.addVertex(clip);
//                positionVertexAt(clip, WIDHT-80, 80+oldCompClips.size()*50, COLOR_COMP_NODE);
//                for (Edge e : clip.edges) {
//
//                    MyWeightedEdge myWeightedEdge = new MyWeightedEdge(e);
//                    g.addEdge(e.start, e.end, myWeightedEdge);
//
//
//
//                }
//            }
//        }
//        updateEdges();


        jgraph.refresh();

    }


    public JGraph getGraph() {
        return jgraph;

    }

    private void applyEdgeDefaults() {



//        // Settings for edges
//        Map<String, Object> edge = new HashMap<String, Object>();
//        edge.put(mxConstants.STYLE_ROUNDED, true);
//        edge.put(mxConstants.STYLE_ORTHOGONAL, false);
//        edge.put(mxConstants.STYLE_EDGE, "elbowEdgeStyle");
//        edge.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_CONNECTOR);
//        edge.put(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_CLASSIC);
//        edge.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
//        edge.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
//        edge.put(mxConstants.STYLE_STROKECOLOR, "#000000"); // default is #6482B9
//        edge.put(mxConstants.STYLE_FONTCOLOR, "#446299");
//
//        mxStylesheet edgeStyle = new mxStylesheet();
//        edgeStyle.setDefaultEdgeStyle(edge);
//        graph.setStylesheet(edgeStyle);
    }

    private void adjustDisplaySettings(JGraph jg) {
        //jg.setPreferredSize(DEFAULT_SIZE);

        Color c = DEFAULT_BG_COLOR;
        jg.setBackground(c);


    }
    public void changeEdge(MyWeightedEdge e){
        DefaultEdge edgeCell = jgAdapter.getEdgeCell(e);
//        AttributeMap attr = cell.getAttributes();
//        edgeCell.at
    }

    @SuppressWarnings("unchecked") // FIXME hb 28-nov-05: See FIXME below
    private void positionVertexAt(Object vertex, int x, int y, Color color) {
        DefaultGraphCell cell = jgAdapter.getVertexCell(vertex);
        AttributeMap attr = cell.getAttributes();

        Rectangle2D bounds = GraphConstants.getBounds(attr);

        Rectangle2D newBounds =
                new Rectangle2D.Double(
                        x,
                        y,
                        bounds.getWidth(),
                        bounds.getHeight());


        GraphConstants.setBounds(attr, newBounds);
        GraphConstants.setBackground(attr, color);

        // TODO: Clean up generics once JGraph goes generic
        AttributeMap cellAttr = new AttributeMap();
        cellAttr.put(cell, attr);
        //jgAdapter.get
        jgAdapter.edit(cellAttr, null, null, null);
    }


    /**
     * a listenable directed multigraph that allows loops and parallel edges.
     */
    private static class ListenableDirectedMultigraph<V, E>
            extends DefaultListenableGraph<V, E>
            implements DirectedGraph<V, E> {
        private static final long serialVersionUID = 1L;

        ListenableDirectedMultigraph(Class<E> edgeClass) {
            super(new DirectedMultigraph<V, E>(edgeClass));
        }
    }

    class MyWeightedEdge extends DefaultWeightedEdge {
        private EdgeGen e;

        public MyWeightedEdge(EdgeGen e) {
            this.e = e;
        }

        @Override
        protected Object getSource() {
            return e.start;
        }

        @Override
        protected Object getTarget() {
            return e.end;
        }

        @Override
        protected double getWeight() {
            return e.getWeight();
        }

        @Override
        public String toString() {
//            if (e.weight <= 1.01) return "";
            double prop =e.start.getHoppingPropabilityToClip(e.end);

            return   String.format("%.0f%%", prop * 100);

//            return (e.emotion ? ":)" : ":(") + " " + String.format("%.1f", e.weight) + " (" + MyUtils.toStrAsPropability(e.start.getHoppingPropability(e.end)) + ")";
        }
    }

    public static void main(String[] args) {



    }


}
