package competition.oystein.gui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by Oystein on 23/10/14.
 */
public class SimpleSwingGraphGUI extends JFrame {


    private ChartPanel chartPanel;
    private JLabel jLabel_steps;


    public SimpleSwingGraphGUI() {

    }

    public SimpleSwingGraphGUI(List<Double> frequencies) {
        XYSeries series = new XYSeries("Frequencies");
        for (int i = 0; i < frequencies.size(); i++) {
            series.add(i,frequencies.get(i ));
        }
        init(series);

    }

    //Needed to differicate list<double> from list<integer>
    public SimpleSwingGraphGUI(List<Double> frequencies, String plotName, String headerName,String xLabel, String yLabel, int yRangeStart, int yRangeEnd,boolean uncessary) {
        XYSeries series = new XYSeries(plotName);
        for (int i = 0; i < frequencies.size(); i++) {
            series.add(i,frequencies.get(i ));
        }
        XYSeriesCollection ds = new XYSeriesCollection();
        ds.addSeries(series);
        init(ds,headerName,xLabel,yLabel,yRangeStart,yRangeEnd);

    }

    public SimpleSwingGraphGUI(List<Integer> frequencies, String plotName, String headerName,String xLabel, String yLabel, int yRangeStart, int yRangeEnd) {
        XYSeries series = new XYSeries(plotName);
        for (int i = 0; i < frequencies.size(); i++) {
            series.add(i,frequencies.get(i ));
        }
        XYSeriesCollection ds = new XYSeriesCollection();
        ds.addSeries(series);
        init(ds,headerName,xLabel,yLabel,yRangeStart,yRangeEnd);

    }

    public SimpleSwingGraphGUI(List<XYSeries> serieses, String headerName,String xLabel, String yLabel, int yRangeStart, int yRangeEnd) {
        XYSeriesCollection ds = new XYSeriesCollection();
        for (XYSeries series: serieses) {
            ds.addSeries(series);
        }
        init(ds,headerName,xLabel,yLabel,yRangeStart,yRangeEnd);

    }



    private void init(ArrayList<XYSeries> serieses){
        init(serieses.toArray(new XYSeries[serieses.size()]));
    }

    private void init(XYSeriesCollection ds, String headerName, String xLabel, String yLabel, int yRangeStart, int yRangeEnd) {
        setLayout(new BorderLayout());

        JFreeChart chart = ChartFactory.createXYLineChart(headerName, xLabel, yLabel, ds, PlotOrientation.VERTICAL, true, true, false);

        XYPlot xyPlot = chart.getXYPlot();
        ValueAxis axis = xyPlot.getRangeAxis();
        axis.setRange(yRangeStart, yRangeEnd);
        chartPanel = new ChartPanel(chart);
        chartPanel.setInitialDelay(1);
        add(chartPanel, BorderLayout.CENTER);
        chartPanel.setChart(chart);

        setMinimumSize(new Dimension(600, 500));
        setLocation(0,300);
        setVisible(true);
        setFocusable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
    private void init(XYSeries... serieses) {
        XYSeriesCollection ds = new XYSeriesCollection();
        for (XYSeries series : serieses)
            ds.addSeries(series);
        init(ds,"Graf","Score","Games",0,200);
    }


    public SimpleSwingGraphGUI(XYSeries... serieses) {
        init(serieses);


    }

    public SimpleSwingGraphGUI(ArrayList<XYSeries> serieses) {
        init(serieses);
    }


    public void setChart(JFreeChart chart) {
        if (chartPanel == null) {
            chartPanel = new ChartPanel(chart);
            chartPanel.setInitialDelay(1);
            add(chartPanel, BorderLayout.CENTER);

        }
        chartPanel.setChart(chart);
    }


    public void updateChart() {
        chartPanel.setChart(generateChart());


    }


    private JFreeChart generateChart(XYSeries... serieses) {

        //generateChart(new ArrayList<XYSeries>(serieses.))
        XYSeriesCollection ds = new XYSeriesCollection();


        for (XYSeries series : serieses) {
            ds.addSeries(series);
        }

        JFreeChart chart = ChartFactory.createXYLineChart("Learning curve", "trials", "Average steps", ds, PlotOrientation.VERTICAL, true, true, false);

        XYPlot xyPlot = chart.getXYPlot();
        //xyPlot.set
        ValueAxis axis = xyPlot.getRangeAxis();
        axis.setRange(0, 250);

        return chart;
    }


    private JPanel getTopPanel() {
//        JPanel p = new JPanel(new MigLayout());
//        JButton b_reset = new JButton("Train");
//        b_reset.addActionListener(e -> {
//
//            train();
//            updateChart();
//        });
//
//
//        jLabel_steps = new JLabel("Steps = ");
//
//
//        JButton b_startAI = new JButton("Run");
//        b_startAI.addActionListener(e ->{
//
//
//
//            System.out.println("done");
//
//        });
//
//        JButton b_stop = new JButton("Stop");
//        b_stop.addActionListener(e ->{
//
//
//
//        });
//        p.add(b_reset, "wrap");
//        p.add(b_startAI,"wrap");
//        p.add(b_stop,"wrap");
//        p.add(jLabel_steps,"wrap");
        return null;
    }


    public void start() {

        EventQueue.invokeLater(SimpleSwingGraphGUI::new);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(SimpleSwingGraphGUI::new);
    }
}
