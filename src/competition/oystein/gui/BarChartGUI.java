package competition.oystein.gui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Oystein on 19/02/15.
 */
public class BarChartGUI  extends JFrame{


    public BarChartGUI() {

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(1.0, "Row 1", "Column 1");
        dataset.addValue(5.0, "Row 1", "Column 2");
        dataset.addValue(3.0, "Row 1", "Column 3");
        dataset.addValue(2.0, "Row 2", "Column 1");
        dataset.addValue(3.0, "Row 2", "Column 2");
        dataset.addValue(2.0, "Row 2", "Column 3");
        init(dataset,"Demo");



    }
    public BarChartGUI( DefaultCategoryDataset dataset,String title) {
        init(dataset,title);
    }

    private void init( DefaultCategoryDataset dataset, String title) {
        setLayout(new BorderLayout());

        JFreeChart chart = ChartFactory.createBarChart(
                title, // chart title
                "Category", // domain axis label
                "Value", // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                true, // include legend
                true, // tooltips?
                false // URLs?
        );

        ChartPanel chartPanel = new ChartPanel(chart, false);
        chartPanel.setPreferredSize(new Dimension(500, 270));
//        setContentPane(chartPanel);

        add(chartPanel, BorderLayout.CENTER);
        chartPanel.setChart(chart);

        setMinimumSize(new Dimension(600, 500));
        setLocation(300,300);
        setVisible(true);
//        setFocusable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }




    public void start() {

        EventQueue.invokeLater(BarChartGUI::new);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(BarChartGUI::new);
    }
}
