package competition.oystein.ps;

import competition.oystein.ps.v2.I_ECM;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 25/02/15.
 */
public class ECM {

    Clip[] perceptClips;
    ActuatorClip[] actuatorClips;

    List<CompositionClip> compClips;

    double gamma = 0.00001;
    double reward = 1;

    List<Edge> lastUsedEdges;
    private int reflectionTime;
    int currentReflection = 0;
    //    private PerceptClip lastUsedPercept;
    int deliberationLength;

    public ECM(int numPercept, int numActions, int deliberationLength, int reflectionTime) {
        if (reflectionTime < 1) System.err.println("ReflectionTIme");
        if (deliberationLength < 1) System.err.println("D");

        this.reflectionTime = reflectionTime;
        this.deliberationLength = deliberationLength;


        actuatorClips = new ActuatorClip[numActions];
        for (int i = 0; i < numActions; i++) {
            actuatorClips[i] = new ActuatorClip(i);
        }
        perceptClips = new PerceptClip[numPercept];
        compClips = new ArrayList<>();
        lastUsedEdges = new ArrayList<>();


    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("--------------- ECM -----------------\n");
        for (Clip perceptClip : perceptClips) {
            if (perceptClip == null)
                continue;
            sb.append(perceptClip).append("\n");
            for (Edge e : perceptClip.edges) {
                sb.append("\t").append(e).append("\n");
            }
        }
        for (CompositionClip compClip : compClips) {
            if (compClip == null)
                continue;
            sb.append(compClip).append("\n");
            for (Edge e : compClip.edges) {
                sb.append("\t").append(e).append("\n");
            }
        }


        sb.append("-------------------------------------\n");
        return sb.toString();
    }

    public int getAction(int percept) {
        currentReflection = 0;
//        if(deliberationLength==2){
//            if(percept==0 || percept==1) return 0;
//            return 1;
//        }


        //if unseen percept, create new
        if (perceptClips[percept] == null) {
            PerceptClip perceptClip = new PerceptClip(percept);
            perceptClip.initDefaultActions();
            perceptClips[percept] = perceptClip;

        }

        lastUsedEdges.clear();

        Clip clip = perceptClips[percept];
        Edge edge;
        int debugLoopCount = 0;
        do {
            edge = clip.getStrongestEdge();
            debugLoopCount++;
            if (debugLoopCount > 5) {
                System.out.println("WTF");
            }
            lastUsedEdges.add(edge);
            clip = edge.end;

        } while (!(clip instanceof ActuatorClip));

        return edge.end.id;
    }


    public void giveReward(boolean success) {
        if (lastUsedEdges.size() > 1) {
//            System.out.println("Going to damp : " + lastUsedEdges + " : " + success);
        }


        for (Clip c : perceptClips) {
            if (c == null) continue;
            for (Edge e : c.edges) {
                if (lastUsedEdges.contains(e))
                    e.giveReward(success);
                else e.damp();
            }
        }
        for (Clip c : compClips) {
            for (Edge e : c.edges) {
                if (lastUsedEdges.contains(e) && success)
                    e.giveReward(success);
//                else e.damp();
            }
        }

//        for (Edge e : lastUsedEdges)
//            e.damp(success);


        if (deliberationLength < 2)
            return;

        if (lastUsedEdges.size() != 1)
            return;


        //if this is a really good edge. consider making it composition
        Edge edge = lastUsedEdges.get(0);

        if (edge.weight < 40)
            return;

        double hoppingProb =edge.start.getHoppingPropability(edge.end);
        if(hoppingProb<0.99)
            return;
        if (edge.start instanceof CompositionClip)
            return; //We don't allow comp clips to form new comp clips

        if (!(edge.end instanceof ActuatorClip))
            return; //We don't allow percept -> comp to make new comp

        PerceptClip start = (PerceptClip) edge.start;
//        System.out.println(edge.end.c);
        ActuatorClip end = (ActuatorClip) edge.end;

        int newId = perceptClips.length + start.id;
        StringBuilder sb = new StringBuilder();
        sb.append("We allready had theses comp clips:\n");
        for (PerceptClip clip : compClips) {

            if (clip.id == newId)
                return;

            Edge edge1 = clip.getStrongestEdge();
            if (edge1.end.id == end.id)
                return;
            sb.append("\t Loop checking : " + edge1 + " is not the same as " + end.id + " \n");
        }

        CompositionClip newComp = new CompositionClip(newId, start);

        compClips.add(newComp);
        Edge newEdge = new Edge(start, newComp, edge.weight + 100);
        start.addEdge(newEdge);

//        System.out.printf("Making ficinous clip %s , since the %s is strong\n", newComp, edge);

        sb.append("But we decided to add clip : " + newComp).append("\n");
        sb.append("And the edge : " + newEdge);
        if (compClips.size() > 2) {
            System.out.println("----------------- Error ---------------------");
            System.out.println(sb.toString());
            for (CompositionClip clip : compClips)
                System.out.println(clip + " " + clip.getStrongestEdge());
            System.out.println("----------------- Error End ---------------------");
        }
//        System.out.println("Making a ficitonus clip  id="+newId+" : pointing at action "+edge.end);

//        System.out.println("\n");
    }


    class Clip {
        public int id;
        Random random = new Random(System.currentTimeMillis());
        List<Edge> edges;

        public Clip(int id) {
            this.id = id;
        }

        public void addEdge(Edge e) {
            edges.add(e);
        }

        public Edge getStrongestEdge() {

            double total = 0.0;
            for (Edge edge : edges)
                total += edge.weight;

            double p = random.nextDouble();
            double cumulativeProbability = 0.0;
            for (int i = 0; i < edges.size(); i++) {
                cumulativeProbability += edges.get(i).weight / total;
                if (p < cumulativeProbability) {
                    if (!edges.get(i).emotion && currentReflection < reflectionTime - 1) {
                        currentReflection++;
                        getStrongestEdge();
                    }
                    return edges.get(i);
                }
            }

            return null;
        }
        public double getHoppingPropability(Clip toClip){

            double total = 0.0;
            for (Edge edge : edges)
                total += edge.weight;
            for (Edge edge : edges){
                if(edge.end.equals(toClip))
                    return edge.weight/total;
            }
            System.out.println("Not connected getHopping()");
            return 0.0;
        }

//        @Override
//        public boolean equals(Object obj) {
//            if(!(obj instanceof Clip))
//                return false;
//            Clip other = (Clip) obj;
//            return this.id==other.id;
//        }
    }

    class PerceptClip extends Clip {

        public PerceptClip(int id) {
            super(id);
            edges = new ArrayList<>();
            for (ActuatorClip actuatorClip : actuatorClips) {
                edges.add(new Edge(this, actuatorClip, 1));
            }
        }

        public void initDefaultActions() {

            for (CompositionClip compositionClip : compClips) {
                edges.add(new Edge(this, compositionClip, 1.0));
            }
        }


        @Override
        public String toString() {
            return "PerceptClip " + id;
        }


    }

    class CompositionClip extends PerceptClip {

        public CompositionClip(int id, PerceptClip copy) {
            super(id);

//            System.out.println(edges.size()+" "+copy.edges.size());
            for (int i = 0; i < edges.size(); i++) {

                edges.get(i).weight = copy.edges.get(i).weight;
            }
        }

        @Override
        public String toString() {
            return "CompClip " + id;
        }
//        public CompositionClip(int id) {
//            super(id);
//        }
    }

    class ActuatorClip extends Clip {
        public ActuatorClip(int id) {
            super(id);
        }

        @Override
        public String toString() {
            return "ActionClip " + id;
        }
    }


    class Edge {
        Clip start;
        Clip end;
        boolean emotion = true;

        //        int id;
        double weight;

        public Edge(Clip start, Clip end, double weight) {
            this.weight = weight;
            this.end = end;
            this.start = start;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Edge))
                return false;
            Edge other = (Edge) obj;
            boolean my = start.equals(other.start) && end.equals(other.end);
            boolean objEq = super.equals(obj);
            if (my != objEq) {
                System.out.println("WTF");
            }
            return objEq;
        }

        public void damp() {
            weight = weight + gamma * (weight - 1);

        }

        public void giveReward(boolean success) {
            if (success)
                weight = weight + gamma * (weight - 1) + reward;
            else
                weight = weight + gamma * (weight - 1);
//            System.out.println("new weight : "+weight +""+success);
            emotion = success;
        }

        @Override
        public String toString() {
            String smiley = emotion ? ":)" : ":(";
            return String.format("{E: %s -> %s  w=%.2f %s }", start, end, weight, smiley);
//        return "{E: "+start+" -> "+end+" w="+String.format("%.2f",weight);
//        return "E: to clip " + end.id + " w=" + weight;
        }

    }
//    @Override
//    public Clip[] getPerceptClips() {
//        return perceptClips;
//    }
//    @Override
//    public ActuatorClip[] getActuatorClips() {
//        return actuatorClips;
//    }
//    @Override
//    public List<CompositionClip> getCompClips() {
//        return compClips;
//    }

    public static void main(String[] args) {

//        printPerceptTraining(50);
        test();
    }

    public static void test() {
        int D = 2;
        int reflection = 2;
        ECM ecm = new ECM(4, 2, D, reflection);

        Random random = new Random(System.currentTimeMillis());
        int[] percepts = {0, 1, 2, 3};
        int[] actions = {0, 1};

//        System.out.println(ecm);


        //train only 2 actions
        System.out.println("\t Training with 2 percepts only");
        for (int i = 0; i < 200; i++) {
            int percept = percepts[random.nextBoolean() ? 1 : 2];
            int action = ecm.getAction(percept);
            boolean isCorrect = isCorrect(percept, action);
            ecm.giveReward(isCorrect);
//            System.out.println(String.format("P:%d -> A:%d : %s", percept, action, isCorrect));
        }
        System.out.println(ecm);

        for (int i = 0; i < 5; i++) {
            int percept2 = percepts[0];
            int action2 = ecm.getAction(percept2);
            boolean isCorrect2 = isCorrect(percept2, action2);
            ecm.giveReward(isCorrect2);
            System.out.println(String.format("P:%d -> A:%d : %s", percept2, action2, isCorrect2));
        }


        System.out.println("\t Training with all percepts");
        for (int i = 0; i < 200; i++) {

            int percept = percepts[random.nextInt(percepts.length)];
            int action = ecm.getAction(percept);
            ecm.giveReward(isCorrect(percept, action));
        }

        System.out.println(ecm);

        System.out.println("\t Results");
        for (int i = 0; i < percepts.length; i++) {

            int percept = percepts[i];
            int action = ecm.getAction(percept);
            boolean isCorrect = isCorrect(percept, action);
            ecm.giveReward(isCorrect);
            System.out.println(String.format("P:%d -> A:%d : %s", percept, action, isCorrect));

        }

        ecm.getAction(1);

    }


    public static void printPerceptTraining(int numEpochs) {
        ECM ecm = new ECM(4, 2, 1, 1);

        for (int i = 0; i < numEpochs; i++) {
            double avg = 0;
            for (int percept = 0; percept < 4; percept++) {
                int action = ecm.getAction(percept);
                boolean correct = isCorrect(percept, action);
                ecm.giveReward(correct);
                avg += correct ? 1 : 0;

            }
            avg /= 4;
            System.out.println(i + " " + avg);
        }

    }

    public static boolean isCorrect(int percept, int action) {
        if (percept == 0 || percept == 1)
            return action == 0;
        return action == 1;
    }
}
