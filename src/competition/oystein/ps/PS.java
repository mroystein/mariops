package competition.oystein.ps;

import java.util.List;
import java.util.Random;
/**
 * Created by Oystein on 24/10/14.
 */
public class PS<E,A> {

    /**
     * A matrix implementation of PS ECM.
     * The clip network are represented as a matrix.
     * <p>
     * <p>
     * Created by Oystein on 02/10/2014.
     */

        public final static int EMOTION_POSITIVE = 1, EMOTION_NEGATIVE = -1;

        private Random random = new Random(System.currentTimeMillis() + new Random().nextInt());
        private final int reflectionTime;
        private final double rewardSuccess;
        private final double gamma;
        /**
         * Defines how far back recently used edges should get of the reward.
         */
        private final double damperGow;

        double h[][], g[][];
        int[][] emotions;

        private final List<E> percepts;
        private final List<A> actions;

        //private int lastSelectedPerc = 0, lastSelectedAction = 0;
        private boolean useSoftMax = true;


        public PS(List<E> percepts, List<A> actions, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
            this.reflectionTime = reflectionTime;
            this.rewardSuccess = rewardSuccess;
            this.gamma = gamma;
            this.damperGow = damperGow;
            this.percepts = percepts;
            this.actions = actions;


            h = new double[percepts.size()][actions.size()];
            g = new double[percepts.size()][actions.size()];
            emotions = new int[percepts.size()][actions.size()];

            for (int i = 0; i < h.length; i++) {
                for (int j = 0; j < h[i].length; j++) {
                    h[i][j] = 1.0;
                    g[i][j] = 0.0;
                }
            }


        }


        public A getAction(E percept) {
            //Runtime : O(|actions|^2)
            int index = percepts.indexOf(percept);
            if (index == -1) {
                System.out.println("Not found!");
                return null;
            }


            //double p = ProjectionSimulationGen.random.nextDouble();
            double p = random.nextDouble();

            double cumulativeProbability = 0.0;
            for (int i = 0; i < h[index].length; i++) {

                cumulativeProbability += getHoppingPropability(index, i);
                if (p <= cumulativeProbability) {
//                lastSelectedPerc = index;
//                lastSelectedAction = i;
                    g[index][i]=1.0;
                    return actions.get(i);
                }
            }

            System.out.println("no actions. Should never happen.");
            return null;
        }


        public void giveReward(boolean success) {

            //g[lastSelectedPerc][lastSelectedAction] = 1.0;

            double reward = 0.0;
            if (success)
                reward = rewardSuccess;

            for (int i = 0; i < h.length; i++) {
                for (int j = 0; j < h[i].length; j++) {


                    h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];

                    g[i][j] = g[i][j] - damperGow * g[i][j];


                    if (g[i][j] < 0.0)
                        g[i][j] = 0.0;
                }
            }

        }


        public void useSoftMax(boolean useSoftMax) {
            this.useSoftMax=useSoftMax;
        }

        private double getHoppingPropability(int perceptInd, int actionInd) {
            if (useSoftMax)
                return softMax(h[perceptInd], h[perceptInd][actionInd]);

            return getHoppingPropabilityFormulaOne(perceptInd, actionInd);
        }

        private double getHoppingPropabilityFormulaOne(int perceptInd, int actionInd) {
            double prop = h[perceptInd][actionInd];
            double total = 0.0;
            for (int i = 0; i < h[perceptInd].length; i++) {
                total += h[perceptInd][i];
            }
            return prop / total;
        }

        @Override
        public String toString() {
            return "G: γ=" + gamma + ", |S/A|=" + percepts.size() + ",λ=" + rewardSuccess + ", R=" + reflectionTime+", g="+damperGow;

    }


    public static double softMax(double[] arr,double a){

        return Math.exp((a-logSumOfExponentials(arr)));
    }

    public static double logSumOfExponentials(double[] xs) {
        if (xs.length == 1) return xs[0];
        double max = maximum(xs);
        double sum = 0.0;
        for (int i = 0; i < xs.length; ++i)
            if (xs[i] != Double.NEGATIVE_INFINITY)
                sum += Math.exp(xs[i] - max);
        return max + Math.log(sum);
    }

    private static double maximum(double[] xs) {
        double currMax = Double.MIN_VALUE;
        for (double x : xs)
            if (x > currMax) currMax = x;
        return currMax;
    }
    private static double maximumJava7(double[] xs) {
        double currMax = Double.MIN_VALUE;
        for (int i = 0; i < xs.length; i++) {

            if (xs[i] > currMax) {
                currMax = xs[i];
            }
        }
        return currMax;
    }

}
