package competition.oystein.ps;

import ch.idsia.utils.wox.serial.ArrayListTest;
import competition.oystein.gui.SimpleSwingGraphGUI;
import competition.oystein.ps.v2.ECM2;
import competition.oystein.ps.v2.I_ECM;
import org.jfree.data.xy.XYSeries;


import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 25/02/15.
 */
public class EcmTestGame {

    public EcmTestGame() {

//        single();
        test();
    }

    public void single() {
        ECM ecm = new ECM(4, 2, 2, 1);


        ArrayList<XYSeries> serieses = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            serieses.add(new XYSeries("Perception " + i));
        }
        for (int i = 0; i < 1000; i++) {

            for (int percept = 0; percept < 4; percept++) {
                int action = ecm.getAction(percept);
                boolean correct = isCorrectMod(percept, action);
                ecm.giveReward(correct);
                serieses.get(percept).add(i, correct ? 1 : 0);
            }
        }
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(serieses, "ECM test", "Time", "SuccessRate", 0, 2));

    }


    public void test() {
        List<XYSeries> seriesList = new ArrayList<>();
        int numAgents = 50000;
        int[] deliberationLengths = {
//                1,
                 2};
        int[] reflectionTimes = {
//                1,
                10};
        for (int d : deliberationLengths) {
            for (int reflection : reflectionTimes)
                for (int version = 1; version <= 2; version++) {
                    if(version==2){
                        seriesList.add(trainAgent(numAgents, d, reflection,version,0));
                        seriesList.add(trainAgent(numAgents, d, reflection,version,0.1));
                        seriesList.add(trainAgent(numAgents, d, reflection,version,1));
                        seriesList.add(trainAgent(numAgents, d, reflection,version,2));

                    }
//                    seriesList.add(trainAgent(numAgents, d, reflection,version));
                }

        }


        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(seriesList, "ECM test", "Time", "SuccessRate", 50, 100));
        System.out.println("Done");
    }

    public XYSeries trainAgent(int numAgents, int d, int reflectionTime, int version, double K) {
        int numAddingNewPercepts = 2;
        double[] successRate = new double[200 * numAddingNewPercepts];
        for (int i = 0; i < numAgents; i++) {

            I_ECM ecm;
//            if(version==1)
//                ecm=new ECM(4,2,d,reflectionTime);
//            else
                ecm=new ECM2(4,2,d-1,reflectionTime,K);

            List<Boolean> successes = trainOneECM(ecm,numAddingNewPercepts);
            for (int j = 0; j < successes.size(); j++) {
                successRate[j] += (successes.get(j) ? 1 : 0) * 100;
            }

        }

        XYSeries series = new XYSeries("D=" + d + " r=" + reflectionTime+" v="+version+" "+ (version==2?("k="+K+""):""));
        for (int j = 0; j < successRate.length; j++) {
            series.add(j, successRate[j] / numAgents);
        }
        return series;
    }


    public List<Boolean> trainOneECM(I_ECM ecm, int numAdding) {
        int perceptsAddedEachRound = 2;
        int timePerEpisode = 200;
        Random random = new Random(System.currentTimeMillis());

        int[] percepts = new int[perceptsAddedEachRound * numAdding];
        for (int i = 0; i < percepts.length; i++) {
            percepts[i] = i;
        }
        int[] actions = {0, 1};
        int[][] perceptsStage= {{0,1},{2,3}};

        int currNumberOfPercepts = 0;
            currNumberOfPercepts-=random.nextInt(2);
//        ECM ecm = new ECM(percepts.length, actions.length, deliberationLength, reflectionTime);
//        ECM2 ecm = new ECM2(percepts.length, actions.length, deliberationLength-1, reflectionTime);


        //train only 2 actions
        ArrayList<Boolean> histo = new ArrayList<>();
        for (int j = 0; j < numAdding; j++) {
            currNumberOfPercepts += perceptsAddedEachRound;
//            System.out.println("Training with "+currNumberOfPercepts+" percepts");
            for (int i = 0; i < timePerEpisode; i++) {

                int percept =  perceptsStage[j][random.nextInt(2)];
                int action = ecm.getAction(percept);
                boolean isCorrect = isCorrectMod(percept, action);
                ecm.giveReward(isCorrect);
                histo.add(isCorrect);

            }
        }


        return histo;

    }
    public List<Boolean> trainOneECM(int deliberationLength, int numAdding, int reflectionTime) {
        int perceptsAddedEachRound = 2;
        int timePerEpisode = 200;
        Random random = new Random(System.currentTimeMillis());

        int[] percepts = new int[perceptsAddedEachRound * numAdding];
        for (int i = 0; i < percepts.length; i++) {
            percepts[i] = i;
        }
        int[] actions = {0, 1};

        int currNumberOfPercepts = 0;
//        ECM ecm = new ECM(percepts.length, actions.length, deliberationLength, reflectionTime);
        ECM2 ecm = new ECM2(percepts.length, actions.length, deliberationLength-1, reflectionTime);


        //train only 2 actions
        ArrayList<Boolean> histo = new ArrayList<>();
        for (int j = 0; j < numAdding; j++) {
            currNumberOfPercepts += perceptsAddedEachRound;
//            System.out.println("Training with "+currNumberOfPercepts+" percepts");
            for (int i = 0; i < timePerEpisode; i++) {

                int percept = percepts[random.nextInt(currNumberOfPercepts)];
                int action = ecm.getAction(percept);
                boolean isCorrect = isCorrectMod(percept, action);
                ecm.giveReward(isCorrect);
                histo.add(isCorrect);

            }
        }


        return histo;

    }


    public boolean[] trainOneECM(int deliberationLength) {
        int part1 = 200, part2 = 200;
        boolean[] histo = new boolean[part1 + part2];
        Random random = new Random(System.currentTimeMillis());
        int[] percepts = {0, 1, 2, 3};
        int[] actions = {0, 1};

        ECM ecm = new ECM(percepts.length, actions.length, deliberationLength, 1);


        //train only 2 actions

        for (int i = 0; i < part1; i++) {
            int percept = percepts[random.nextBoolean() ? 1 : 2];
            int action = ecm.getAction(percept);
            boolean isCorrect = isCorrect(percept, action);
            ecm.giveReward(isCorrect);
            histo[i] = isCorrect;
        }
        //train with all
        for (int i = 0; i < 200; i++) {

            int percept = percepts[random.nextInt(percepts.length)];
            int action = ecm.getAction(percept);
            boolean isCorrect = isCorrect(percept, action);
            ecm.giveReward(isCorrect);
            histo[i + part1] = isCorrect;
        }

//        for (int i = 0; i < percepts.length; i++) {
//
//            int percept = percepts[i];
//            int action = ecm.getAction(percept);
//            boolean isCorrect = isCorrect(percept,action);
//            ecm.giveReward(isCorrect);
//            System.out.println(String.format("P:%d -> A:%d : %s",percept,action,isCorrect));
//
//        }

        return histo;

    }

    public static boolean isCorrect(int percept, int action) {
        if (percept == 0 || percept == 1)
            return action == 0;
        return action == 1;
    }

    public static boolean isCorrectMod(int percept, int action) {

        return percept % 2 == action;
    }


    public static void main(String[] args) {
        new EcmTestGame();
    }
}
