package competition.oystein.ps;

import competition.oystein.controller.*;
import competition.oystein.controller.helpers.Pair;
import competition.oystein.controller.helpers.StoredPSConfig;
import competition.oystein.controller.helpers.Utils;
import competition.oystein.controller.states.IState;
import competition.oystein.controller.states.State;
import competition.oystein.controller.states.StaticState;
import competition.reinforcment.ILearningAlgorithm;

import java.io.Serializable;
import java.util.*;

/**
 * A matrix implementation of PS ECM.
 * The clip network are represented as a matrix.
 * <p>
 * <p>
 * Created by Oystein on 02/10/2014.
 */
public class PSMario implements ILearningAlgorithm, Serializable {
    public static final double DAMPER_GLOW_LOW = 0.0000000001;
//    public final static int EMOTION_POSITIVE = 1, EMOTION_NEGATIVE = -1;

    protected Random random = new Random(System.currentTimeMillis() + new Random().nextInt());
    protected int reflectionTime;

    public double rewardSuccess;
    public double gamma;
    /**
     * Defines how far back recently used edges should get of the reward.
     */
    public double damperGow;

    public double h[][], g[][];

    boolean[][] emotions;
    int currReflectionTime = 0;

    int[] debugStateCounter;

    protected int lastSelectedPerc = 0, lastSelectedAction = 0;
    protected boolean useSoftMax = true;


    int[][] lastTimeUsed;
    int currentTimeStep;

    LinkedList<Pair> exitedEdges = new LinkedList<>();
    public boolean uniqueExitedList = false;

    int debugBiggestList = 100; //To see how long the exitedList can get

    public PSMario(int numPercepts, int numActions, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        this.reflectionTime = reflectionTime;
        this.rewardSuccess = rewardSuccess;
        this.gamma = gamma;
        this.damperGow = damperGow;

        h = new double[numPercepts][numActions];
        g = new double[numPercepts][numActions];
        emotions = new boolean[numPercepts][numActions];
        lastTimeUsed = new int[numPercepts][numActions];

        debugStateCounter = new int[numPercepts];
        reset();
        validateParamters();
    }

    protected void validateParamters(){
        StringBuilder sb = new StringBuilder();
        if(reflectionTime<1)
            sb.append("Reflection Time must be 1 or bigger. Currently=").append(reflectionTime).append("\n");

        if(gamma<0.0 || gamma>1.0)
            sb.append("Gamma must be in the interval [0,1]. Currently=").append(gamma).append("\n");
        if(damperGow<0.0 || damperGow>1.0)
            sb.append("Damper Glow must be in the interval [0,1]. Currently=").append(damperGow).append("\n");
        System.err.println(sb.toString());


    }

    public PSMario(int numPercepts, int numActions, StoredPSConfig config) {
        this(numPercepts, numActions, config.gamma, config.reward, 1, config.damperGlow);
    }

    public PSMario() {
    }

    public void reset() {
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {
                h[i][j] = 1.0;
                g[i][j] = 0.0;
                emotions[i][j] = true;
            }
        }
    }

    @Override
    public int getAction(int percept) {
        debugStateCounter[percept]++;
        double p = random.nextDouble();

        double[] actionsCopy = h[percept];
        double cumulativeProbability = 0.0;
        for (int i = 0; i < h[percept].length; i++) {

            cumulativeProbability += getHoppingPropability(percept, i);
            if (p <= cumulativeProbability) {
                if(!emotions[percept][i] && currReflectionTime<reflectionTime) {
                    currReflectionTime++;
                    return getAction(percept);
                }


                lastSelectedPerc = percept;
                lastSelectedAction = i;
                g[percept][i] = 1.0;

                //new


                Pair pair = new Pair(percept, i);
                if (uniqueExitedList) {
                    if (!exitedEdges.contains(pair))
                        exitedEdges.add(pair);
                } else
                    exitedEdges.add(pair);

                lastTimeUsed[percept][i] = currentTimeStep;
                currentTimeStep++;
                return i;
            }
        }


        //debug shit
        double[] posActions = h[percept];
        System.out.println("no actions. Should never happen.");
        return -1;
    }

    @Override
    public void update(double reward, int currentStateNumber) {
        giveRewardAmount(reward);
    }

    double debugMAXERROR = 10e100;


    public void giveReward(boolean success) {
        double reward = 0.0;
        if (success)
            reward = rewardSuccess;
        System.out.println("Outdated");

        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {


                h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];
//                if(Double.isNaN(h[i][j])){
//                    System.out.println("REALL THA FUCK");
//                }
//
//                if(h[i][j]>debugMAXERROR){
//                    System.out.println("DA FUCK, whay to big numbers "+h[i][j]);
//                }

                g[i][j] = g[i][j] - damperGow * g[i][j];


                if (g[i][j] < 0.0)
                    g[i][j] = 0.0;
            }
        }

    }


    public void giveRewardAmount(double reward) {
        if (exitedEdges.size() > debugBiggestList) {
            debugBiggestList *= 2;
            System.err.println("Exited edges size : " + exitedEdges.size() + ". Not good!!");
        }
        if (rewardSuccess >= 1.0) {
            if (reward < 0)
                reward = 0;
            else reward = rewardSuccess;

        }


        if (exitedEdges.size() > debugBiggestList) {
//            debugBiggestList = exitedEdges.size();
//            System.out.println(debugBiggestList);

        }
        Iterator<Pair> it = exitedEdges.iterator();
        int i, j;
        while (it.hasNext()) {
            Pair p = it.next();

            i = p.x;
            j = p.y;
            h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];
            g[i][j] = g[i][j] - damperGow * g[i][j];

            if (h[i][j] < 0.0)
                h[i][j] = 0.0;


            if (g[i][j] < DAMPER_GLOW_LOW) {
                g[i][j] = 0.0;
                it.remove();
            }


        }


    }


    public void useSoftMax(boolean useSoftMax) {
        this.useSoftMax = useSoftMax;
    }


    protected double getHoppingPropability(int perceptInd, int actionInd) {
        return MyUtils.softMaxIgnoreNegative(h[perceptInd], h[perceptInd][actionInd]);
    }
//    protected double getHoppingPropability(int perceptInd, int actionInd) {
//        if (useSoftMax)
//            return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);
//
//        return getHoppingPropabilityFormulaOne(perceptInd, actionInd);
//    }

    protected double getHoppingPropabilityFormulaOne(int perceptInd, int actionInd) {
        double prop = h[perceptInd][actionInd];
        double total = 0.0;
        for (int i = 0; i < h[perceptInd].length; i++) {
            total += h[perceptInd][i];
        }
        return prop / total;
    }

    @Override
    public String toString() {
        String unq = uniqueExitedList ? " Unique" : " Normal";
        return "G: γ=" + gamma + ", |S/A|=" + h.length + ",λ=" + rewardSuccess + ", R=" + reflectionTime + ", g=" + damperGow + "" + unq;
    }


    public String toStringEvalAfter() {
        int usedMoreThan0 = 0;
        int usedMoreThan100 = 0;
        for (int i = 0; i < debugStateCounter.length; i++) {
            if (debugStateCounter[i] > 0) usedMoreThan0++;
            if (debugStateCounter[i] > 100) usedMoreThan100++;
        }
        return String.format("Out of %d states, %d was used at all, and %d was used more than 100 times", debugStateCounter.length, usedMoreThan0, usedMoreThan100);
    }

    public void findUneededStates() {
        int n = debugStateCounter.length;// number of percepts
        int base = (int) (Math.log(n) / Math.log(2));

        boolean[][] all = new boolean[n][base];
        for (int i = 0; i < debugStateCounter.length; i++) {
            if (debugStateCounter[i] != 0)
                continue;
            //unused state. check if

            boolean[] state = Utils.intToBinary(i, base);
            all[i] = state;
        }


        for (int i = 0; i < base; i++) {
            //12


        }

    }

    public void printALlUnusedStates(IState state) {
        System.out.println("-------------- Never used states ----------------- ");
        for (int i = 0; i < debugStateCounter.length; i++) {
            if (debugStateCounter[i] > 0)
                continue;
            System.out.println(state.toStringStateNumber(i));

        }
    }


    public String toStringTopPerceptPerceptsAndActions(int numOfPercepts, IState state) {
        Pair[] pairs = new Pair[debugStateCounter.length];
        int numUnusedState = 0;
        for (int i = 0; i < debugStateCounter.length; i++) {
            pairs[i] = new Pair(i, debugStateCounter[i]);
            if (debugStateCounter[i] == 0) numUnusedState++;
        }
        Arrays.sort(pairs, Pair.byYHighestFirst);

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("\n----------------- Out of %d states (top %d percepts) ------------ %d states where never used", debugStateCounter.length, numOfPercepts, numUnusedState));
        sb.append("\n");
        for (int i = 0; i < numOfPercepts; i++) {
            int perceptInd = pairs[i].x;
            int perceptCount = pairs[i].y;

            sb.append("Percept:" + perceptInd + " used " + perceptCount + " times.");
            sb.append("\t\t\t");
            sb.append(state.toStringStateNumber(perceptInd));
            sb.append("\t\t\t\t\t\t\t");
            sb.append(toStringTopActions(perceptInd, 4));
            sb.append("\n");
        }


        return sb.toString();
    }

    public String toStringTopPerceptsAndActions(int usedMoreThan, IState state) {

        ArrayList<Integer> perceptsIndexes = new ArrayList<>();
        for (int i = 0; i < debugStateCounter.length; i++) {
            if (debugStateCounter[i] > usedMoreThan) perceptsIndexes.add(i);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("\nOut of %d states, %d was used more than %d times", debugStateCounter.length, perceptsIndexes.size(), usedMoreThan));
        sb.append("\n");
        for (Integer perceptInd : perceptsIndexes) {
            sb.append("Percept:" + perceptInd);
            sb.append("\t\t\t\t\t\t");
            sb.append(state.toStringStateNumber(perceptInd));
            sb.append("\t\t\t\t\t");
            sb.append(toStringTopActions(perceptInd, 4));
            sb.append("\n");
        }


        return sb.toString();
    }

    public String toStringTopActions(int perceptIndex, int numberOftopActions) {
        ArrayList<Integer> topActionIndexes = new ArrayList<>();
//        for (int i = 0; i < h[perceptIndex].length; i++) {
//
//        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < h[perceptIndex].length; i++) {

            if (h[perceptIndex][i] > 1.1)
                sb.append(String.format("%.2f(%s) \t", h[perceptIndex][i], MarioAction.getName(i)));
        }
        return sb.toString();
    }

    public String toStringALL() {
        StaticState staticState = new StaticState();
        State state = new State(false);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < h.length; i++) {
            double bestHopping = -1;
            int bestAction = -1;
            for (int j = 0; j < h[i].length; j++) {
                if (bestHopping < getHoppingPropability(i, j)) {
                    bestHopping = getHoppingPropability(i, j);
                    bestAction = j;
                }
            }
            if (debugStateCounter[i] == 0)
                continue;
            boolean[] stateBoolArr = State.toBinary(i, 6);
            String stateStr = state.stateToString(stateBoolArr);
            staticState.setStateNumber(i);

            sb.append(staticState.toString());
            sb.append(" used:" + debugStateCounter[i] + " times. \t");
            sb.append(MarioAction.getName(bestAction));
            sb.append(" ");

            sb.append(String.format("%.2f", bestHopping * 100) + "%");
            sb.append("\n");

        }
        return sb.toString();
    }


    public static void main(String[] args) {
        PSMario ps = new PSMario(4, 3, 0.0, -1, 1, 1.0);

        int perception = 0;
        int correctAction = 1;
        for (int i = 0; i < 100; i++) {
            int action = ps.getAction(perception);
            ps.giveRewardAmount((correctAction == action) ? 100 : -20);
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(ps.getAction(perception));
        }
    }

}
