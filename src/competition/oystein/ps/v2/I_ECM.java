package competition.oystein.ps.v2;

import java.util.List;

/**
 * Created by Oystein on 26/02/15.
 */
public interface I_ECM {
    public int getAction(int percept);
    public void giveReward(boolean success);

    public Clip[] getPerceptClips();

    public ActuatorClip[] getActuatorClips();

    public List<CompositionClip> getCompClips();
}
