package competition.oystein.ps.v2;

import org.jgraph.JGraph;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by Oystein on 26/02/15.
 */
public class GraphSwing extends JFrame{

    private I_ECM ecm;

    public GraphSwing(JGraph jGraph){
//        setLayout(new BorderLayout());
//        addImpl(jGraph.getGra);
        getContentPane().add(jGraph);

    }

    JGraph jGraph;
    GraphPanel2 graphPanel2;
    public GraphSwing(I_ECM ecm){
        this.ecm = ecm;
        setLayout(new BorderLayout());
         graphPanel2 = new GraphPanel2(ecm,950,600);

        add(topPanel(),BorderLayout.PAGE_START);
        jGraph=graphPanel2.getGraph();
       add(jGraph, BorderLayout.CENTER);
//        setLayout(new BorderLayout());
//        addImpl(jGraph.getGra);
//        getContentPane().add(jGraph);
        initSwing();
    }

    JTextField jTFTrainTime;
    public JPanel topPanel(){
        JPanel panel = new JPanel();

       jTFTrainTime = new JTextField("100",4);
        panel.add(jTFTrainTime);
        JButton button = new JButton("Train");
        button.addActionListener(a->{
            try {
                int trainTime = Integer.parseInt(jTFTrainTime.getText());
                train(trainTime);
            }catch (Exception e){

            }
        });
        panel.add(button);
        return panel;
    }

    Random random = new Random(System.currentTimeMillis());
    public void train2(int trainTime){
        System.out.println("Training "+trainTime+" times");
        for (int i = 0; i < trainTime; i++) {

            int percept = random.nextInt(4);

            int action = ecm.getAction(percept);
            boolean isCorrect = percept%2==action;
            System.out.printf("%d | P %d -> A %d : %s \n",i,percept,action,isCorrect);
            ecm.giveReward(isCorrect);
//            for (int percept = 0; percept < 4; percept++) {
//                int action = ecm.getAction(percept);
//                boolean isCorrect = percept%2==action;
//                ecm.giveReward(isCorrect);
//            }
        }



//        jGraph.refresh();
        graphPanel2.update();
        System.out.println("Finished!!!");
//        System.out.println(ecm.toString());

    }
    public void train(int trainTime){
        System.out.println("Training "+trainTime+" times");
        Thread t = new Thread(() -> {
            for (int i = 0; i < trainTime; i++) {

                int percept = random.nextInt(4);
                int action = ecm.getAction(percept);
                boolean isCorrect = percept%2==action;
                System.out.printf("%d | P %d -> A %d : %s \n",i,percept,action,isCorrect);
                ecm.giveReward(isCorrect);


            }
            graphPanel2.update();
            System.out.println("Finished!!!");
//            theModel.go();

        }
        ); //This separate thread will start the new go...
        t.start(); //...when you start the thread! go!




//        jGraph.refresh();

//        System.out.println(ecm.toString());

    }


    private void initSwing(){
        setMinimumSize(new Dimension(1200, 700));
        setLocation(0,300);
        setVisible(true);
        setFocusable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


    public static void main(String[] args) {
        I_ECM ecm = new ECM2(4,2,2,2,10);

        for (int i = 0; i < 1; i++) {

            for (int percept = 0; percept < 4; percept++) {
                int action = ecm.getAction(percept);
                boolean isCorrect = percept%2==action;
                ecm.giveReward(isCorrect);
            }
        }

            new GraphSwing(ecm);

    }
}
