package competition.oystein.ps.v2;

/**
* Created by Oystein on 26/02/15.
*/
class ActuatorClip extends Clip {
    public ActuatorClip(ECM2 ecm,int id) {
        super(ecm,id);
    }

//    @Override
//    public boolean isPerceptClip() {
//        return false;
//    }
//
//    @Override
//    public boolean isAcuatorClip() {
//        return true;
//    }
//
//    @Override
//    public boolean isComposedClip() {
//        return false;
//    }

    @Override
    public String toString() {
        return "ActionClip " + id;
    }
}
