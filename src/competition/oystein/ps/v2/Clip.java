package competition.oystein.ps.v2;

import java.util.List;
import java.util.Random;

/**
* Created by Oystein on 26/02/15.
*/
abstract class  Clip {
    public ECM2 ecm;
    public int id;
    Random random = new Random(System.currentTimeMillis());
    List<Edge> edges;

    public Clip(ECM2 ecm, int id) {
        this.ecm = ecm;
        this.id = id;
    }

    public void addEdge(Edge e) {
        edges.add(e);
    }

    public  boolean isAcuatorClip(){
        return this instanceof ActuatorClip;
    }

    public boolean isPerceptClip(){
        return this instanceof PerceptClip;
    }
    public  boolean isCompositionClip(){
        return this instanceof CompositionClip;
    }

    public Edge getStrongestDirectEdge(){
//        double total2 = edges.stream().filter(e->e.end.isAcuatorClip()).mapToDouble(e->e.weight).sum();
        double total = 0.0;
        for (Edge edge : edges)
            if(edge.end.isAcuatorClip())
                total += edge.weight;

        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (Edge edge : edges) {
            if(!edge.end.isAcuatorClip()) continue;

            cumulativeProbability += edge.weight / total;
            if (p < cumulativeProbability) {
                if (!edge.emotion && ecm.currentReflection < ecm.reflectionTime - 1) {
                    ecm.currentReflection++;
                    getStrongestEdge();
                }
                return edge;
            }
        }

        return null;
    }

    public Edge getStrongestCompositonEdge(){
//        double total2 = edges.stream().filter(e->e.end.isAcuatorClip()).mapToDouble(e->e.weight).sum();
        double total = 0.0;
        for (Edge edge : edges)
            if(edge.end.isCompositionClip())
                total += edge.weight;

        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (Edge edge : edges) {
            if(!edge.end.isCompositionClip()) continue;

            cumulativeProbability += edge.weight / total;
            if (p < cumulativeProbability) {
                if (!edge.emotion && ecm.currentReflection < ecm.reflectionTime - 1) {
                    ecm.currentReflection++;
                    getStrongestEdge();
                }
                return edge;
            }
        }

        return null;
    }



    public Edge getStrongestEdge() {

        double total = 0.0;
        for (Edge edge : edges)
            total += edge.weight;

        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (int i = 0; i < edges.size(); i++) {
            cumulativeProbability += edges.get(i).weight / total;
            if (p < cumulativeProbability) {
                if (!edges.get(i).emotion && ecm.currentReflection < ecm.reflectionTime - 1) {
                    ecm.currentReflection++;
                    getStrongestEdge();
                }
                return edges.get(i);
            }
        }

        return null;
    }
    public double getHoppingPropability(Clip toClip){

        double total = 0.0;
        for (Edge edge : edges)
            total += edge.weight;
        for (Edge edge : edges){
            if(edge.end.equals(toClip))
                return edge.weight/total;
        }
        System.out.println("Not connected getHopping()");
        return 0.0;
    }


    public Edge getEdgeToClip(Clip c){
        for(Edge e: edges)
            if(e.end.equals(c)) return e;
        return null;
    }

//        @Override
//        public boolean equals(Object obj) {
//            if(!(obj instanceof Clip))
//                return false;
//            Clip other = (Clip) obj;
//            return this.id==other.id;
//        }
}
