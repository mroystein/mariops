package competition.oystein.ps.v2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 25/02/15.
 */
public class ECM2 implements I_ECM{

    public Clip[] perceptClips;
    public ActuatorClip[] actuatorClips;

    public List<CompositionClip> compClips;

    public double gamma = 0.001;
    public double reward = 1;

    List<Edge> lastUsedEdges;
    public int reflectionTime;
    public int currentReflection = 0;
    //    private PerceptClip lastUsedPercept;
    public int deliberationLength;

    public double K=1;

    public ECM2(int numPercept, int numActions, int deliberationLength, int reflectionTime, double K){
        this(numPercept,numActions,deliberationLength,reflectionTime);
        this.K=K;


    }

    public ECM2(int numPercept, int numActions, int deliberationLength, int reflectionTime) {
        if (reflectionTime < 1) System.err.println("ReflectionTIme");
        if (deliberationLength < 0) System.err.println("D");

        this.reflectionTime = reflectionTime;
        this.deliberationLength = deliberationLength;


        actuatorClips = new ActuatorClip[numActions];
        for (int i = 0; i < numActions; i++) {
            actuatorClips[i] = new ActuatorClip(this, i);
        }
        perceptClips = new PerceptClip[numPercept];
        compClips = new ArrayList<>();
        lastUsedEdges = new ArrayList<>();


    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("--------------- ECM -----------------\n");
        for (Clip perceptClip : perceptClips) {
            if (perceptClip == null)
                continue;
            sb.append(perceptClip).append("\n");
            for (Edge e : perceptClip.edges) {
                sb.append("\t").append(e).append("\n");
            }
        }
        for (CompositionClip compClip : compClips) {
            if (compClip == null)
                continue;
            sb.append(compClip).append("\n");
            for (Edge e : compClip.edges) {
                sb.append("\t").append(e).append("\n");
            }
        }


        sb.append("-------------------------------------\n");
        return sb.toString();
    }

    public int getAction(int percept) {
        currentReflection = 0;
//        if(deliberationLength==2){
//            if(percept==0 || percept==1) return 0;
//            return 1;
//        }


        //if unseen percept, create new
        if (perceptClips[percept] == null) {
            PerceptClip perceptClip = new PerceptClip(this, percept);
            perceptClip.initDefaultActions();
            perceptClips[percept] = perceptClip;

        }

        lastUsedEdges.clear();

        Clip clip = perceptClips[percept];

        //Look for a direct path
        Edge edge= clip.getStrongestDirectEdge();
        if(edge.emotion){

            lastUsedEdges.add(edge);
            return edge.end.id;
        }


//       edge = clip.getStrongestCompositonEdge();

        int debugLoopCount = 0;
        do {
            if(clip.isPerceptClip())
                edge=clip.getStrongestCompositonEdge();
            if(clip.isCompositionClip())
                edge=clip.getStrongestDirectEdge();
//            edge = clip.getStrongestEdge();
            if(edge==null) {//if no composition, and no :) edges before. just do anything
                edge = clip.getStrongestDirectEdge();
                lastUsedEdges.add(edge);
                return edge.end.id;
            }
            debugLoopCount++;
            if (debugLoopCount > 5) {
                System.out.println("WTF");
            }
            lastUsedEdges.add(edge);
            clip = edge.end;

        } while (!(clip instanceof ActuatorClip));

        return edge.end.id;
    }



    public void giveReward(boolean success) {
        if (lastUsedEdges.size() > 1) {
//            System.out.println("Going to damp : " + lastUsedEdges + " : " + success);
        }
        if(success){
            if (lastUsedEdges.size() == 2) {
                // (s,s1,s2,..,a) -> (pc, cc) , (cc,a)
                Edge pcTOccEdge = lastUsedEdges.get(0);
                Edge ccTOaEdge = lastUsedEdges.get(1);

                pcTOccEdge.weight+=K;
                ccTOaEdge.weight+=pcTOccEdge.weight;

                //direct edge update with 1
                pcTOccEdge.start.getEdgeToClip(ccTOaEdge.end).weight+=1;

            }else if(lastUsedEdges.size()==1){
                lastUsedEdges.get(0).giveReward(success);

            }else {
                System.out.println("Not implemented for so many edges.");
            }

        }else{
            lastUsedEdges.get(0).emotion=false;
        }





        for (Clip c : perceptClips) {
            if (c == null) continue;
            for (Edge e : c.edges) {
                e.weight-= gamma*(e.weight-1);
//                if (lastUsedEdges.contains(e))
//                    e.giveReward(success);

//                else e.damp();
            }
        }
        for (Clip c : compClips) {
            for (Edge e : c.edges) {
                e.weight-= gamma*(e.weight-K);
//                if (lastUsedEdges.contains(e) && success)
//                    e.giveReward(success);
//                else e.damp();
            }
        }

//        for (Edge e : lastUsedEdges)
//            e.damp(success);


        if (deliberationLength < 1)
            return;

        if (lastUsedEdges.size() != 1)
            return;


        //if this is a really good edge. consider making it composition
        Edge edge = lastUsedEdges.get(0);

//        if (edge.weight < 40)
//            return;

        double hoppingProb = edge.start.getHoppingPropability(edge.end);
        if (hoppingProb < 0.99)
            return;
        if (edge.start instanceof CompositionClip)
            return; //We don't allow comp clips to form new comp clips

        if (!(edge.end instanceof ActuatorClip))
            return; //We don't allow percept -> comp to make new comp

        PerceptClip start = (PerceptClip) edge.start;
//        System.out.println(edge.end.c);
        ActuatorClip end = (ActuatorClip) edge.end;

        int newId = perceptClips.length + start.id;
        StringBuilder sb = new StringBuilder();
        sb.append("We allready had theses comp clips:\n");
        for (PerceptClip clip : compClips) {

            if (clip.id == newId)
                return;

            Edge edge1 = clip.getStrongestEdge();
            if (edge1.end.id == end.id)
                return;
            sb.append("\t Loop checking : " + edge1 + " is not the same as " + end.id + " \n");
        }

        CompositionClip newComp = new CompositionClip(this, newId, start);

        compClips.add(newComp);
        Edge newEdge = new Edge(this, start, newComp, edge.weight + 100);
        start.addEdge(newEdge);

//        System.out.printf("Making ficinous clip %s , since the %s is strong\n", newComp, edge);

        sb.append("But we decided to add clip : " + newComp).append("\n");
        sb.append("And the edge : " + newEdge);
        if (compClips.size() > 2) {
            System.out.println("----------------- Error ---------------------");
            System.out.println(sb.toString());
            for (CompositionClip clip : compClips)
                System.out.println(clip + " " + clip.getStrongestEdge());
            System.out.println("----------------- Error End ---------------------");
        }
//        System.out.println("Making a ficitonus clip  id=" + newId + " : pointing at action " + edge.end);

//        System.out.println("\n");
    }


    public static void main(String[] args) {

//        printPerceptTraining(50);
        test();
    }

    public static void gui() {

    }

    public static void test() {
        int D = 1;
        int reflection = 2;
        ECM2 ecm = new ECM2(4, 2, D, reflection,4);

        Random random = new Random(System.currentTimeMillis());
        int[] percepts = {0, 1, 2, 3};
        int[] actions = {0, 1};

//        System.out.println(ecm);


        //train only 2 actions
        System.out.println("\t Training with 2 percepts only");
        for (int i = 0; i < 200; i++) {
            int percept = percepts[random.nextBoolean() ? 1 : 2];
            int action = ecm.getAction(percept);
            boolean isCorrect = isCorrect(percept, action);
            ecm.giveReward(isCorrect);
//            System.out.println(String.format("P:%d -> A:%d : %s", percept, action, isCorrect));
        }
        System.out.println(ecm);

        for (int i = 0; i < 5; i++) {
            int percept2 = percepts[0];
            int action2 = ecm.getAction(percept2);
            boolean isCorrect2 = isCorrect(percept2, action2);
            ecm.giveReward(isCorrect2);
            System.out.println(String.format("P:%d -> A:%d : %s", percept2, action2, isCorrect2));
        }


        System.out.println("\t Training with all percepts");
        for (int i = 0; i < 200; i++) {

            int percept = percepts[random.nextInt(percepts.length)];
            int action = ecm.getAction(percept);
            ecm.giveReward(isCorrect(percept, action));
        }

        System.out.println(ecm);

        System.out.println("\t Results");
        for (int i = 0; i < percepts.length; i++) {

            int percept = percepts[i];
            int action = ecm.getAction(percept);
            boolean isCorrect = isCorrect(percept, action);
            ecm.giveReward(isCorrect);
            System.out.println(String.format("P:%d -> A:%d : %s", percept, action, isCorrect));

        }

        ecm.getAction(1);


//        Clip c = new ActuatorClip(ecm,4);
//        System.out.println(c.isAcuatorClip());
//        System.out.println(c.isPerceptClip());
//
//        boolean b = c instanceof

    }
   static  void lolTest(Class clipClass){
       ECM2 ecm = new ECM2(4, 2, 1, 1,4);
       Clip c = new ActuatorClip(ecm,4);




    }

    @Override
    public Clip[] getPerceptClips() {
        return perceptClips;
    }
    @Override
    public ActuatorClip[] getActuatorClips() {
        return actuatorClips;
    }
    @Override
    public List<CompositionClip> getCompClips() {
        return compClips;
    }

    public static void printPerceptTraining(int numEpochs) {
        ECM2 ecm = new ECM2(4, 2, 1, 1);

        for (int i = 0; i < numEpochs; i++) {
            double avg = 0;
            for (int percept = 0; percept < 4; percept++) {
                int action = ecm.getAction(percept);
                boolean correct = isCorrect(percept, action);
                ecm.giveReward(correct);
                avg += correct ? 1 : 0;

            }
            avg /= 4;
            System.out.println(i + " " + avg);
        }

    }

    public static boolean isCorrect(int percept, int action) {
        if (percept == 0 || percept == 1)
            return action == 0;
        return action == 1;
    }
}
