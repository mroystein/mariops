package competition.oystein.ps.v2;

/**
* Created by Oystein on 26/02/15.
*/
class Edge {
    private ECM2 ecm;
    Clip start;
    Clip end;
    boolean emotion = true;

    //        int id;
    double weight;

    public Edge(ECM2 ecm, Clip start, Clip end, double weight) {
        this.ecm = ecm;
        this.weight = weight;
        this.end = end;
        this.start = start;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Edge))
            return false;
        Edge other = (Edge) obj;
        boolean my = start.equals(other.start) && end.equals(other.end);
        boolean objEq = super.equals(obj);
        if (my != objEq) {
            System.out.println("WTF");
        }
        return objEq;
    }

    public void damp() {
        weight = weight + ecm.gamma * (weight - 1);

    }


    public void giveReward(boolean success) {
        if (success)
            weight = weight + ecm.gamma * (weight - 1) + ecm.reward;
        else
            weight = weight + ecm.gamma * (weight - 1);
//            System.out.println("new weight : "+weight +""+success);
        emotion = success;
    }

    @Override
    public String toString() {
        String smiley = emotion ? ":)" : ":(";
        return String.format("{E: %s -> %s  w=%.2f %s }", start, end, weight, smiley);
//        return "{E: "+start+" -> "+end+" w="+String.format("%.2f",weight);
//        return "E: to clip " + end.id + " w=" + weight;
    }

}
