package competition.oystein.ps.v2;

/**
* Created by Oystein on 26/02/15.
*/
class CompositionClip extends PerceptClip {

    public CompositionClip(ECM2 ecm,int id, PerceptClip copy) {
        super(ecm,id);

//            System.out.println(edges.size()+" "+copy.edges.size());
        for (int i = 0; i < edges.size(); i++) {

            edges.get(i).weight = copy.edges.get(i).weight;
        }
    }

    @Override
    public String toString() {
        return "CompClip " + id;
    }


}
