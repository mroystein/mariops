package competition.oystein.ps.v2;

import java.util.ArrayList;

/**
* Created by Oystein on 26/02/15.
*/
class PerceptClip extends Clip {



    public PerceptClip(ECM2 ecm, int id) {
        super(ecm,id);

        edges = new ArrayList<>();
        for (ActuatorClip actuatorClip : ecm.actuatorClips) {
            edges.add(new Edge(ecm, this, actuatorClip, 1));
        }
    }

    public void initDefaultActions() {

        for (CompositionClip compositionClip : ecm.compClips) {
            edges.add(new Edge(ecm, this, compositionClip, 1.0));
        }
    }


    @Override
    public String toString() {
        return "PerceptClip " + id;
    }


}
