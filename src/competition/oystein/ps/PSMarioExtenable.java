package competition.oystein.ps;

/**
 * Created by oystein on 09/02/15.
 */

import competition.oystein.controller.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import competition.oystein.controller.helpers.Pair;
import competition.oystein.controller.helpers.StoredPSConfig;


/**
 * A matrix implementation of PS ECM.
 * The clip network are represented as a matrix.
 *
 * Can discover new actions.
 * <p>
 * <p>
 * Created by Oystein on 02/10/2014.
 */
public class PSMarioExtenable extends PSMario {
    public boolean debugPrint = true;

    public static double H_UNSEEN_ACTION = -1.0;
    public static double ACTION_CREATION_THRESHOLD = 2.0;

    List<Integer> knownActions;
    List<Integer> debugKnowActionsInStart;

    public PSMarioExtenable(int numPercepts, int numActions, int[] knownActionsArr, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        this.reflectionTime = reflectionTime;
        this.rewardSuccess = rewardSuccess;
        this.gamma = gamma;
        this.damperGow = damperGow;

        h = new double[numPercepts][numActions];
        g = new double[numPercepts][numActions];
        lastTimeUsed = new int[numPercepts][numActions];


        debugStateCounter = new int[numPercepts];

        debugKnowActionsInStart = new ArrayList<>();
        for (int a : knownActionsArr) {
            debugKnowActionsInStart.add(a);
        }
        reset();


    }

    public PSMarioExtenable(int numPercepts, int numActions, StoredPSConfig config) {
//        this(numPercepts,numActions,config.gamma,config.reward,1,config.damperGlow);
    }

    public PSMarioExtenable() {
    }


    public List<MarioAction> getTheNewLearnedActions() {
        List<MarioAction> l = new ArrayList<>();
        for (Integer a : knownActions) {
            if (debugKnowActionsInStart.contains(a))
                continue;//new it from the start. ignore

            MarioAction ma = MarioAction.values()[a];
            l.add(ma);
        }
        return l;
    }

    public List<MarioAction> getAllActionsKnown() {
        List<MarioAction> l = new ArrayList<>();
        for (Integer a : knownActions) {
            MarioAction ma = MarioAction.values()[a];
            l.add(ma);
        }
        return l;
    }


    public String getLearnedActions() {
        StringBuilder sb = new StringBuilder();

        for (int actionNum : knownActions) {

            sb.append(MarioAction.values()[actionNum]).append(" ,");
        }
        return sb.toString();

    }


    private boolean isKnownAction(int action) {
        for (int a : knownActions)
            if (action == a) return true;
        return false;
    }

    @Override
    public void reset() {

        knownActions = new ArrayList<>();
        knownActions.addAll(debugKnowActionsInStart.stream().collect(Collectors.toList()));

        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {

                if (isKnownAction(j))
                    h[i][j] = 1.0;
                else
                    h[i][j] = H_UNSEEN_ACTION;
                g[i][j] = 0.0;
            }
        }
    }


    @Override
    public int getAction(int percept) {
        numActionTaken++;
        debugStateCounter[percept]++;
        double p = random.nextDouble();

        double cumulativeProbability = 0.0;

//        checkForActionCompestion(percept);

        double lastLargestProb = -1;
        int lastLargestProbAction = -1;
        double hoppingProb;

        for (int i = 0; i < h[percept].length; i++) {
            if (h[percept][i] == H_UNSEEN_ACTION) continue;

            hoppingProb = getHoppingPropability(percept, i);
            if (hoppingProb > 0.2) {
                if (lastLargestProb == -1) {//first big one
                    lastLargestProb = hoppingProb;
                    lastLargestProbAction = i;
                } else {
                    checkForActionCompestion(percept, lastLargestProbAction, i);
                }
            }

            cumulativeProbability += hoppingProb;

            if (p <= cumulativeProbability) {
                lastSelectedPerc = percept;
                lastSelectedAction = i;
                g[percept][i] = 1.0;

                //new
                Pair pair = new Pair(percept, i);
                if(uniqueExitedList){
                    if(!exitedEdges.contains(pair))
                        exitedEdges.add(pair);
                }else
                    exitedEdges.add(pair);
//

                lastTimeUsed[percept][i] = currentTimeStep;
                currentTimeStep++;
                return i;
            }
        }

        //debug shit
//        double[] posActions = h[percept];
        System.err.println("Error in PSMarioExtenable: no actions. Should never happen. Returning random Action");

        return knownActions.get(random.nextInt(knownActions.size()));
    }


    public long numActionTaken = 0;
    public long numActionCompChecking = 0;


    @Override
    protected double getHoppingPropability(int perceptInd, int actionInd) {
        return MyUtils.softMaxIgnoreNegative(h[perceptInd], h[perceptInd][actionInd]);
    }

    private void checkForActionCompestion(int percept, int actionPercept1, int actionPercept2) {

        double biggestAction1 = h[percept][actionPercept1], biggestAction2 = h[percept][actionPercept2];

        if (biggestAction1 < ACTION_CREATION_THRESHOLD || biggestAction2 < ACTION_CREATION_THRESHOLD) {
            return;
        }
        numActionCompChecking++;

        //debug
        MarioAction[] ms = MarioAction.values();


        String action1Name = ms[actionPercept1].name();
        String action2Name = ms[actionPercept2].name();
        boolean[] action1 = ms[actionPercept1].getAction();
        boolean[] action2 = ms[actionPercept2].getAction();
        int difference = getDifference(action1, action2);
        double sumH = biggestAction1 + biggestAction2;

//        System.out.println("Action "+action1Name+" and "+action2Name+" can form a new action!!");


//        if(difference >2){
////            System.out.println("Difference is too big : "+difference);
//            return;
//        }
//        System.out.println("Difference is OK : "+difference);

//        if(actionPercept1+actionPercept2==3)
//            return;

        boolean[][] a = XOR_and_AND(action1, action2);


        MarioAction newAction1 = MarioAction.getMarioAction(a[0]);
        MarioAction newAction2 = MarioAction.getMarioAction(a[1]);


//        System.out.println(newAction1+" "+newAction2);

//        isKnownAction(newAction1.getActionNumber());
        if (newAction1 != null) {
            boolean b1 = createActionIfNotKnown(newAction1.getActionNumber(), percept, sumH);
            if (b1 && debugPrint)
                System.out.println("Learned a new action :" + newAction1.name() + " from action :" + action1Name + " and " + action2Name + "");

        }

        if (newAction1 != null) {
            boolean b2 = createActionIfNotKnown(newAction2.getActionNumber(), percept, sumH);
            if (b2 && debugPrint) {
                System.out.println("Learned a new action :" + newAction2.name() + " from action :" + action1Name + " and " + action2Name + "");
            }

        }


    }

    private void checkForActionCompestion(int percept) {
//        numActionCompChecking++;
        double biggestAction1 = 0.0, biggestAction2 = 0.0;
        int biggestPercept1 = 0, biggestPercept2 = 0;
        for (int i = 0; i < h[percept].length; i++) {
            double hAction = h[percept][i];
            if (hAction > biggestAction1) {
                biggestAction2 = biggestAction1;
                biggestAction1 = hAction;
                biggestPercept2 = biggestPercept1;
                biggestPercept1 = i;
            } else if (hAction > biggestAction2) {
                biggestAction2 = hAction;
                biggestPercept2 = i;
            }
        }
        checkForActionCompestion(percept, biggestPercept1, biggestPercept2);


    }

    private boolean createActionIfNotKnown(int actionNumber, int origPercept, double sumH) {

        if (knownActions.contains(actionNumber))
            return false;
        if (actionNumber == 0)
            return false;
        knownActions.add(actionNumber);

        for (int i = 0; i < h.length; i++) {
            h[i][actionNumber] = 1.0;
        }
        h[origPercept][actionNumber] = sumH;
        return true;
    }


    private boolean[][] XOR_and_AND(boolean[] action1, boolean[] action2) {
        boolean[][] a = new boolean[2][6];

        for (int i = 0; i < action1.length; i++) {
            a[0][i] = action1[i] ^ action2[i];
            a[1][i] = action1[i] && action2[i];
        }
        return a;
    }

    private int getDifference(boolean[] action1, boolean[] action2) {
        int diff = 0;
        for (int i = 0; i < action1.length; i++) {
            if (action1[i] != action2[i]) diff++;
        }
        return diff;
    }


    @Override
    public String toString() {
        return "PS-E: "+ (uniqueExitedList ?"Unq":"Mult")+" " + super.toString();
    }


    public static void main(String[] args) {
//        DynamicState ds = new DynamicState(RewardMethod.EXAMPLE_WITH_PUNISH);
//        PSMarioExtenable ps = new PSMarioExtenable();
//
//
//        ps.ge
    }
}
