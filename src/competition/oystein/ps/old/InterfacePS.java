package competition.oystein.ps.old;

/**
 * Created by Oystein on 02/10/2014.
 */
public interface InterfacePS<E, A> {

    /**
     * Method will find the best action given a perception.
     * Corresponding to the parameters and a random variable.
     *
     * @param percept The perceived input.
     * @return The agents best action.
     */
    public A getAction(E percept);


    /**
     * Should be called on after getAction is called.
     * Such that the PS agent can evaluate the action it just did.
     * <p>
     * Most of the time, the "success" will be false, and it will just damper (decrease) the h and g values.
     * If "success" is positive, the recently used edges will be strengthen.
     *
     * @param success if the last action was good.
     */
    public void giveReward(boolean success);


    public void useSoftMax(boolean useSoftMax);
}
