package competition.oystein.ps.old;

import ch.idsia.agents.Agent;
import ch.idsia.agents.controllers.BasicMarioAIAgent;
import ch.idsia.benchmark.mario.engine.sprites.Mario;
import competition.oystein.ps.PS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Oystein on 24/10/14.
 */
public class PSAgent extends BasicMarioAIAgent implements Agent {




    public PSAgent() {
        super("Oystein First Simple");
        System.out.println("Started agent: " + name);
        List<PSPerception> PSperceptions = getPerceptionsSimple();
        List<PSAction> PSactions = getActions();

        ps = new PS<PSPerception,PSAction>(PSperceptions,getActions(),0.001,1.0,1,1.0);


    }


    private boolean isMarioInAir() {
        return !isMarioOnGround && action[Mario.KEY_JUMP];
    }

    int trueJumpCounter = 0;



    int oldDistPassedCells=0;
    @Override
    public boolean[] getAction() {
//        System.out.println("Action doing.");

        boolean[] percBool = new boolean[]{isEnemyInFront()};
        PSPerception perception = new PSPerception(percBool);

        PSAction psAction = ps.getAction(perception);

        if(evaluationInfo.distancePassedCells>oldDistPassedCells){
            ps.giveReward(true);
            oldDistPassedCells=evaluationInfo.distancePassedCells;
        }else{
            ps.giveReward(false);
        }

        System.out.println(evaluationInfo.distancePassedCells);

        boolean agentJump = psAction.actions[0];
        boolean agentSpeed = psAction.actions[1];
        boolean agentRight = psAction.actions[2];


        action[Mario.KEY_RIGHT] = agentRight;
        action[Mario.KEY_SPEED] = agentSpeed;

        //action[Mario.KEY_JUMP]=isMarioAbleToJump && isMarioOnGround;

        if (isEnemyInFront() || isObstacleInFront()) {
            action[Mario.KEY_SPEED] = false;

            if (isMarioAbleToJump || isMarioInAir())
                action[Mario.KEY_JUMP] = agentJump;

            trueJumpCounter++;
        } else {
            action[Mario.KEY_JUMP] = false;
            trueJumpCounter = 0;
        }

        if (trueJumpCounter > 16) {
            trueJumpCounter = 0;
            action[Mario.KEY_JUMP] = false;
        }


        return action;
    }

    private boolean isObstacleInFront() {
        return getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 || getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0;
    }

    private boolean isEnemyInFront() {

        return getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 || getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0;
    }



    private final PS<PSPerception, PSAction> ps;

    public ArrayList<PSAction> getActions() {
        ArrayList<PSAction> l = new ArrayList<PSAction>();
        boolean[][] b = new boolean[][]{{false, false, false},
                {false, false, true},
                {false, true, false},
                {false, true, true},

                {true, false, false},
                {true, false, true},
                {true, true, false},
                {true, true, true},
        };

        for (int i = 0; i < b.length; i++) {
            l.add(new PSAction(b[i]));
            System.out.println(Arrays.toString(b[i]));
        }
        return l;
    }

    public static class PSAction {
        public final boolean[] actions;
        public PSAction(boolean[] actions){
            this.actions=actions;
        }

        @Override
        public boolean equals(Object obj) {
            PSAction o = (PSAction) obj;
            return Arrays.equals(actions,o.actions);
        }
    }

    public ArrayList<PSPerception> getPerceptionsSimple() {
        ArrayList<PSPerception> l = new ArrayList<PSPerception>();
        boolean[] b= new boolean[]{true};
        boolean[] b2= new boolean[]{false};
        l.add(new PSPerception(b));
        l.add(new PSPerception(b2));
        return l;
    }

    public ArrayList<PSPerception> getPerceptionsInt() {
        ArrayList<PSPerception> l = new ArrayList<PSPerception>();
        int[][] b = new int[][]{{0,0},{0,1},{1,0},{1,1}
        };

//        for (int i = 0; i < b.length; i++) {
//            l.add(new PSPerception(b[i]));
//            System.out.println(Arrays.toString(b[i]));
//        }
        return l;
    }
    public ArrayList<PSPerception> getPerceptions() {
        ArrayList<PSPerception> l = new ArrayList<PSPerception>();
        boolean[][] b = new boolean[][]{{false, false, false},
                {false, false, true},
                {false, true, false},
                {false, true, true},

                {true, false, false},
                {true, false, true},
                {true, true, false},
                {true, true, true},
        };

        for (int i = 0; i < b.length; i++) {
            l.add(new PSPerception(b[i]));
            System.out.println(Arrays.toString(b[i]));
        }
        return l;
    }
    public static class PSPerception {
        public final boolean[] perceptions;
        public PSPerception(boolean[] perceptions){
            this.perceptions=perceptions;
        }
        @Override
        public boolean equals(Object obj) {
            PSPerception o = (PSPerception) obj;
            return Arrays.equals(perceptions,o.perceptions);
        }
    }
}
