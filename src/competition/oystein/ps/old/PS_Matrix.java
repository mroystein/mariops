package competition.oystein.ps.old;

import competition.oystein.ps.MyUtils;

import java.util.List;
import java.util.Random;

/**
 * A matrix implementation of PS ECM.
 * The clip network are represented as a matrix.
 * <p>
 * <p>
 * Created by Oystein on 02/10/2014.
 */
public class PS_Matrix<E, A> implements InterfacePS<E, A> {
//    public final static int EMOTION_POSITIVE = 1, EMOTION_NEGATIVE = -1;

    protected Random random = new Random(System.currentTimeMillis() + new Random().nextInt());
    protected int reflectionTime;
    private double rewardSuccess;
    private double gamma;
    /**
     * Defines how far back recently used edges should get of the reward.
     */
    private double damperGow;

    double h[][], g[][];
//    int[][] emotions;

    protected List<E> percepts;
    protected List<A> actions;

    protected int lastSelectedPerc = 0, lastSelectedAction = 0;
    private boolean useSoftMax = true;


    int[][] lastTimeUsed;
    int currentTimeStep;
//    public PS_Matrix(){
//
//    };

    public PS_Matrix(List<E> percepts, List<A> actions, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        this.reflectionTime = reflectionTime;
        this.rewardSuccess = rewardSuccess;
        this.gamma = gamma;
        this.damperGow = damperGow;
        this.percepts = percepts;
        this.actions = actions;


        h = new double[percepts.size()][actions.size()];
        g = new double[percepts.size()][actions.size()];
        lastTimeUsed = new int[percepts.size()][actions.size()];
//        emotions = new int[percepts.size()][actions.size()];

        reset();


    }



    public PS_Matrix() {
    }

    public void reset() {
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {
                h[i][j] = 1.0;
                g[i][j] = 0.0;
            }
        }
    }

    @Override
    public A getAction(E percept) {
        //Runtime : O(|actions|^2)
        int index = percepts.indexOf(percept);
        if (index == -1) {
            System.out.println("Not found!");
            return null;
        }

//        Collections.binarySearch(percepts,percept);

        //double p = ProjectionSimulationGen.random.nextDouble();
        double p = random.nextDouble();

        double cumulativeProbability = 0.0;
        for (int i = 0; i < h[index].length; i++) {

            cumulativeProbability += getHoppingPropability(index, i);
            if (p <= cumulativeProbability) {
                lastSelectedPerc = index;
                lastSelectedAction = i;
                g[index][i] = 1.0;

                //new

                lastTimeUsed[index][i] = currentTimeStep;
                currentTimeStep++;
                return actions.get(i);
            }
        }

        System.out.println("no actions. Should never happen.");
        return null;
    }


    @Override
    public void giveReward(boolean success) {

        //g[lastSelectedPerc][lastSelectedAction] = 1.0;

        double reward = 0.0;
        if (success)
            reward = rewardSuccess;

        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {


                h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];

                g[i][j] = g[i][j] - damperGow * g[i][j];


                if (g[i][j] < 0.0)
                    g[i][j] = 0.0;
            }
        }

    }

    public void giveRewardAmount(double reward) {

        //g[lastSelectedPerc][lastSelectedAction] = 1.0;
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {


                h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];

                g[i][j] = g[i][j] - damperGow * g[i][j];


                if (g[i][j] < 0.0)
                    g[i][j] = 0.0;
            }
        }

    }


    @Override
    public void useSoftMax(boolean useSoftMax) {
        this.useSoftMax = useSoftMax;
    }

    private double getHoppingPropability(int perceptInd, int actionInd) {
        if (useSoftMax)
            return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);

        return getHoppingPropabilityFormulaOne(perceptInd, actionInd);
    }

    private double getHoppingPropabilityFormulaOne(int perceptInd, int actionInd) {
        double prop = h[perceptInd][actionInd];
        double total = 0.0;
        for (int i = 0; i < h[perceptInd].length; i++) {
            total += h[perceptInd][i];
        }
        return prop / total;
    }

    @Override
    public String toString() {
        return "G: γ=" + gamma + ", |S/A|=" + percepts.size() + ",λ=" + rewardSuccess + ", R=" + reflectionTime + ", g=" + damperGow;
    }
}
