package competition.oystein.ps.old;

import ch.idsia.agents.Agent;
import ch.idsia.agents.LearningAgent;
import ch.idsia.benchmark.mario.environments.Environment;
import ch.idsia.benchmark.tasks.BasicTask;
import ch.idsia.benchmark.tasks.LearningTask;
import ch.idsia.benchmark.tasks.MarioCustomSystemOfValues;
import ch.idsia.tools.MarioAIOptions;
import competition.oystein.controller.states.old.MarioState;

/**
 * Created by Oystein on 10/12/14.
 */
public class MySimpleAgent implements LearningAgent {

    private MarioState marioState;


    public MySimpleAgent() {
        marioState = new MarioState();
    }


    @Override
    public void learn() {

    }

    @Override
    public void giveReward(float reward) {

    }

    @Override
    public void newEpisode() {

    }

    @Override
    public void setLearningTask(LearningTask learningTask) {

    }

    @Override
    public void setEvaluationQuota(long num) {

    }


    @Override
    public Agent getBestAgent() {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public boolean[] getAction() {

        return marioState.getAction();
    }

    @Override
    public void integrateObservation(Environment environment) {

        marioState.update(environment);

    }

    @Override
    public void giveIntermediateReward(float intermediateReward) {

    }

    @Override
    public void reset() {

    }

    @Override
    public void setObservationDetails(int rfWidth, int rfHeight, int egoRow, int egoCol) {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(String name) {

    }


    public static void main(String[] args) {
        boolean PLAY_MYSELF = false;
        Agent agent = new MySimpleAgent();

        int fps = 24;


        final MarioAIOptions marioAIOptions = new MarioAIOptions();
        marioAIOptions.setFPS(fps);
        marioAIOptions.setTimeLimit(Integer.MAX_VALUE);
//        marioAIOptions.set
        marioAIOptions.setEnemies("off");
        marioAIOptions.setLevelDifficulty(0);
        marioAIOptions.setFlatLevel(true);//makes it easy
        //marioAIOptions.setVisualization(false);
        marioAIOptions.setVisualization(true);

        if (PLAY_MYSELF) {
            playKeyboard(marioAIOptions);
        } else {
            marioAIOptions.setAgent(agent);
            final BasicTask basicTask = new BasicTask(marioAIOptions);

//        basicTask.reset(marioAIOptions);
            final MarioCustomSystemOfValues m = new MarioCustomSystemOfValues();
//        basicTask.runSingleEpisode(1);
            // run 1 episode with same options, each time giving output of Evaluation info.
            // verbose = false
            basicTask.doEpisodes(5, false, 1);
            System.out.println("\nEvaluationInfo: \n" + basicTask.getEnvironment().getEvaluationInfoAsString());
            System.out.println("\nCustom : \n" + basicTask.getEnvironment().getEvaluationInfo().computeWeightedFitness(m));
        }

        System.exit(0);


    }

    private static void playKeyboard(MarioAIOptions marioAIOptions) {

        final BasicTask basicTask = new BasicTask(marioAIOptions);
        basicTask.setOptionsAndReset(marioAIOptions);
        basicTask.doEpisodes(2, true, 1);
        System.exit(0);
    }
}
