package competition.oystein.ps.old;

import ch.idsia.agents.Agent;
import ch.idsia.agents.controllers.BasicMarioAIAgent;
import ch.idsia.benchmark.mario.engine.sprites.Mario;
import ch.idsia.benchmark.mario.environments.Environment;

/**
 * Created by Oystein on 24/10/14.
 */
public class FirstAgent extends BasicMarioAIAgent implements Agent {


    public FirstAgent() {
        super("Oystein First Simple");
        System.out.println("Started agent: " + name);

    }


    private boolean isMarioInAir(){
        return !isMarioOnGround && action[Mario.KEY_JUMP];
    }

    int trueJumpCounter=0;
    @Override
    public boolean[] getAction() {
//        System.out.println("Action doing.");
        action[Mario.KEY_RIGHT] = true;
        action[Mario.KEY_SPEED] = true;

        //action[Mario.KEY_JUMP]=isMarioAbleToJump && isMarioOnGround;

        if(isEnemyInFront() || isObstacleInFront()){
            action[Mario.KEY_SPEED] = false;

            if(isMarioAbleToJump || isMarioInAir())
                action[Mario.KEY_JUMP] =true;

            trueJumpCounter++;
        }else{
            action[Mario.KEY_JUMP] =false;
            trueJumpCounter=0;
        }

        if(trueJumpCounter>16){
            trueJumpCounter=0;
            action[Mario.KEY_JUMP] =false;
        }


        return action;
    }

    private boolean isObstacleInFront(){
        return getReceptiveFieldCellValue(marioEgoRow,marioEgoCol+1)!=0 || getReceptiveFieldCellValue(marioEgoRow,marioEgoCol+2)!=0;
    }

    private boolean isEnemyInFront() {

        return getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 || getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0;
    }


}
