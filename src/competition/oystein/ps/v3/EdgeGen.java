package competition.oystein.ps.v3;

/**
 * Created by Oystein on 13/07/15.
 */
public class EdgeGen {
    private PS_GeneralizationWActionComp ps_generalizationWActionComp;
    public double h = 1;
    public ClipGen start;
    public ClipGen end;


    public EdgeGen(PS_GeneralizationWActionComp ps_generalizationWActionComp, ClipGen start, ClipGen end) {
        this.ps_generalizationWActionComp = ps_generalizationWActionComp;
        this.start = start;
        this.end = end;

    }

    public double getWeight() {
        return h;
    }

    public void update(double reward) {
        h = h - ps_generalizationWActionComp.gamma * (h - 1) + reward;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EdgeGen edge = (EdgeGen) o;
        return !(end != null ? !end.equals(edge.end) : edge.end != null) && !(start != null ? !start.equals(edge.start) : edge.start != null);
    }

    @Override
    public int hashCode() {
        int result = start != null ? start.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("E{%s -> %s h=%.3f}", start, end, h);
    }
}
