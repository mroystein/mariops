package competition.oystein.ps.v3;

import competition.oystein.controller.helpers.MyUtils;
import competition.oystein.controller.helpers.Pair;
import competition.reinforcment.ILearningAlgorithm;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 * A matrix implementation of PS ECM.
 * The clip network are represented as a matrix.
 * <p>
 * With edge glow.
 * Features:
 * - Damping
 * - Emotion (Reflection)
 * - Only Direct Walks
 * - Fast
 * <p>
 * <p>
 * <p>
 * I think it works..
 * Created by Oystein on 02/10/2014.
 */
public class PS implements ILearningAlgorithm {


    public static final double DAMPER_GLOW_LOW = 0.0000000001;
    public int actionSelection= ILearningAlgorithm.SOFTMAX;
    protected Random random = new Random(System.currentTimeMillis() + new Random().nextInt());

    protected double rewardSuccess;

    /**
     * Damping. How much to forget/remember from previous.
     */
    protected double damping;
    /**
     * Defines how far back recently used edges should get of the reward.
     */
    protected double glow;
    /**
     * How many times agent can reflect. 1=None. Must be >=1
     */
    protected int maxReflection;
    protected int currReflection;



    double h[][], g[][];
    boolean[][] emotion;

    protected int lastSelectedPerc = 0, lastSelectedAction = 0;

    /**
     * Alternative way of holding the clips which are glowing. Faster than looping through the whole matrix.
     */

    protected LinkedList<Pair> exitedEdges = new LinkedList<>();
    public boolean allowMultipleEqualClipsInTheExitedList = false;

    protected int[][] lastTimeUsed;
    protected int currentTimeStep;

    public double epsilon = 0.01;

    public boolean EXPERIMENTAL=false;
    public double learningRate=0.25;
    public double gammaFuture =0.9;

    public PS(int numPercepts, int numActions, double damping, double rewardSuccess, int maxReflection, double glow) {
        this.rewardSuccess = rewardSuccess;
        this.damping = damping;
        this.glow = glow;
        this.maxReflection = maxReflection;

        h = new double[numPercepts][numActions];
        g = new double[numPercepts][numActions];
        emotion = new boolean[numPercepts][numActions];
        lastTimeUsed = new int[numPercepts][numActions];
        reset();
        validateParamters();

        double recoGlow = 1.0 / (numPercepts * 10);
        System.out.printf("Recommended glow %.5f\n", recoGlow);
    }
    public PS(int numPercepts, int numActions, double damping, double rewardSuccess, int maxReflection, double glow, int actionSelection) {
        this(numPercepts, numActions, damping, rewardSuccess, maxReflection, glow);

        this.actionSelection = actionSelection;
    }



    @Override
    public int getAction(int percept) {
        double p = random.nextDouble();
        currReflection++;
        double cumulativeProbability = 0.0;
        for (int i = 0; i < h[percept].length; i++) {

            cumulativeProbability += getHoppingPropability(percept, i);
            if (p <= cumulativeProbability) {

                //If negative edge, reflection is on, and bigger than current reflection. Re-select.
                if (currReflection < maxReflection && !emotion[percept][i])
                    return getAction(percept);

                g[percept][i] = 1.0;

                Pair pair = new Pair(percept, i);

                if (allowMultipleEqualClipsInTheExitedList || !exitedEdges.contains(pair)) {
                    exitedEdges.addFirst(pair);
                } else {
                    //Re-exite the edge
                    //g[percept][i]=1.0;
                    Pair p2 = exitedEdges.get(exitedEdges.indexOf(pair));

                    g[p2.x][p2.y] = 1.0;
                }

                //Integer.MAX_VALUE
                //new
                lastSelectedPerc = percept;
                lastSelectedAction = i;
                lastTimeUsed[percept][i] = currentTimeStep;
                currentTimeStep++;

                return i;
            }
        }
        return -1;
    }

    @Override
    public void update(double reward, int currentStateNumber) {
        currReflection = 0;

        if (damping > 0.0) {
            giveRewardListEvenWithDamping(reward);
//            giveRewardLoop(reward);
        } else {
            if(EXPERIMENTAL)
                giveRewardList(reward);
            else
                giveRewardList2(reward);
        }

    }

    @Override
    public void reset() {
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {
                h[i][j] = 1.0;
                g[i][j] = 0.0;
                lastTimeUsed[i][j] = 0;
                emotion[i][j] = true;
            }
        }

    }

    private void validateParamters() {
        if (damping < 0 && damping > 1)
            System.err.println("Invalid damping" + damping);
        if (glow < 0 && glow > 1)
            System.err.println("Invalid damperGlow" + damping);
        if (maxReflection < 1)
            System.err.println("Reflection must >=1 , not " + maxReflection);


        checkDamperGlow();
    }

    private void checkDamperGlow() {
        int counter = 0;
        double g = 1.0;
        while (g > DAMPER_GLOW_LOW) {
            counter++;
            g = g - glow * g;
        }
        System.out.println("Max number of exited edges: " + counter);

//        System.out.println(g);
//        System.out.println(Math.pow(glow,34));


    }

    public static void calculateNumberOfExitedClipsGivenGlowValue(double glow) {
        int counter = 0;
        double g = 1.0;
        while (g > DAMPER_GLOW_LOW) {
            counter++;
            g = g - glow * g;
        }
        System.out.println("Max number of exited edges: " + counter);
    }

    private void checkDamperGlow2() {
        int counter = 0;
        double a = 1.0;
        while (a > DAMPER_GLOW_LOW) {
            counter++;
            a *= glow;
        }
        System.out.println("Max number of exited edges: " + counter);

        System.out.println(a);
        System.out.println(Math.pow(glow, 34));


    }





    public void setParameter(String parameter, double value) {
        switch (parameter.toLowerCase()) {
            case "glow":
                this.glow = value;
                break;
            case "damping":
                damping = value;
                break;
            case "learningrate":
                learningRate=value;
                break;
            case "gammafuture":
                gammaFuture=value;
                break;
            default:
                System.err.println("Unknown parameter " + parameter);
        }
    }



    /**
     * Give reward by looping Matrix.
     * Takes longer time than the list method, but need this if damper is >0, since ALL cells get updated.
     *
     * @param reward
     */
    public void giveRewardLoop(double reward) {
        reward = setEmotionAndGetAdaptiveReward(reward);
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {

                h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward * g[i][j];
                g[i][j] = g[i][j] - glow * g[i][j];

                if (g[i][j] < 0.0)
                    g[i][j] = 0.0;
                if (h[i][j] < 1.0) h[i][j] = 1.0;
            }
        }
    }


    /**
     * 1. Set the emotion tag.
     * 2. Decides if negative rewards are allowed.
     *
     * @param reward from environment.
     * @return new reward.
     */
    private double setEmotionAndGetAdaptiveReward(double reward) {
        boolean wasSuccess = reward > 0;
        emotion[lastSelectedPerc][lastSelectedAction] = wasSuccess;

//        if(!wasSuccess)
//            epsilon *= 1.02;

//        System.out.println(world.getStepCounter()+" e="+epsilon);

        if (rewardSuccess < 0) {
            return reward;
        }
        if (reward < 0)
            return 0;
        return reward;
    }

    /**
     * If damping paramter is 0, there is no need to loop the whole matrix. Only update the recently used!
     *
     * @param reward
     */
    public void giveRewardList2(double reward) {
        reward = setEmotionAndGetAdaptiveReward(reward);

        if (exitedEdges.size() > 10000)
            System.err.println("Exited edges size : " + exitedEdges.size() + ". Not good!!");

        Iterator<Pair> it = exitedEdges.iterator();
        int i, j;
        while (it.hasNext()) {
            Pair p = it.next();
            i = p.x;
            j = p.y;
            h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward * g[i][j];

            g[i][j] = g[i][j] - glow * g[i][j];

            if (g[i][j] < DAMPER_GLOW_LOW) {
                g[i][j] = 0.0;
                it.remove();
            }
            if (h[i][j] < 1.0) h[i][j] = 1.0;
        }
    }

    public void giveRewardList(double reward) {
        reward = setEmotionAndGetAdaptiveReward(reward);
        //try to reward depending on newest value
        if (exitedEdges.size() > 10000)
            System.err.println("Exited edges size : " + exitedEdges.size() + ". Not good!!");

        Iterator<Pair> it = exitedEdges.iterator();


        int timeCounter=0;
        int i, j;
        Pair prev=null;
        while (it.hasNext()) {
            timeCounter++;
            Pair p = it.next();

            i = p.x;
            j = p.y;
            h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward * g[i][j];

//            double dOldH=h[i][j];
//            double valueFeauture=0.99;
//            double perceptMatter=(double)timeCounter/(double)exitedEdges.size();

            if (h[i][j] < 1.0) h[i][j] = 1.0;

            if(prev!=null){
                double lastH=h[prev.x][prev.y];
                double dAdded=  learningRate*( gammaFuture*lastH - h[i][j]);
                h[i][j]=h[i][j]+dAdded;
//                double dNewH=h[i][j];

                if(Math.abs(dAdded)>2){
//                     System.out.println(""+dAdded);
                }
            }


            g[i][j] = g[i][j] - glow * g[i][j];
            prev=p;
            if (g[i][j] < DAMPER_GLOW_LOW) {
                g[i][j] = 0.0;
                it.remove();
            }
            if (h[i][j] < 1.0) h[i][j] = 1.0;


        }
    }


    public void giveRewardListEvenWithDamping(double reward) {
        reward = setEmotionAndGetAdaptiveReward(reward);

        if (exitedEdges.size() > 10000)
            System.err.println("Exited edges size : " + exitedEdges.size() + ". Not good!!");


        Iterator<Pair> it = exitedEdges.iterator();
        int i, j;
        while (it.hasNext()) {
            Pair p = it.next();
            i = p.x;
            j = p.y;

            int stepsSinceUsed = currentTimeStep - lastTimeUsed[i][j];
//            double old = h[i][j];
//            for (int k = 0; k < stepsSinceUsed; k++) {
//                if(h[i][j]<=1.0) break;
//                h[i][j] = h[i][j] - damping * (h[i][j] - 1);
//            }
            h[i][j] = h[i][j] - (damping * (h[i][j] - 1))*stepsSinceUsed;
            if (h[i][j] < 1.0) h[i][j] = 1.0;
//            System.out.printf("h was %.4f -> %.4f , not used in %d steps \n",old,h[i][j],stepsSinceUsed);

            h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward * g[i][j];

            g[i][j] = g[i][j] - glow * g[i][j];

            if (g[i][j] < DAMPER_GLOW_LOW) {
                g[i][j] = 0.0;
                it.remove();
            }
            if (h[i][j] < 1.0) h[i][j] = 1.0;
        }
    }



    private double getHoppingPropability(int perceptInd, int actionInd) {
        switch (actionSelection) {
            case PS_SIMPLE:
                return getHoppingPropabilityFormulaOne(perceptInd, actionInd);

            case SOFTMAX:
                return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);
            case E_GREEDY:
                System.err.println("Not implemented EGreedy");
//                return E_Greedy(perceptInd) == actionInd ? 1.0 : 0.0;
            default:
                System.err.println("No Action policy selected. Doing SoftMax");
                return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);
        }



    }

    private double getHoppingPropabilityFormulaOne(int perceptInd, int actionInd) {
        double prop = h[perceptInd][actionInd];
        double total = 0.0;
        for (int i = 0; i < h[perceptInd].length; i++) {
            total += h[perceptInd][i];
        }
        return prop / total;
    }
    boolean simpleToString=false;

    @Override
    public String toString() {
        String refStr = maxReflection==1?"":" R="+maxReflection+" ";
        String as = ILearningAlgorithm.toStringActionSelection(actionSelection);
        if(simpleToString){
            return String.format("γ=%.4f %s %s ",damping,refStr,as);
        }


        String e = EXPERIMENTAL?"EXPERIMENTAL ":"";
        if(EXPERIMENTAL){
            e=String.format("E: [g=%.3f, l=%.3f]",gammaFuture,learningRate);
        }

        return String.format("%s PS γ=%.4f |S|=%d, |A|=%d, λ=%.2f, R=%d g=%.4f, %s",e, damping, h.length, h[0].length, rewardSuccess, maxReflection, glow, as);

//        return "PS: γ=" + damping + ", |S|=" + h.length + ",λ=" + rewardSuccess + ", R=" + maxReflection + ", g=" + glow + " " + as;
    }


    public static void main(String[] args) {
//        IWorld world1;
//        world1 = GridWorldGen.parseFile(GridWorldGen.BOARD_20x20);
//        PS ps = new PS(world1, 0.0, -1, 0.001);
//        System.out.println(ps);
//
//
//        PS ps2 = new PS(world1, 0.0, -1, 0.001);
//        System.out.println(ps2);


        double h, gamma, reward, g;
        for (int i = 0; i < 10; i++) {

            h = Math.random() * 50 + 100;
            gamma = Math.random();
            reward = Math.random() * 123 + 10;
            g = Math.random();

            double t1 = t1(h, gamma, reward, g);
            double t2 = t2(h, gamma, reward, g);
            System.out.println(t1);
            System.out.println(t2);
            System.out.println(t1==t2);
            System.out.println();
        }

        System.out.println(t1(2,0.1,1,1));
        System.out.println(t2(2, 0.1, 1, 1));
    }


    public static double t1(double h, double gamma, double reward, double g) {
        h = h - gamma * (h - 1) + reward * g;
        return h;
        // h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward * g[i][j];
    }

    public static double t2(double h, double gamma, double reward, double g) {
        h += gamma * (h - 1) + reward * g;
        return h;
        // h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward * g[i][j];
    }
}
