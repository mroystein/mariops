package competition.oystein.ps.v3;

import competition.oystein.controller.helpers.MyUtils;
import competition.oystein.gui.GraphPanel2;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.List;

/**
 * Clip Glow with association. Clip to clip etc.
 * <p>
 * Using the idea Clip glow.
 * Each clip can be exited
 * Created by Oystein on 10/04/15.
 */
public class PS_GeneralizationWActionComp {
    public int WILDCARD = -1;
    public double gamma;

    public static Random random = new Random(System.currentTimeMillis());

    public List<EdgeGen> currentSequenceEdges = new LinkedList<>();

    public ArrayList<ArrayList<ClipGen>> clipLayers;

    int perceptLayer = 0;
    int actionLayer;
    int numCategories;
    public int clipCounter = 0;

    public PS_GeneralizationWActionComp(double gamma, int[][] actions, int numCategories) {
        this.numCategories = numCategories;
//        System.out.println("Number of categories: " + numCategories);
        int numLayers = numCategories + 2;
        actionLayer = numLayers - 1;
        clipLayers = new ArrayList<>();
        for (int i = 0; i < numLayers; i++) {
            clipLayers.add(new ArrayList<>());
        }
        this.gamma = gamma;

        for (int[] action : actions) {
            clipLayers.get(actionLayer).add(new ClipGen(this, action, true));
        }
    }

    public void countEdges() {
        int size = 0;
        for (ArrayList<ClipGen> layer : clipLayers) {
            for (ClipGen c : layer) {
                size += c.edges.size();
            }
        }
        System.out.println("num edges: " + size);
    }

    public void saveToGEXFformat() {


        StringBuilder nodes = new StringBuilder();
        StringBuilder edges = new StringBuilder();
        int layerId=0;
        for (ArrayList<ClipGen> clipLayer : clipLayers) {
            nodes.append("\t\t<node id=\"" + layerId + "\" label=\"layer " + layerId + "\" />\n");
            nodes.append("\t\t<nodes>\n");
            for (ClipGen clipGen : clipLayer) {
                nodes.append("\t\t\t<node id=\"" + clipGen.id + "\" label=\"" + clipGen.toString() + "\" />\n");
            }
            nodes.append("\t\t</nodes>\n");
        }
        int edgeIds = 0;
        for (ArrayList<ClipGen> clipLayer : clipLayers) {
            for (ClipGen clipGen : clipLayer) {
                for (EdgeGen edge : clipGen.edges) {
                    edges.append("\t\t<edge id=\"" + edgeIds + "\" source=\"" + edge.start.id + "\" target=\"" + edge.end.id + "\" />\n");
                    edgeIds++;
                }

            }
        }


        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\">\n" +
                "<graph mode=\"static\" defaultedgetype=\"directed\">\n" +
                "\t<nodes>\n" +
                nodes.toString() +
                "\t</nodes>\n" +
                "\t<edges>\n" +
                edges.toString() +
                "\t</edges>\n" +
                "</graph>\n" +
                "</gexf>");


        PrintWriter writer;
        try {

            writer = new PrintWriter("g1.txt", "UTF-8");
            writer.println(sb.toString());

            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    private String getColorForLayer(int layer, int totalLayers){

        double v1 = (double) layer/ (double) totalLayers;

//        String[] arr = {"[color=\""+v1+" 0.700 0.700\"]","color=\"0.002 0.999 0.999\""};
//        return "[color=\""+v1+" 0.700 0.700\"]";
//        return "";
        layer+=2;
        return "[color="+layer+"]";

    }

    public void saveToGraphvizformat(boolean run, String filename) {


        StringBuilder nodes = new StringBuilder();
        StringBuilder edges = new StringBuilder();
        int layerId=0;
        for (ArrayList<ClipGen> clipLayer : clipLayers) {
//            nodes.append("\t\t<node id=\"" + layerId + "\" label=\"layer " + layerId + "\" />\n");
//            nodes.append("\t\t<nodes>\n");
            for (ClipGen clipGen : clipLayer) {
                nodes.append("\""+clipGen+"\" "+getColorForLayer(layerId,clipLayers.size())+";\n");
            }
            layerId++;
//            nodes.append("\t\t</nodes>\n");
        }


        int edgeIds = 0;
        for (ArrayList<ClipGen> clipLayer : clipLayers) {
            for (ClipGen clipGen : clipLayer) {
                for (EdgeGen edge : clipGen.edges) {
                    double w =    clipGen.getHoppingPropabilityToClip(edge.end);

                    String edgeProperty= "[weight= "+edge.h+" penwidth=\""+w+"\"]";
                    edges.append("\t \"" + edge.start+"\" -> \""+edge.end+"\" "+edgeProperty+";\n");

//                    edges.append("\t\t<edge id=\"" + edgeIds + "\" source=\"" + edge.start.id + "\" target=\"" + edge.end.id + "\" />\n");
                    edgeIds++;
                }

            }
        }

        String nodeScheme = "node [ colorscheme=paired12, color = 1, style = filled];";


        StringBuilder sb = new StringBuilder();
        sb.append("digraph G {"+
                "\n"+nodeScheme+"\n"+
                edges.toString() +
                ""+
                nodes.toString()+
                "}");

        PrintWriter writer;
        try {

            writer = new PrintWriter(filename, "UTF-8");
            writer.println(sb.toString());

            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(run) {
            Runtime rt = Runtime.getRuntime();
            try {
                Process pr = rt.exec("dot -Tpng "+filename+" -o "+filename+".png");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public String toString() {
        StringBuilder sbLayers = new StringBuilder("Layers: {");
        for (int i = 0; i < clipLayers.size(); i++) {
            sbLayers.append(clipLayers.get(i).size());
            if (i < clipLayers.size() - 1)
                sbLayers.append(",");
        }
        sbLayers.append("} ");

        return String.format("PS gen. γ=%.4f , %s", gamma, sbLayers.toString());
    }

    public int[] getAction(int[] percept) {
        percept = percept.clone();

        ClipGen perceptClip = new ClipGen(this, percept);

        int indexOf = clipLayers.get(perceptLayer).indexOf(perceptClip);
        if (indexOf == -1) {
            addClip(perceptClip);
        } else {
            perceptClip = clipLayers.get(perceptLayer).get(indexOf);
        }

        return perceptClip.hop();
    }

    private int[] createWildCard(int[] p1, int[] p2) {
        int[] newP = new int[p1.length];
        for (int i = 0; i < p1.length; i++) {
            newP[i] = p1[i] == p2[i] ? p1[i] : WILDCARD;
        }
        return newP;
    }

    private int countDiffElements(int[] p1, int[] p2) {
        int diff = 0;
        for (int i = 0; i < p1.length; i++) {
            diff += p1[i] == p2[i] ? 0 : 1;
        }
        return diff;
    }

    private void addClip(ClipGen perceptClip) {
        //look for match in clip layer
        for (ClipGen clip : clipLayers.get(perceptLayer)) {
            int diffElements = countDiffElements(perceptClip.percept, clip.percept);
            if (diffElements <= 0 || diffElements > numCategories) continue;

            int[] wildCardPercept = createWildCard(perceptClip.percept, clip.percept);
            ClipGen wildCardClip = new ClipGen(this, wildCardPercept);

            if (clipLayers.get(diffElements).contains(wildCardClip)) continue;

            clipLayers.get(diffElements).add(wildCardClip);
//            addWildCardClip(wildCardClip);
            addOrComfirmAllEdgesExist();
        }


        clipLayers.get(perceptLayer).add(perceptClip);

        addOrComfirmAllEdgesExist();

        // add direct edge to action clips
//        clipLayers.get(actionLayer).forEach(perceptClip::addEdge);


    }

    /**
     * Call this for all clips (except action clips) every time a clip is created
     *
     * @param clip      this
     * @param thisLayer current layer
     */
    private void addEdgesToEveryThingBelow(ClipGen clip, int thisLayer) {
        for (int i = thisLayer + 1; i < clipLayers.size(); i++) {
            clipLayers.get(i).forEach(clip::addEdge);
        }
    }

    private void addOrComfirmAllEdgesExist() {
        //skip action layer
        for (int i = 0; i < clipLayers.size() - 1; i++) {
            for (ClipGen c : clipLayers.get(i)) {
                addEdgesToEveryThingBelow(c, i);
            }
        }
    }

//    private void addWildCardClip(Clip wildCardClip) {
////        System.out.println("Created new WildCard clip " + wildCardClip);
//        for (Clip actionClip : clipLayers.get(actionLayer)) {
//            wildCardClip.addEdge(actionClip);
//        }
//
//        for (Clip perceptClip : clipLayers.get(perceptLayer)) {
//            perceptClip.addEdge(wildCardClip);
//        }
//
//    }

    int longestSequence = 0;

    public void giveReward(double reward) {
//        System.out.println();
//        System.out.println("|S|=" + currentSequenceEdges.size() + " : " + currentSequenceEdges);
//        System.out.println();

        if (currentSequenceEdges.size() > longestSequence) {
            longestSequence = currentSequenceEdges.size();
            System.out.println(currentSequenceEdges.size() + " : " + currentSequenceEdges);
        }
        if (reward < 0)
            reward = 0;

        for (ArrayList<ClipGen> clipLayer : clipLayers) {
            for (ClipGen clip : clipLayer) {
                for (EdgeGen edge : clip.edges) {
                    if (currentSequenceEdges.contains(edge))
                        edge.update(reward);
                    else edge.update(0);
                }
            }
        }


        currentSequenceEdges.clear();

    }


    public static void main(String[] args) {
        experimentFromPaper();
    }


    public static int[] GREEN_LEFT = {0, 0};
    public static int[] GREEN_RIGHT = {0, 1};
    public static int[] RED_LEFT = {1, 0};
    public static int[] RED_RIGHT = {1, 1};

    public static int[] ACTION_STOP = {0};
    public static int[] ACTION_GO = {1};

    public static void experimentFromPaper() {


        int[][] percepts = {GREEN_LEFT, GREEN_RIGHT, RED_LEFT, RED_RIGHT};
        int[][] actions = {ACTION_STOP, ACTION_GO};


        PS_GeneralizationWActionComp ps = new PS_GeneralizationWActionComp(0.0, actions, percepts[0].length);

        double correctCount = 0;
        double numTests = 4000;

        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[0] == action[0];
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[0] != action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }

        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[1] == action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[1] != action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }


        System.out.println("Correct count : " + correctCount / numTests);

//        GraphPanel2 graph = new GraphPanel2(ps.clipLayers,600,600);
//        GraphJFrame g = new GraphJFrame(ps);
        ps.saveToGraphvizformat(true,"g1.txt");

    }

    public void checkForActionCompestion(ClipGen clipGen, ClipGen actionClip1, ClipGen actionClip2) {


        double prob1 = clipGen.getHoppingPropabilityToClip(actionClip1);
        double prob2 = clipGen.getHoppingPropabilityToClip(actionClip2);



        boolean[][] a = XOR_and_AND(actionClip1.percept, actionClip2.percept);

        ClipGen newC1 = new ClipGen(this,toInt(a[0]),true);
        ClipGen newC2 = new ClipGen(this,toInt(a[1]),true);

        checkAndMaybeaddNewActionClip(newC1);
        checkAndMaybeaddNewActionClip(newC2);


    }
    private void checkAndMaybeaddNewActionClip(  ClipGen newActionClip){
        if(clipLayers.get(actionLayer).contains(newActionClip))
            return;


        clipLayers.get(actionLayer).add(newActionClip);
        System.out.println("Learned a new Action : "+ newActionClip);

    }

    private boolean[][] XOR_and_AND(boolean[] action1, boolean[] action2) {
        boolean[][] a = new boolean[2][6];

        for (int i = 0; i < action1.length; i++) {
            a[0][i] = action1[i] ^ action2[i];
            a[1][i] = action1[i] && action2[i];
        }
        return a;
    }
    private boolean[][] XOR_and_AND(int[] action1, int[] action2) {
        return XOR_and_AND(toBool(action1),toBool(action2));
    }
    private boolean[] toBool(int[] a){
        boolean[] b = new boolean[a.length];
        for (int i = 0; i < a.length; i++) {
            if(a[i]==0)
                b[i]=false;
            else if(a[i]==1)
                b[i]=true;
            else
                System.err.println("huge error");

        }
        return b;
    }
    private int[] toInt(boolean[] a){
        int[] b = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            if(a[i])
                b[i]=1;
            else
                b[i]=0;

        }
        return b;
    }

    static class GraphJFrame {
        public GraphJFrame(PS_GeneralizationWActionComp ps) {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    JFrame frame = new JFrame();
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    GraphPanel2 graphPanel2 = new GraphPanel2(ps.clipLayers, 600, 600);
                    frame.add(graphPanel2.getGraph());
                    frame.pack();
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                }
            });
        }
    }
}
