package competition.oystein.ps.v3;

import competition.oystein.controller.MarioAction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Oystein on 13/07/15.
 */
public class ClipGen {
    boolean withActionComp = true;
    private PS_GeneralizationWActionComp ps_generalizationWActionComp;
    public int[] percept;
    public List<EdgeGen> edges = new ArrayList<>();
    public boolean isActionClip = false;
    public boolean isWildCardClip = false;
    public int id;

    public ClipGen(PS_GeneralizationWActionComp ps_generalizationWActionComp, int[] percept) {
        this(ps_generalizationWActionComp, percept, false);
    }

    public ClipGen(PS_GeneralizationWActionComp ps_generalizationWActionComp, int[] percept, boolean isActionClip) {
        this.ps_generalizationWActionComp = ps_generalizationWActionComp;
        this.percept = percept;
        this.isActionClip = isActionClip;

        for (int i : percept) {
            if (i == -1) isWildCardClip = true;
        }

        this.id = ps_generalizationWActionComp.clipCounter;
        ps_generalizationWActionComp.clipCounter++;
    }

    public void addEdge(ClipGen toClip) {
        if (edges.contains(new EdgeGen(ps_generalizationWActionComp, this, toClip))) {
            return;
        }
        edges.add(new EdgeGen(ps_generalizationWActionComp, this, toClip));
    }

    @Override
    public String toString() {


        StringBuilder sb = new StringBuilder();
        sb.append(isActionClip ? "A" : "P");
        sb.append("[");
        if (isActionClip) {

            sb.append(MarioAction.getNameFromInt(percept));
            sb.append(" ");
        }

        for (int i : percept) {
            sb.append(i == -1 ? "#" : i);
        }
        sb.append("]");
        return sb.toString();
    }


    public double getHoppingPropabilityToClip(ClipGen end) {
        double sum = 0.0;
        double endH = -1;
        for (EdgeGen edge : edges) {
            sum += edge.h;
            if (edge.end.equals(end)) {
                endH = edge.h;
            }
        }
        return endH / sum;


    }

    public int[] hop() {
        if (withActionComp) return hopWithCheckForActionComp();
//            currentSequence.add(this);
        if (isActionClip) return percept;

        double p = PS_GeneralizationWActionComp.random.nextDouble();
        double cumulativeProbability = 0.0;
        for (EdgeGen edge : edges) {
            cumulativeProbability += getHoppingPropabilityToClip(edge.end);
            if (p <= cumulativeProbability) {
                ps_generalizationWActionComp.currentSequenceEdges.add(edge);
                return edge.end.hop();
            }

        }
        return null;
    }

    public int[] hopWithCheckForActionComp() {
//            currentSequence.add(this);
        if (isActionClip) return percept;

        double lastLargestProb = -1;
//        int lastLargestProbAction = -1;
        ClipGen lastLargestProbAction = null;
        double hoppingProb;

        double p = PS_GeneralizationWActionComp.random.nextDouble();
        double cumulativeProbability = 0.0;
        for (EdgeGen edge : edges) {

            hoppingProb = getHoppingPropabilityToClip(edge.end);
            if (edge.end.isActionClip && hoppingProb > 0.2) {
                if (lastLargestProb == -1) {//first big one
                    lastLargestProb = hoppingProb;
                    lastLargestProbAction = edge.end;
                } else {

                    ps_generalizationWActionComp.checkForActionCompestion(this, lastLargestProbAction, edge.end);
//
                }
            }


            cumulativeProbability += hoppingProb;
            if (p <= cumulativeProbability) {
                ps_generalizationWActionComp.currentSequenceEdges.add(edge);
                return edge.end.hop();
            }

        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClipGen clip = (ClipGen) o;
        return Arrays.equals(percept, clip.percept);
    }

    @Override
    public int hashCode() {
        return percept != null ? Arrays.hashCode(percept) : 0;
    }
}
