package competition.oystein.ps.v3;

import java.util.*;

/**
 * Clip Glow with association. Clip to clip etc.
 * <p>
 * Using the idea Clip glow.
 * Each clip can be exited
 * Created by Oystein on 10/04/15.
 */
public class PS_Generalization {
    public int WILDCARD = -1;
    private double gamma;

    public static Random random = new Random(System.currentTimeMillis());

    public List<Edge> currentSequenceEdges = new LinkedList<>();

    public ArrayList<ArrayList<ClipGen>> clipLayers;

    int perceptLayer = 0;
    int actionLayer;
    int numCategories;

    public PS_Generalization(double gamma, int[][] actions, int numCategories) {
        this.numCategories = numCategories;
//        System.out.println("Number of categories: " + numCategories);
        int numLayers = numCategories + 2;
        actionLayer = numLayers - 1;
        clipLayers = new ArrayList<>();
        for (int i = 0; i < numLayers; i++) {
            clipLayers.add(new ArrayList<>());
        }
        this.gamma = gamma;

        for (int[] action : actions) {
            clipLayers.get(actionLayer).add(new ClipGen(action, true));
        }
    }



    private String getColorForLayer(int layer, int totalLayers){

        double v1 = (double) layer/ (double) totalLayers;

//        String[] arr = {"[color=\""+v1+" 0.700 0.700\"]","color=\"0.002 0.999 0.999\""};
//        return "[color=\""+v1+" 0.700 0.700\"]";
//        return "";
        return "[color=green]";

    }


    public void countEdges(){
        int size=0;
        for (ArrayList<ClipGen> layer: clipLayers) {
            for (ClipGen c: layer){
                size+=c.edges.size();
            }
        }
        System.out.println("num edges: "+size);
    }

    @Override
    public String toString() {
        StringBuilder sbLayers = new StringBuilder("Layers: {");
        for (int i = 0; i < clipLayers.size(); i++) {
            sbLayers.append(clipLayers.get(i).size());
            if(i<clipLayers.size()-1)
                sbLayers.append(",");
        }
        sbLayers.append("} ");

        return String.format("PS gen. γ=%.4f , %s",gamma,sbLayers.toString());
    }

    public int[] getAction(int[] percept) {
        percept=percept.clone();

        ClipGen perceptClipGen = new ClipGen(percept);

        int indexOf = clipLayers.get(perceptLayer).indexOf(perceptClipGen);
        if (indexOf == -1) {
            addClip(perceptClipGen);
        } else {
            perceptClipGen = clipLayers.get(perceptLayer).get(indexOf);
        }

        return perceptClipGen.hop();
    }

    private int[] createWildCard(int[] p1, int[] p2) {
        int[] newP = new int[p1.length];
        for (int i = 0; i < p1.length; i++) {
            newP[i] = p1[i] == p2[i] ? p1[i] : WILDCARD;
        }
        return newP;
    }

    private int countDiffElements(int[] p1, int[] p2) {
        int diff = 0;
        for (int i = 0; i < p1.length; i++) {
            diff += p1[i] == p2[i] ? 0 : 1;
        }
        return diff;
    }

    private void addClip(ClipGen perceptClipGen) {
        //look for match in clip layer
        for (ClipGen clipGen : clipLayers.get(perceptLayer)) {
            int diffElements = countDiffElements(perceptClipGen.percept, clipGen.percept);
            if (diffElements <= 0 || diffElements > numCategories) continue;

            int[] wildCardPercept = createWildCard(perceptClipGen.percept, clipGen.percept);
            ClipGen wildCardClipGen = new ClipGen(wildCardPercept);

            if (clipLayers.get(diffElements).contains(wildCardClipGen)) continue;

            clipLayers.get(diffElements).add(wildCardClipGen);
//            addWildCardClip(wildCardClip);
            addOrComfirmAllEdgesExist();
        }


        clipLayers.get(perceptLayer).add(perceptClipGen);

        addOrComfirmAllEdgesExist();
        // add direct edge to action clips
//        clipLayers.get(actionLayer).forEach(perceptClip::addEdge);


    }

    /**
     * Call this for all clips (except action clips) every time a clip is created
     * @param clipGen this
     * @param thisLayer current layer
     */
    private void addEdgesToEveryThingBelow(ClipGen clipGen, int thisLayer){
        for (int i = thisLayer+1; i < clipLayers.size(); i++) {
            clipLayers.get(i).forEach(clipGen::addEdge);
        }
    }
    private void addOrComfirmAllEdgesExist(){
        //skip action layer
        for (int i = 0; i < clipLayers.size() - 1; i++) {
            for (ClipGen c: clipLayers.get(i)){
                addEdgesToEveryThingBelow(c,i);
            }
        }
    }

//    private void addWildCardClip(Clip wildCardClip) {
////        System.out.println("Created new WildCard clip " + wildCardClip);
//        for (Clip actionClip : clipLayers.get(actionLayer)) {
//            wildCardClip.addEdge(actionClip);
//        }
//
//        for (Clip perceptClip : clipLayers.get(perceptLayer)) {
//            perceptClip.addEdge(wildCardClip);
//        }
//
//    }

    int longestSequence =0;

    public void giveReward(double reward) {
//        System.out.println();
//        System.out.println("|S|=" + currentSequenceEdges.size() + " : " + currentSequenceEdges);
//        System.out.println();

        if(currentSequenceEdges.size()>longestSequence){
            longestSequence=currentSequenceEdges.size();
            System.out.println(currentSequenceEdges.size()+" : "+currentSequenceEdges);
        }
        if(reward<0)
            reward=0;

        for (ArrayList<ClipGen> clipGenLayer : clipLayers) {
            for (ClipGen clipGen : clipGenLayer) {
                for (Edge edge : clipGen.edges) {
                    if (currentSequenceEdges.contains(edge))
                        edge.update(reward);
                    else edge.update(0);
                }
            }
        }


        currentSequenceEdges.clear();

    }


    public class ClipGen {
        public int[] percept;
        public List<Edge> edges = new ArrayList<>();
        public boolean isActionClip = false;

        public ClipGen(int[] percept) {
            this.percept = percept;
        }

        public ClipGen(int[] percept, boolean isActionClip) {
            this.percept = percept;
            this.isActionClip = isActionClip;
        }

        public void addEdge(ClipGen toClipGen) {
            if(edges.contains(new Edge(this, toClipGen))){
                return;
            }
            edges.add(new Edge(this, toClipGen));
        }

        @Override
        public String toString() {

            if (isActionClip) {
                return String.format("A%s", Arrays.toString(percept));
            }
            return String.format("P%s", Arrays.toString(percept));
//            return "C{" + Arrays.toString(percept) + '}';
        }

        public double getHoppingPropabilityToClip(ClipGen end) {
            double sum = 0.0;
            double endH = -1;
            for (Edge edge : edges) {
                sum += edge.h;
                if (edge.end.equals(end)) {
                    endH = edge.h;
                }
            }
            return endH / sum;


        }

        public int[] hop() {
//            currentSequence.add(this);
            if (isActionClip) return percept;

            double p = random.nextDouble();
            double cumulativeProbability = 0.0;
            for (Edge edge : edges) {
                cumulativeProbability += getHoppingPropabilityToClip(edge.end);
                if (p <= cumulativeProbability) {
                    currentSequenceEdges.add(edge);
                    return edge.end.hop();
                }

            }
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ClipGen clipGen = (ClipGen) o;
            return Arrays.equals(percept, clipGen.percept);
        }

        @Override
        public int hashCode() {
            return percept != null ? Arrays.hashCode(percept) : 0;
        }
    }

    public class Edge {
        public double h = 1;
        public ClipGen start, end;

        public Edge(ClipGen start, ClipGen end) {
            this.start = start;
            this.end = end;
        }

        public void update(double reward) {
            h = h - gamma * (h - 1) + reward;

        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Edge edge = (Edge) o;
            return !(end != null ? !end.equals(edge.end) : edge.end != null) && !(start != null ? !start.equals(edge.start) : edge.start != null);
        }

        @Override
        public int hashCode() {
            int result = start != null ? start.hashCode() : 0;
            result = 31 * result + (end != null ? end.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return String.format("E{%s -> %s h=%.3f}", start, end, h);
        }
    }

    public static void main(String[] args) {
        experimentFromPaper();
    }


    public static int[] GREEN_LEFT =  {0, 0};
    public static int[] GREEN_RIGHT = {0, 1};
    public static int[] RED_LEFT =    {1, 0};
    public static int[] RED_RIGHT =   {1, 1};

    public static int[] ACTION_STOP = {0};
    public static int[] ACTION_GO =   {1};

    public static void experimentFromPaper() {


        int[][] percepts = {GREEN_LEFT, GREEN_RIGHT, RED_LEFT, RED_RIGHT};
        int[][] actions = {ACTION_STOP, ACTION_GO};


        PS_Generalization ps = new PS_Generalization(0.0, actions, percepts[0].length);

        double correctCount = 0;
        double numTests = 4000;

        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[0] == action[0];
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[0] != action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }

        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[1] == action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[1] != action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }


        System.out.println("Correct count : " + correctCount / numTests);
    }


}
