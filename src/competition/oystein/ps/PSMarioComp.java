package competition.oystein.ps;

import competition.oystein.controller.helpers.Pair;

/**
 * Created by Oystein on 25/02/15.
 */
public class PSMarioComp extends PSMario {

    int[] compoMemory;

    public PSMarioComp(int numPercepts, int numActions, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        super(numPercepts, numActions, gamma, rewardSuccess, reflectionTime, damperGow);


    }





    @Override
    public int getAction(int percept) {
        if(debugStateCounter[percept]==0){

        }

        debugStateCounter[percept]++;
        double p = random.nextDouble();

        double cumulativeProbability = 0.0;
        for (int i = 0; i < h[percept].length; i++) {

            cumulativeProbability += getHoppingPropability(percept, i);
            if (p <= cumulativeProbability) {

                if(!emotions[percept][i] && currReflectionTime<reflectionTime) {
                    currReflectionTime++;
                    return getAction(percept);
                }


                lastSelectedPerc = percept;
                lastSelectedAction = i;
                g[percept][i] = 1.0;

                //new

                lastTimeUsed[percept][i] = currentTimeStep;
                currentTimeStep++;
                exitedEdges.add(new Pair(percept,i));
//                System.out.println("Added ["+percept+"]["+i+"]");
                return i;
            }
        }

        //debug shit
        double[] posActions = h[percept];
        System.out.println("no actions. Should never happen.");
        return -1;
    }
}
