package competition.oystein.ps;

/**
 * Created by Oystein on 27.08.14.
 */
public class MyUtils {

    public static String toStr(double d){
        return String.format("%.3f",d);
    }
    public static String toStrAsPropability(double d){
        return String.format("%.0f%%",d*100);
    }




    /**
     * Safe softmax. if weights are larger than 700, e^720 overflows..
     * Runtime o(|a|)
     * @param arr
     * @param a
     * @return
     */
    public static double softMax(double[] arr,double a){

        return Math.exp((a-logSumOfExponentials(arr)));
    }

    public static double logSumOfExponentials(double[] xs) {
        if (xs.length == 1) return xs[0];
        double max = maximum(xs);
        double sum = 0.0;
        for (int i = 0; i < xs.length; ++i)
            if (xs[i] != Double.NEGATIVE_INFINITY)
                sum += Math.exp(xs[i] - max);
        return max + Math.log(sum);
    }

    private static double maximum(double[] xs) {
        double currMax = Double.MIN_VALUE;
        for (double x : xs)
            if (x > currMax) currMax = x;
        return currMax;
    }
    private static double maximumJava7(double[] xs) {
        double currMax = Double.MIN_VALUE;
        for (int i = 0; i < xs.length; i++) {

            if (xs[i] > currMax) {
                currMax = xs[i];
            }
        }
        return currMax;
    }




    public static double softMaxIgnoreNegative(double[] arr,double a){
        if(a==PSMarioExtenable.H_UNSEEN_ACTION)
            return 0.0;

        return Math.exp((a-logSumOfExponentialsIgnoreNegative(arr)));
    }

    public static double logSumOfExponentialsIgnoreNegative(double[] xs) {
        if (xs.length == 1) return xs[0];
        double max = maximum(xs);
        double sum = 0.0;
        for (int i = 0; i < xs.length; ++i)
            if (xs[i] != Double.NEGATIVE_INFINITY && xs[i]!=PSMarioExtenable.H_UNSEEN_ACTION)
                sum += Math.exp(xs[i] - max);

        return max + Math.log(sum);
    }




    public static void main(String[] args) {
        double[] a = {1,3,2};
        double[] b = {1,3,PSMarioExtenable.H_UNSEEN_ACTION,2};


        System.out.println(softMax(a,a[1]));
        System.out.println(softMaxIgnoreNegative(b, b[1]));
        double sum1=0.0;
        double sum2=0.0;
        for (int i = 0; i < b.length; i++) {
            if(i<a.length)
            sum1+=softMax(a,a[i]);

            sum2+=softMaxIgnoreNegative(b,b[i]);
            System.out.println(i+" "+sum1);
            System.out.println(i+" "+sum2);

        }
        System.out.println(sum1);
        System.out.println(sum2);




    }
}
