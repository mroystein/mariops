package competition.oystein.ps;

import competition.oystein.controller.MarioAction;
import competition.oystein.controller.helpers.*;
import competition.oystein.controller.states.IState;
import competition.oystein.controller.states.State;
import competition.oystein.controller.states.StaticState;


import java.io.Serializable;
import java.util.*;

/**
 * A matrix implementation of PS ECM.
 * The clip network are represented as a matrix.
 * <p>
 * <p>
 * Created by Oystein on 02/10/2014.
 */
public class PSMario2 extends PSMario {

    private  int numPercepts;
    private  int numActions;
    IMatrix h;
    IMatrix g;

    public PSMario2(int numPercepts, int numActions, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        this.numPercepts = numPercepts;
        this.numActions = numActions;
        this.reflectionTime = reflectionTime;
        this.rewardSuccess = rewardSuccess;
        this.gamma = gamma;
        this.damperGow = damperGow;

//        h = new double[numPercepts][numActions];
//        g = new double[numPercepts][numActions];


//        h = new MatrixArray(numPercepts, numActions);
//        g = new MatrixArray(numPercepts, numActions);
        h=new MatrixHash(numPercepts,numActions);
        g=new MatrixHash(numPercepts,numActions);
        emotions = new boolean[numPercepts][numActions];
        lastTimeUsed = new int[numPercepts][numActions];

        debugStateCounter = new int[numPercepts];
        reset();
        validateParamters();


    }

    public PSMario2(int numPercepts, int numActions, StoredPSConfig config) {
        this(numPercepts, numActions, config.gamma, config.reward, 1, config.damperGlow);
    }

    public PSMario2() {
    }

    public void reset() {
        h.reset();
        g.reset();
        for (int i = 0; i < numPercepts; i++) {
            for (int j = 0; j < numActions; j++) {
//                h[i][j] = 1.0;
//                g[i][j] = 0.0;
                emotions[i][j] = true;
            }
        }
    }


    public int getAction(int percept) {
        debugStateCounter[percept]++;
        double p = random.nextDouble();

        double cumulativeProbability = 0.0;
        double[] actions = h.getActions(percept);
        for (int i = 0; i < actions.length; i++) {

            cumulativeProbability += getHoppingPropability(percept, i);
            if (p <= cumulativeProbability) {
//                if(!emotions[percept][i] && currReflectionTime<reflectionTime) {
//                    currReflectionTime++;
//                    return getAction(percept);
//                }

                lastSelectedPerc = percept;
                lastSelectedAction = i;
                g.setValue(percept,i,1.0);
                                //new

                lastTimeUsed[percept][i] = currentTimeStep;
                currentTimeStep++;
                Pair pair = new Pair(percept, i);
                if (uniqueExitedList) {
                    if (!exitedEdges.contains(pair))
                        exitedEdges.add(pair);
                }  else
                        exitedEdges.add(pair);
//                System.out.println("Added ["+percept+"]["+i+"]");
                return i;
            }
        }

        //debug shit
        double[] posActions = g.getActions(percept);
        System.out.println("no actions. Should never happen.");
        return -1;
    }

    public void giveRewardAmount(double reward) {
        if (exitedEdges.size() > debugBiggestList) {
            debugBiggestList*=10;
            System.err.println("Exited edges size : " + exitedEdges.size() + ". Not good!!");
        }

        if (rewardSuccess >= 1.0) {
            if (reward < 0)
                reward = 0;
            else reward = rewardSuccess;

        }


        if (exitedEdges.size() > debugBiggestList) {
            debugBiggestList = exitedEdges.size();
//            System.out.println(debugBiggestList);

        }
        Iterator<Pair> it = exitedEdges.iterator();
        int i, j;
        while (it.hasNext()) {
            Pair p = it.next();

            i = p.x;
            j = p.y;
            double hIJ = h.getAction(i,j);
            double gIJ = g.getAction(i,j);
            double newHIJ= hIJ - gamma * (hIJ-1)+reward*gIJ;


//            h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];
//            g[i][j] = g[i][j] - damperGow * g[i][j];
            double newGIJ = gIJ-damperGow*gIJ;

            if (newHIJ < 0.0)
                newHIJ = 0.0;

            if (newGIJ < DAMPER_GLOW_LOW) {
                newGIJ = 0.0;
                it.remove();
            }
            h.setValue(i,j,newHIJ);
            g.setValue(i,j,newGIJ);

        }


    }


    public void useSoftMax(boolean useSoftMax) {
        this.useSoftMax = useSoftMax;
    }

    protected double getHoppingPropability(int perceptInd, int actionInd) {
        if (useSoftMax) {
            return MyUtils.softMax( h.getActions(perceptInd),  h.getActions(perceptInd)[actionInd]);
        }

        return getHoppingPropabilityFormulaOne(perceptInd, actionInd);
    }

    protected double getHoppingPropabilityFormulaOne(int perceptInd, int actionInd) {
        System.err.println("Not used getHoppingProp");
        return 0;
//        double prop = h[perceptInd][actionInd];
//        double total = 0.0;
//        for (int i = 0; i < h[perceptInd].length; i++) {
//            total += h[perceptInd][i];
//        }
//        return prop / total;
    }

    @Override
    public String toString() {
        String unq = uniqueExitedList ? " Unique" : " Normal";
        return "PS2 G: γ=" + gamma + ", |S/A|=" + numPercepts + ",λ=" + rewardSuccess + ", R=" + reflectionTime + ", g=" + damperGow + "" + unq;
    }


    public String toStringEvalAfter() {
        int usedMoreThan0 = 0;
        int usedMoreThan100 = 0;
        for (int i = 0; i < debugStateCounter.length; i++) {
            if (debugStateCounter[i] > 0) usedMoreThan0++;
            if (debugStateCounter[i] > 100) usedMoreThan100++;
        }
        return String.format("Out of %d states, %d was used at all, and %d was used more than 100 times", debugStateCounter.length, usedMoreThan0, usedMoreThan100);
    }

    public void findUneededStates() {
        int n = debugStateCounter.length;// number of percepts
        int base = (int) (Math.log(n) / Math.log(2));

        boolean[][] all = new boolean[n][base];
        for (int i = 0; i < debugStateCounter.length; i++) {
            if (debugStateCounter[i] != 0)
                continue;
            //unused state. check if

            boolean[] state = Utils.intToBinary(i, base);
            all[i] = state;
        }


        for (int i = 0; i < base; i++) {
            //12


        }

    }

    public void printALlUnusedStates(IState state) {
        System.out.println("-------------- Never used states ----------------- ");
        for (int i = 0; i < debugStateCounter.length; i++) {
            if (debugStateCounter[i] > 0)
                continue;
            System.out.println(state.toStringStateNumber(i));

        }
    }


    public String toStringTopPerceptPerceptsAndActions(int numOfPercepts, IState state) {
        Pair[] pairs = new Pair[debugStateCounter.length];
        int numUnusedState = 0;
        for (int i = 0; i < debugStateCounter.length; i++) {
            pairs[i] = new Pair(i, debugStateCounter[i]);
            if (debugStateCounter[i] == 0) numUnusedState++;
        }
        Arrays.sort(pairs, Pair.byYHighestFirst);

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("\n----------------- Out of %d states (top %d percepts) ------------ %d states where never used", debugStateCounter.length, numOfPercepts, numUnusedState));
        sb.append("\n");
        for (int i = 0; i < numOfPercepts; i++) {
            int perceptInd = pairs[i].x;
            int perceptCount = pairs[i].y;

            sb.append("Percept:" + perceptInd + " used " + perceptCount + " times.");
            sb.append("\t\t\t");
            sb.append(state.toStringStateNumber(perceptInd));
            sb.append("\t\t\t\t\t\t\t");
            sb.append(toStringTopActions(perceptInd, 4));
            sb.append("\n");
        }


        return sb.toString();
    }

    public String toStringTopPerceptsAndActions(int usedMoreThan, IState state) {

        ArrayList<Integer> perceptsIndexes = new ArrayList<>();
        for (int i = 0; i < debugStateCounter.length; i++) {
            if (debugStateCounter[i] > usedMoreThan) perceptsIndexes.add(i);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("\nOut of %d states, %d was used more than %d times", debugStateCounter.length, perceptsIndexes.size(), usedMoreThan));
        sb.append("\n");
        for (Integer perceptInd : perceptsIndexes) {
            sb.append("Percept:" + perceptInd);
            sb.append("\t\t\t\t\t\t");
            sb.append(state.toStringStateNumber(perceptInd));
            sb.append("\t\t\t\t\t");
            sb.append(toStringTopActions(perceptInd, 4));
            sb.append("\n");
        }


        return sb.toString();
    }

    public String toStringTopActions(int perceptIndex, int numberOftopActions) {
        ArrayList<Integer> topActionIndexes = new ArrayList<>();
//        for (int i = 0; i < h[perceptIndex].length; i++) {
//
//        }
        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < h[perceptIndex].length; i++) {
//
//            if (h[perceptIndex][i] > 1.1)
//                sb.append(String.format("%.2f(%s) \t", h[perceptIndex][i], MarioAction.getName(i)));
//        }
        return sb.toString();
    }

    public String toStringALL() {
        StaticState staticState = new StaticState();
        State state = new State(false);
        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < h.length; i++) {
//            double bestHopping = -1;
//            int bestAction = -1;
//            for (int j = 0; j < h[i].length; j++) {
//                if (bestHopping < getHoppingPropability(i, j)) {
//                    bestHopping = getHoppingPropability(i, j);
//                    bestAction = j;
//                }
//            }
//            if (debugStateCounter[i] == 0)
//                continue;
//            boolean[] stateBoolArr = State.toBinary(i, 6);
//            String stateStr = state.stateToString(stateBoolArr);
//            staticState.setStateNumber(i);
//
//            sb.append(staticState.toString());
//            sb.append(" used:" + debugStateCounter[i] + " times. \t");
//            sb.append(MarioAction.getName(bestAction));
//            sb.append(" ");
//
//            sb.append(String.format("%.2f", bestHopping * 100) + "%");
//            sb.append("\n");
//
//        }
        return sb.toString();
    }


    public static void main(String[] args) {
        PSMario2 ps = new PSMario2(100,4,0.0,-1,1,1.0);

        int percept=55;
        int correctAction=2;
        for (int i = 0; i < 100; i++) {
            int action = ps.getAction(percept);
            ps.giveRewardAmount(action==correctAction?10:0);
        }

        for (int i = 0; i < 10; i++) {
            System.out.println(ps.getAction(percept));
        }

    }


}
