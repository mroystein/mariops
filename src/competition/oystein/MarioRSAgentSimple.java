/* Some thoughts on the Mario implementation. What is to be gained from training?
 * 1. Should the states be rough and abstract? Yes. How abstract? It should be 
 *    abstract enough that the number of state-action pairs is exceedingly large.
 *    But if it is too abstract, we may lose the approximate Markov property. 
 *    (e.g. if the defined Mario state lacks "Mario's position", then suppose
 *    two original scenes, one with Mario on high platform, the other wiht Mario 
 *    on low platform, and other parameters the same. They have the same abstract
 *    state S. But S x Action A -> undetermined for the two scenes.
 *       With that said, we hope given many trials and a large state space the
 *    effect is not affecting us.
 *  
 * 2. Learning for specific actions (keystrokes) or movement preferences?
 *    Learning for keystrokes seems to be hard, but can be tolerated. Consider we
 *    can first hard-code the preferences, and modify the reward function to "unit
 *    learn" the keystroke combo. For example, we could define first learning unit
 *    to be "advance", and set reward to be large for every step going rightward.
 *    Then we train the "search" unit, etc.
 *      After the units complete, we face the problem that given a scene, what is
 *    the task to carry out. This can be completed using a higher-level QTable, or
 *    simply estimate the reward given by carrying out each task, and pick the
 *    best-rewarded.
 *        I think the latter approach is easier, but possibly contain bugs. Let's see
 *    whether is will become a problem.
 * 
 * 3. How to let Mario advance?
 *    -given a scene, abstract to Mario state
 *    -construct a QTable AdvanceTable, containing State, Action pairs
 *    -each Action is a combination of keystrokes
 *    -the MDP is also learned, not predetermined?
 *    -the reward function: the number of steps rightward taken
 *    -possible problem: how to let Mario jump through gaps, platforms and enemies?
 *        -jump until necessary? could give negative rewards for unnecessary jumps
 *    -the Mario state should contain "complete" information about the scene
 *        -idea: "poles", where the Mario should be jumping off and how far?
 * 
 * */

package competition.oystein;

import ch.idsia.agents.Agent;
import ch.idsia.agents.LearningAgent;
import ch.idsia.benchmark.mario.engine.sprites.Mario;
import ch.idsia.benchmark.mario.environments.Environment;
import ch.idsia.benchmark.tasks.BasicTask;
import ch.idsia.benchmark.tasks.LearningTask;
import ch.idsia.benchmark.tasks.MarioCustomSystemOfValues;
import ch.idsia.tools.EvaluationInfo;
import ch.idsia.tools.MarioAIOptions;
import competition.oystein.controller.*;
import competition.oystein.controller.helpers.Utils;
import competition.oystein.controller.states.*;
import competition.oystein.controller.states.IState.RewardMethod;
import competition.oystein.gui.SimpleSwingGraphGUI;
import competition.oystein.ps.PSMario;
import competition.oystein.ps.PSMarioExtenable;
import competition.reinforcment.ILearningAlgorithm;


import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class MarioRSAgentSimple implements LearningAgent {

    public static int[] successSeeds = {-390653533,-2131073684,1796097696,1132148668,587584254,-2029651743,867463949};
    public static int[] failSeeds = {-1070788919,};

    public static Random random = new Random(System.currentTimeMillis());

    public static boolean PRINT_EVAL_INFO = true;
    public static PlayMode playMode = PlayMode.ACTION_LEARN;

    private String name;

    // Training options, task and quota.
    private MarioAIOptions options;
    private LearningTask learningTask;
//    private final PSMario ps;
    private IState currentState;

    private ILearningAlgorithm learner;

    // Fields for the Mario Agent


    // The type of phase the Agent is in.
    // INIT: initial phase
    // LEARN: accumulatively update the Qtable
    private enum Phase {
        INIT, LEARN, EVAL
    }


    private Phase currentPhase = Phase.INIT;

    private int learningTrial = 0;


//    public PSMario getPs() {
//        return ps;
//    }
    public ILearningAlgorithm getLearnger() {
        return learner;
    }

    public MarioRSAgentSimple() {
        setName("PS agent");
//        currentState = new State(false);
        currentState = new DynamicState(DynamicState.RewardMethod.SIMPLE);

//        ps = new PSMario(currentState.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0001, -1.0, 1, 0.8);
//        ps = new PSMario(currentState.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0001, 1.0, 1, 0.8);

//        StoredPSConfig{gamma=1.0E-5, damperGlow=1.0, reward=-1.0, rewardMethod=SIMPLE}
        learner = new PSMario(currentState.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 1.0E-5, -1.0, 1, 1.0);

//        ps = new PSMario(currentState.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0, -1.0, 1, 0.999);

    }


    @Override
    public String toString() {
        return learner.toString()+ " " + currentState.getName();
//        return ps.toString() + " " + currentState.getName();
    }

    public MarioRSAgentSimple(PSMario ps, DynamicState.RewardMethod rewardMethod) {
        setName("PS agent");
        currentState = new DynamicState(rewardMethod);
        this.learner = ps;
    }

    public IState getCurrentState() {
        return currentState;
    }

    public MarioRSAgentSimple(IState stateClass, double gamma, double damperGlow) {
        this(stateClass, gamma, -1.0, damperGlow);
    }

    public MarioRSAgentSimple(IState stateClass, double gamma, double reward, double damperGlow) {
        setName("PS agent");
        currentState = stateClass;
        this.learner = new PSMario(stateClass.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, gamma, reward, 1, damperGlow);
    }


    public MarioRSAgentSimple(PSMario ps, boolean simple) {
        setName("PS agent");

        if (simple)
            currentState = new State(false);
        else currentState = new DynamicState();

        this.learner = ps;
    }

    public MarioRSAgentSimple(ILearningAlgorithm learner, IState state) {
        setName("PS agent");
        currentState = state;
        this.learner = learner;
    }
    public PSMario getPs(){
        return (PSMario) learner;
    }

    String currStateStr, currActionName;
    int currActionNumber;

    @Override
    public boolean[] getAction() {
        // Transforms the best action number to action array.

//        int actionNumber = ps.getAction(currentState.getStateNumber());
        int actionNumber =  learner.getAction(currentState.getStateNumber());

        currStateStr = currentState.toString();
        currActionName = MarioAction.getName(actionNumber);
        currActionNumber = actionNumber;
//        System.out.println(s + "\t -> \t" + actionName);
//        double[] actions = ps.h[currentState.getStateNumber()];

        return MarioAction.getAction(actionNumber);
    }

    /**
     * Importance of this function: the scene observation is THE RESULT after
     * performing some action given the previous state. Therefore we could get
     * information on:
     * 1. prev state x prev action -> current state.
     * 2. get the reward for prev state, prev action pair.
     * <p>
     * The reward function, however, is not provided and has to be customized.
     */
    @Override
    public void integrateObservation(Environment environment) {
        // Update the current state.
        currentState.update(environment);


        if (currentPhase == Phase.INIT && environment.isMarioOnGround()) {
            // Start learning after Mario lands on the ground.
//            ps.giveRewardAmount(0.0);
            learner.update(0.0,currentState.getStateNumber());
            currentPhase = Phase.LEARN;
        } else if (currentPhase == Phase.LEARN) {
            // Update the Qvalue entry in the Qtable.


            double reward = currentState.calcReward();
            learner.update(currentState.calcReward(),currentState.getStateNumber());
//            ps.giveRewardAmount(reward);


        } else {
            learner.update(0.0,currentState.getStateNumber());
//            ps.giveRewardAmount(0.0);
        }
    }

    ArrayList<Integer> timesUsed = new ArrayList<>();
    ArrayList<Integer> distances = new ArrayList<>();

    private void learnOnce() {

        if (PRINT_EVAL_INFO) {
            System.out.println("================================================");
            System.out.println("Trial: " + learningTrial);
        }

//        init();

        learningTask.runSingleEpisode(1);
//        learningTask.

        EvaluationInfo evaluationInfo =
                learningTask.getEnvironment().getEvaluationInfo();

        distances.add(evaluationInfo.distancePassedCells);
        if (evaluationInfo.timeSpent == 200 && PRINT_EVAL_INFO) {
            System.out.println("Ran out of time");
        }
        int timeUsed = evaluationInfo.timeSpent;

        int score = evaluationInfo.computeWeightedFitness();
        boolean died = evaluationInfo.marioStatus == Mario.STATUS_DEAD;

        if (died) {
            timeUsed*=-1;
//            timeUsed=201;
//            if (evaluationInfo.timeSpent == 200) timesUsed.add(201);
//            else timeUsed *= -1;
        }

        timesUsed.add(timeUsed);
        if (PRINT_EVAL_INFO) {
            System.out.println("Intermediate SCORE = " + score + " TimeUsed:" + timeUsed + " Distance:" + evaluationInfo.distancePassedPhys);
            System.out.println(evaluationInfo.toStringSingleLine());
        }
//        System.out.println(timesUsed.size()+" "+distances.size());
//        System.out.println(evaluationInfo.distancePassedCells);

        learningTrial++;
    }

    public void learnAndStopIfFailing(int epochs) {
        for (int i = 0; i < epochs; i++) {
            learnOnce();

            if (i > 3) {
                if (timesUsed.get(i - 3) >= 200 && timesUsed.get(i - 2) >= 200 && timesUsed.get(i - 1) >= 200 && timesUsed.get(i) >= 200) {
                    System.out.println("\tStopped training since the 2 last " + i + " and " + (i - 1) + " was ran out of time");
                    return;
                }
            }
        }
    }

    public boolean learnUntilSuccess(int maxEpoch){
        int nGamesNeedsToPass=20;
        for (int i = 0; i < maxEpoch; i++) {
            learnOnce();

            if(i>nGamesNeedsToPass) {
               double avg= distances.subList(distances.size() - nGamesNeedsToPass, distances.size()).stream().mapToInt(x -> x).average().getAsDouble();

                if(avg>=256-1) {
                    System.out.println(" Stopped at "+i+" due to "+nGamesNeedsToPass+"  successfull sequential runs.");
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void learn() {
        // for (int m = 0; m < 3; m++) {
        //options.setMarioMode(m);

        for (int i = 0; i < 1; i++) {
            learnOnce();
        }

        //}

    }

    public void learn(int epochs) {

        for (int i = 0; i < epochs; i++) {
            learnOnce();
        }

    }


    @Override
    public void init() {

    }

    @Override
    public void reset() {
        //dont used. gets called by Mario Game Engine
//            resetAgentForNewSameAgeent();
        //currentState = new S tate();
    }

    public void resetAgentForNewSameAgeent() {
        learner.reset();
//        ps.reset();
        timesUsed.clear();
        distances.clear();
    }


    public void setOptions(MarioAIOptions options) {
        this.options = options;
    }

    /**
     * Gives access to the evaluator through learningTask.evaluate(Agent).
     */
    @Override
    public void setLearningTask(LearningTask learningTask) {
        this.learningTask = learningTask;
    }

    @Deprecated
    @Override
    public void setEvaluationQuota(long num) {
    }

    @Deprecated
    @Override
    public Agent getBestAgent() {
        return this;
    }

    @Override
    public void setObservationDetails(
            int rfWidth, int rfHeight, int egoRow, int egoCol) {
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Deprecated
    @Override
    public void newEpisode() {
    }

    @Deprecated
    @Override
    public void giveReward(float reward) {
    }

    // This function is completely bogus! intermediateReward is not properly given
    // either modify the intermediate reward calculation or ignore this function
    // and do reward update elsewhere. forexample when integrating observation.
    @Deprecated
    @Override
    public void giveIntermediateReward(float intermediateReward) {
        // TODO Auto-generated method stub
    }


    private enum PlayMode {
        HUMAN, PS, TRAIN, TEST, ACTION_LEARN, DEMO,BEST
    }

    public static void main(String[] args) {


        MarioRSAgentSimple agent;// = new MarioRSAgentSimple();
        MarioAIOptions marioAIOptions = new MarioAIOptions();

        marioAIOptions.setFPS(24);
        marioAIOptions.setVisualization(false);
        marioAIOptions.setLevelDifficulty(0);
//        marioAIOptions.set
//        marioAIOptions.setEnemies("off");
//        marioAIOptions.setGapsCount(false);
        marioAIOptions.setLevelRandSeed(0);

//        marioAIOptions.setFlatLevel(true);

        PSMario ps;
        IState state;
        BasicTask basicTask;
        PSMario psMario;
        PSMarioExtenable psMarioExtenable;
        switch (playMode) {
            case HUMAN:
//                marioAIOptions.setGameViewer(true);
//                playKeyboard(marioAIOptions);
                marioAIOptions.setTimeLimit(4000);
                marioAIOptions.setMarioInvulnerable(true);
                marioAIOptions.setLevelLength(5000);
                marioAIOptions.setVisualization(true);
                marioAIOptions.setAgent(new MyHumanAgentDebug());
                basicTask = new BasicTask(marioAIOptions);
                basicTask.setOptionsAndReset(marioAIOptions);
                basicTask.doEpisodes(3, true, 1);
                break;
            case PS:
                agent = new MarioRSAgentSimple();
                agent.setLearningTask(new LearningTask(marioAIOptions));
                marioAIOptions.setAgent(agent);
                marioAIOptions.setVisualization(true);
                basicTask = new BasicTask(marioAIOptions);
                basicTask.runSingleEpisode(4);
                break;
            case TRAIN:
                agent = new MarioRSAgentSimple();
                agent.setLearningTask(new LearningTask(marioAIOptions));
                marioAIOptions.setAgent(agent);
                agent.learn(10);
                 ps = (PSMario) agent.getLearnger();
                System.out.println(ps.toStringEvalAfter());
//                System.out.println(agent.ps.toStringTopPerceptsAndActions(50));
                marioAIOptions.setVisualization(true);
                basicTask = new BasicTask(marioAIOptions);
                basicTask.runSingleEpisode(1);
                break;
            case TEST:

                state = new HardCodedState2(RewardMethod.SIMPLE_WITH_PUNISH);
//                HardCodedState ds  = new HardCodedState(RewardMethod.SIMPLE_WITH_PUNISH);
//                DynamicState ds = new DynamicState(RewardMethod.SIMPLE_WITH_PUNISH);
                agent = new MarioRSAgentSimple(state, 0.00001, -1.0, 0.3); //new MarioRSAgentSimple(hcs, 1.0E-4,1.0);
                marioAIOptions.setAgent(agent);

                marioAIOptions.setMarioInvulnerable(true);
                agent.setLearningTask(new LearningTask(marioAIOptions));
                agent.learn(200);


                marioAIOptions.setMarioInvulnerable(false);
                int numGamesPerLevel = 400;
                for (int i = 0; i < 4000; i += numGamesPerLevel) {
                    marioAIOptions.setLevelRandSeed(random.nextInt());
                    agent.setLearningTask(new LearningTask(marioAIOptions));
                    agent.learn(numGamesPerLevel);
                }


                if (agent.timesUsed.size() > 2) {
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.distances, agent.toString(), "Distances", "Epoch", "Distance", 0, 280));
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.timesUsed, agent.toString(), "Timesteps (200= ran out of time)", "Epoch", "Time", -220, 220));
                    ps = (PSMario) agent.getLearnger();
//                    System.out.println(agent2.getPs().toStringTopPerceptsAndActions(40, ds));
                    System.out.println(ps.toStringTopPerceptPerceptsAndActions(25, state));
                }
                ps = (PSMario) agent.getLearnger();
                System.out.println(ps.toStringEvalAfter());
                ps.printALlUnusedStates(state);
//                System.out.println(agent.ps.toStringTopPerceptsAndActions(50));
//                marioAIOptions.setLevelRandSeed(0);
                marioAIOptions.setVisualization(true);
                basicTask = new BasicTask(marioAIOptions);
                basicTask.runSingleEpisode(10);
                break;

            case ACTION_LEARN:
                state = new HardCodedState(RewardMethod.SIMPLE_WITH_PUNISH);
                state = new DynamicState3(RewardMethod.PS_REWARD);
//                HardCodedState hcs3  = new HardCodedState(RewardMethod.EXAMPLE_WITH_PUNISH);


                psMarioExtenable = new PSMarioExtenable(
                        state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, MarioAction.BASIC_ACTIONS, 1.0E-5, -1.0, 1, 0.995);
                agent = new MarioRSAgentSimple(psMarioExtenable, state);
                marioAIOptions.setAgent(agent);

                //marioAIOptions.setMarioInvulnerable(true);
                // agent3.setLearningTask(new LearningTask(marioAIOptions));
                // agent3.learn(1000);

                marioAIOptions.setMarioInvulnerable(false);
                agent.setLearningTask(new LearningTask(marioAIOptions));
//                agent.learn(1000);

//                System.out.println("Distances "+agent3.distances.size());

                if (agent.timesUsed.size() > 2) {
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.distances, agent.toString(), "Distances", "Epoch", "Distance", 0, 300));
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.timesUsed, agent.toString(), "Timesteps (200= ran out of time)", "Epoch", "Time", 0, 300));
                    System.out.println(psMarioExtenable.toStringTopPerceptsAndActions(40, state));
                    System.out.println(psMarioExtenable.toStringTopPerceptPerceptsAndActions(25, state));
                }
//                EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent3.timesUsed));
//                EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent3.distances));


                System.out.println("Learned actions:   " + psMarioExtenable.getTheNewLearnedActions() + " size:" + psMarioExtenable.getTheNewLearnedActions().size());
                System.out.println("All known actions: " + psMarioExtenable.getAllActionsKnown() + " size:" + psMarioExtenable.getAllActionsKnown().size());

                List<MarioAction> notLearnedList = Arrays.stream(MarioAction.values()).filter(ma -> !psMarioExtenable.getAllActionsKnown().contains(ma)).collect(Collectors.toList());
                System.out.println("Actions not learned: " + notLearnedList);

                System.out.println("PS num actions total: " + psMarioExtenable.numActionTaken + ", num compositionTesting: " + psMarioExtenable.numActionCompChecking);
//                System.out.println("-----------------------------------");
//                System.out.println(psMarioExtenable.toStringTopPerceptPerceptsAndActions(100,hcs3));
//                System.out.println(agent.ps.toStringTopPerceptsAndActions(50));
                marioAIOptions.setVisualization(true);
                basicTask = new BasicTask(marioAIOptions);
                basicTask.runSingleEpisode(10);
                break;

            case BEST:
//                state = new DynamicState3(RewardMethod.PS_REWARD);
                state = new DynamicState(RewardMethod.PS_REWARD);
//                HardCodedState hcs3  = new HardCodedState(RewardMethod.EXAMPLE_WITH_PUNISH);

                psMario = new PSMario(state.getNumberOfStates(),MarioAction.TOTAL_ACTIONS, 0.0, -1.0, 1, 0.339);

                agent = new MarioRSAgentSimple(psMario, state);
                marioAIOptions.setAgent(agent);

                //marioAIOptions.setMarioInvulnerable(true);
                // agent3.setLearningTask(new LearningTask(marioAIOptions));
                // agent3.learn(1000);

                marioAIOptions.setFlatLevel(true);
                marioAIOptions.setVisualization(false);
                marioAIOptions.setFPS(80);
                marioAIOptions.setMarioInvulnerable(false);
                agent.setLearningTask(new LearningTask(marioAIOptions));
                agent.learn(500);

//                System.out.println("Distances "+agent3.distances.size());

                if (agent.timesUsed.size() > 2) {
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.distances, agent.toString(), "Distances", "Epoch", "Distance", 0, 300));
//                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.timesUsed, agent.toString(), "Timesteps (200= ran out of time)", "Epoch", "Time", 0, 300));
                }

                marioAIOptions.setVisualization(true);
                basicTask = new BasicTask(marioAIOptions);
                basicTask.runSingleEpisode(10);
                break;


            case DEMO:
                state = new HardCodedState2(RewardMethod.SIMPLE_WITH_PUNISH);
//               state= new DynamicState(RewardMethod.SIMPLE_WITH_PUNISH);
                psMarioExtenable = new PSMarioExtenable(
                        state.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, MarioAction.BASIC_ACTIONS, 0.0, -1.0, 1, 0.3);
                agent = new MarioRSAgentSimple(psMarioExtenable, state);
                marioAIOptions.setAgent(agent);


                int[] randomTracks = new int[1];
                for (int i = 0; i < randomTracks.length; i++) {
//                    randomTracks[i] = random.nextInt(Integer.MAX_VALUE);
                    randomTracks[i] = i;
                }
                System.out.println("Random tracks : " + Arrays.toString(randomTracks));
//                marioAIOptions.setVisualization(true);
//                marioAIOptions.setFPS(84);
//                basicTask = new BasicTask(marioAIOptions);
//                basicTask.runSingleEpisode(3);

//                marioAIOptions.setVisualization(false);
//                marioAIOptions.setMarioInvulnerable(true);
//                agent.setLearningTask(new LearningTask(marioAIOptions));
//                agent.learn(1000);


                marioAIOptions.setMarioInvulnerable(false);
                int nGamesPerLvl = 1000;
                //random tracks
                List<Integer> failedLvlsList = new ArrayList<>();
                List<Integer> successLvlsList = new ArrayList<>();
                for (int randomTrack : randomTracks) {
                    System.out.println();
                    marioAIOptions.setLevelRandSeed(randomTrack);
                    agent.setLearningTask(new LearningTask(marioAIOptions));
                    agent.learn(nGamesPerLvl);

                    boolean success = agent.learnUntilSuccess(nGamesPerLvl);

//                    double avg = agent.distances.subList(agent.distances.size() - nGamesPerLvl, agent.distances.size())
//                            .stream().mapToInt(x -> x).average().getAsDouble();
                    double avgLast20 = agent.distances.subList(agent.distances.size() - 20, agent.distances.size())
                            .stream().mapToInt(x -> x).average().getAsDouble();

                    List<Integer> last20 = agent.distances.subList(agent.distances.size() - 20, agent.distances.size())
                            .stream().collect(Collectors.toList());
                    System.out.println("Level "+randomTrack+"  success: "+success+" avgLast20: "+avgLast20 + " "+last20.toString());
                    if(success)
                        successLvlsList.add(randomTrack);
                    else
                        failedLvlsList.add(randomTrack);



//

                }


//                System.out.println("Distances "+agent3.distances.size());

                if (agent.timesUsed.size() > 2) {
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.distances, agent.toString(), "Distances", "Epoch", "Distance", 0, 300));
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.timesUsed, agent.toString(), "Timesteps (200= ran out of time)", "Epoch", "Time", 0, 300));
//                    System.out.println(psMarioExtenable.toStringTopPerceptsAndActions(40, state));
                    System.out.println(psMarioExtenable.toStringTopPerceptPerceptsAndActions(25, state));
                }


                System.out.println("Learned actions:   " + psMarioExtenable.getTheNewLearnedActions() + " size:" + psMarioExtenable.getTheNewLearnedActions().size());
                System.out.println("All known actions: " + psMarioExtenable.getAllActionsKnown() + " size:" + psMarioExtenable.getAllActionsKnown().size());

                System.out.println("Failed "+failedLvlsList.size()+" of "+randomTracks.length+ " tracks. "+failedLvlsList.toString());

                Utils.saveObjectToFile(psMarioExtenable,"ps20RandLvls");

                for (int failedLvlSeed : failedLvlsList) {
                    marioAIOptions.setFPS(84);
                    marioAIOptions.setLevelRandSeed(failedLvlSeed);
                    marioAIOptions.setVisualization(true);
                    basicTask = new BasicTask(marioAIOptions);
                    basicTask.runSingleEpisode(1);
                }

                System.out.println("Success lvls: "+successLvlsList.toString());
                for (int lvlSeed: successLvlsList){
                    marioAIOptions.setFPS(84);
                    marioAIOptions.setLevelRandSeed(lvlSeed);
                    marioAIOptions.setVisualization(true);
                    basicTask = new BasicTask(marioAIOptions);
                    basicTask.runSingleEpisode(1);

                }

                break;


        }


//        System.out.println(agent.ps.toStringALL());
//
//        XYSeries seriesTimeUsed = new XYSeries("Time steps");
//        for (int i = 0; i < agent.timesUsed.size(); i++) {
//            seriesTimeUsed.add(i, agent.timesUsed.get(i));
//        }
//
//
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(seriesTimeUsed));


        //System.exit(0);

    }

    private static void playKeyboard(MarioAIOptions marioAIOptions) {
        marioAIOptions.setAgent(new MyHumanAgentDebug());
        final BasicTask basicTask = new BasicTask(marioAIOptions);
        basicTask.setOptionsAndReset(marioAIOptions);
        basicTask.doEpisodes(2, true, 1);
        System.exit(0);
    }


    private static void play2() {
        boolean PLAY_MYSELF = false;
        Agent agent = new MarioRSAgentSimple();

        int fps = 24;


        final MarioAIOptions marioAIOptions = new MarioAIOptions();

        marioAIOptions.setFPS(fps);
        marioAIOptions.setTimeLimit(Integer.MAX_VALUE);
//        marioAIOptions.set
        marioAIOptions.setEnemies("off");
        marioAIOptions.setLevelDifficulty(0);
        marioAIOptions.setFlatLevel(true);//makes it easy
        //marioAIOptions.setVisualization(false);
        marioAIOptions.setVisualization(true);

        if (PLAY_MYSELF) {
            playKeyboard(marioAIOptions);

        } else {
//            marioAIOptions.setAgent(new HumanKeyboardAgent());
            marioAIOptions.setAgent(agent);
        }
        final BasicTask basicTask = new BasicTask(marioAIOptions);

//        basicTask.reset(marioAIOptions);
        final MarioCustomSystemOfValues m = new MarioCustomSystemOfValues();
//        basicTask.runSingleEpisode(1);
        // run 1 episode with same options, each time giving output of Evaluation info.
        // verbose = false
        basicTask.doEpisodes(5, false, 1);
        System.out.println("\nEvaluationInfo: \n" + basicTask.getEnvironment().getEvaluationInfoAsString());
        System.out.println("\nCustom : \n" + basicTask.getEnvironment().getEvaluationInfo().computeWeightedFitness(m));


        System.exit(0);
    }


}
