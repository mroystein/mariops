package competition.reinforcment;

import competition.oystein.controller.MarioAction;
import competition.oystein.controller.helpers.IMatrix;
import competition.oystein.controller.helpers.MatrixHash;
import competition.oystein.controller.helpers.Pair;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Oystein on 04/03/15.
 */
public class QLearning implements ILearningAlgorithm{
    public boolean DEBUG_PRINT = false;

    private final int numStates;
    private final int numActions;
    Random random = new Random(System.currentTimeMillis());
//    double[][] table;

    IMatrix table;

    int[][] transitions;

    int prevState;
    int prevAction;

    double explorationChance = 0.3d;//0.3d;

//    HashMap<Integer, HashMap<Integer,Integer>> transMap;

    /**
     * Learning rate determines how much new information will overrite old information.
     * 0 Will not learn anything, 1 only consider the old information.
     * Transtion from 1 -> 0
     */
    private double learningRate = 1.0;//0.8d;//0.8d;
    private double learningScale = 3.123;

    /**
     * How much future matter
     */
    double gamma = 0.5;//0.6d;


    int[] debugOutLearnedState;

    public QLearning(int numStates, int numActions) {
        this.numStates = numStates;
        this.numActions = numActions;
        reset();

    }

    public QLearning(int numStates, int numActions, double gamma, double learningScale) {
        this(numStates, numActions);
        this.gamma = gamma;
        this.learningScale = learningScale;
    }


    public void reset() {

        this.table = new MatrixHash(numStates, numActions);
        this.transitions = new int[numStates][numActions];
        debugOutLearnedState = new int[numStates];
    }
    @Override
    public String toString() {
        return String.format("Q{gamma=%.3f, learning=%.3f , scale=%.3f}", gamma, learningRate, learningScale);
    }


    @Override
    public int getAction(int stateNumber) {
        prevState = stateNumber;
        if (random.nextDouble() < explorationChance) {
            explorationChance *= 0.00001;
            prevAction = random.nextInt(numActions);
        } else
            prevAction = getBestAction(stateNumber);

        return prevAction;

    }

    private int getBestAction(int stateNumber) {
        double maxReward = Double.MIN_VALUE;
        int maxAction = -1;
        double[] Qs = table.getActions(stateNumber);
        for (int i = 0; i < numActions; i++) {
            if (Qs[i] > maxReward) {
                maxReward = Qs[i];
                maxAction = i;
            }
        }
        if (maxReward <= 0.000001) {
            if (DEBUG_PRINT)
                System.out.println("No good actions (all 0)" + stateNumber);
            return random.nextInt(numActions);
        }
        if (maxAction == -1) {
            if (DEBUG_PRINT)
                System.out.println("No good actions " + stateNumber);
            return random.nextInt(numActions);
        }
        if (DEBUG_PRINT)
            System.out.printf("q(%d,%s)=%.4f \n", stateNumber, MarioAction.getName(maxAction), maxReward);
        return maxAction;
    }

    @Override
    public void update(double reward, int currentStateNumber) {
        updateSrc(reward, currentStateNumber);

    }


    private void updateSrc(double reward, int currentStateNumber) {
        transitions[prevState][prevAction]++;

        double[] prevQs = table.getActions(prevState);
        double prevQ = prevQs[prevAction];

//        int bestAction = getBestAction(currentStateNumber);
//        double maxQ = table.getActions(currentStateNumber)[bestAction];

        //q-learning, should take the max of any action of next state
        double[] nextQs=table.getActions(currentStateNumber);
        double nextMaxQ=Double.MIN_VALUE;
        for (double nextQ : nextQs) {
            if (nextQ > nextMaxQ) nextMaxQ = nextQ;
        }

        int nTransStoA = transitions[prevState][prevAction];
//        double alpha = calcAlpha(learningRate, transitions[prevState][prevAction]);///(transitions[prevState][prevAction]+1);
//        double alpha = learningRate/nTransStoA;


        double alpha = learningRate / ((nTransStoA + learningScale) / learningScale);
        if (alpha <= 0.0001 && debugOutLearnedState[currentStateNumber] == 0) {
            debugOutLearnedState[currentStateNumber] = 1;
            //out learned
        }

        double newQ = (1 - alpha) * prevQ + alpha * (reward + gamma * nextMaxQ);

        if (DEBUG_PRINT) {
            double learnedValue = (reward * gamma * nextMaxQ);
            System.out.printf("oldQ=%.4f , newQ=%.4f , r=%.4f \n", prevQ, newQ, reward);
            if (reward > 1)
                System.out.printf("%.3f = %.3f + %.3f * (%.3f) \n", newQ, prevQ, alpha, learnedValue);
        }


        prevQs[prevAction] = newQ;

    }



    private void updateWiki(double reward, int currentStateNumber) {
//        transitions[prevState][prevAction]++;
//
//        double[] prevQs = table[prevState];
//        double prevQ = table[prevState][prevAction];
//
//        int bestAction = getBestAction(currentStateNumber);
//        double maxQ = table[currentStateNumber][bestAction];
//
//        double alpha = calcAlpha(learningRate, transitions[prevState][prevAction]);//learningRate/(transitions[prevState][prevAction]+1);
//
//        double newQ = prevQ + alpha * (reward * gamma * maxQ - prevQ);
//
//        prevQs[prevAction] = newQ;

    }

    private static double calcAlpha(double learningRate, int timesUsed) {
        double alpha = learningRate / ((double) timesUsed / 3);
        if (alpha > 0.9)
            return 0.9;

        if (alpha < 0.01)
            return 0.01;
        return alpha;
    }

    public static void main(String[] args) {
        testLearningRate();

    }

    public void toStringTopStatesAndAction(int nTopStates) {

        Pair[] pairs = new Pair[numStates];
        int unusedStates = 0;
        for (int i = 0; i < numStates; i++) {
            int stateUsedCount = Arrays.stream(transitions[i]).sum();
            pairs[i] = new Pair(i, stateUsedCount);
            if (stateUsedCount == 0) unusedStates++;
        }
        Arrays.sort(pairs, Pair.byYHighestFirst);

        System.out.println("---------------- " + numStates + " states. " + unusedStates + " states never used. -> Only used " + (numStates - unusedStates) + " states. ------------");
        for (int i = 0; i < nTopStates; i++) {

            System.out.printf("S:%d used %d times  \t\t %s \n", pairs[i].x, pairs[i].y, getTopActions(pairs[i].x));
        }
    }

    private String getTopActions(int state) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numActions; i++) {
//            if (table[state][i] > 0.0) {
            sb.append(String.format("%s(%d): %.4f \t", MarioAction.getName(i), transitions[state][i], table.getAction(state, i)));
//            }
        }
        return sb.toString();
    }

    public static void testLearningRate() {
        double learningRate = 1.0d;

        for (int i = 0; i < 200; i++) {
            double scale = 4.0;
            double alpha = learningRate / ((i + scale) / scale);
            double alpha2 = learningRate / ((double) i / 4);
            System.out.println(i + ": " + alpha);
        }
    }
}
