package competition.reinforcment;

/**
 * Created by Oystein on 09/03/15.
 */
public interface ILearningAlgorithm {

    public static final int PS_SIMPLE = 0;
    public static final int E_GREEDY = 1;
    public static final int SOFTMAX = 2;
    /**
     * Getting the next action from an agent
     * @param stateNumber
     * @return
     */
    public int getAction(int stateNumber);

    /**
     * Method for giving a reward. Current stateNumber may be used or not. (used in SARSA and Q-Learning)
     * @param reward
     * @param currentStateNumber
     */
    public void update(double reward, int currentStateNumber);

    /**
     * Reset the agent. Should behave the same as creating a new agent.
     */
    public void reset();


    public static String toStringActionSelection(int actionSelection){

        switch (actionSelection) {
            case PS_SIMPLE:{
                return "PS-Simple";
            }
            case SOFTMAX: {
                return "SoftMax";
            }
            case E_GREEDY:
                return "eGreedy";
            default: return "Unknown action selectiob. ";

        }
    }
}
