/* Some thoughts on the Mario implementation. What is to be gained from training?
 * 1. Should the states be rough and abstract? Yes. How abstract? It should be 
 *    abstract enough that the number of state-action pairs is exceedingly large.
 *    But if it is too abstract, we may lose the approximate Markov property. 
 *    (e.g. if the defined Mario state lacks "Mario's position", then suppose
 *    two original scenes, one with Mario on high platform, the other wiht Mario 
 *    on low platform, and other parameters the same. They have the same abstract
 *    state S. But S x Action A -> undetermined for the two scenes.
 *       With that said, we hope given many trials and a large state space the
 *    effect is not affecting us.
 *  
 * 2. Learning for specific actions (keystrokes) or movement preferences?
 *    Learning for keystrokes seems to be hard, but can be tolerated. Consider we
 *    can first hard-code the preferences, and modify the reward function to "unit
 *    learn" the keystroke combo. For example, we could define first learning unit
 *    to be "advance", and set reward to be large for every step going rightward.
 *    Then we train the "search" unit, etc.
 *      After the units complete, we face the problem that given a scene, what is
 *    the task to carry out. This can be completed using a higher-level QTable, or
 *    simply estimate the reward given by carrying out each task, and pick the
 *    best-rewarded.
 *        I think the latter approach is easier, but possibly contain bugs. Let's see
 *    whether is will become a problem.
 * 
 * 3. How to let Mario advance?
 *    -given a scene, abstract to Mario state
 *    -construct a QTable AdvanceTable, containing State, Action pairs
 *    -each Action is a combination of keystrokes
 *    -the MDP is also learned, not predetermined?
 *    -the reward function: the number of steps rightward taken
 *    -possible problem: how to let Mario jump through gaps, platforms and enemies?
 *        -jump until necessary? could give negative rewards for unnecessary jumps
 *    -the Mario state should contain "complete" information about the scene
 *        -idea: "poles", where the Mario should be jumping off and how far?
 * 
 * */

package competition.reinforcment;

import ch.idsia.agents.Agent;
import ch.idsia.agents.LearningAgent;
import ch.idsia.benchmark.mario.engine.sprites.Mario;
import ch.idsia.benchmark.mario.environments.Environment;
import ch.idsia.benchmark.tasks.BasicTask;
import ch.idsia.benchmark.tasks.LearningTask;
import ch.idsia.tools.EvaluationInfo;
import ch.idsia.tools.MarioAIOptions;
import competition.oystein.controller.MarioAction;
import competition.oystein.controller.MyHumanAgentDebug;
import competition.oystein.controller.states.*;
import competition.oystein.controller.states.IState.RewardMethod;
import competition.oystein.gui.SimpleSwingGraphGUI;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


public class MarioQAgent implements LearningAgent {



    public static Random random = new Random(System.currentTimeMillis());

    public static boolean PRINT_EVAL_INFO = false;
    public static PlayMode playMode = PlayMode.DEMO;

    private String name;

    // Training options, task and quota.
    private MarioAIOptions options;
    private LearningTask learningTask;
    private IState currentState;

    // Fields for the Mario Agent
    QLearning qLearning;

    // The type of phase the Agent is in.
    // INIT: initial phase
    // LEARN: accumulatively update the Qtable
    private enum Phase {
        INIT, LEARN, EVAL
    }
    private Phase currentPhase = Phase.INIT;

    private int learningTrial = 0;


    public MarioQAgent() {
        setName("Q-learning agent");
        currentState = new DynamicState(RewardMethod.SIMPLE);
        qLearning = new QLearning(currentState.getNumberOfStates(),MarioAction.TOTAL_ACTIONS);
    }

    public MarioQAgent(IState state) {
        setName("Q-learning agent");
        this.currentState=state;
        qLearning = new QLearning(currentState.getNumberOfStates(),MarioAction.TOTAL_ACTIONS);
    }


    public MarioQAgent(IState state  ,double gamma, double learningScale) {
        setName("Q-learning agent");
        this.currentState=state;
        qLearning = new QLearning(currentState.getNumberOfStates(),MarioAction.TOTAL_ACTIONS,gamma,learningScale);
    }
    @Override
    public String toString() {

        return qLearning.toString() + " " + currentState.getName();
    }


    public IState getCurrentState() {
        return currentState;
    }



    String currStateStr, currActionName;
    int currActionNumber;

    @Override
    public boolean[] getAction() {
        // Transforms the best action number to action array.

        int actionNumber = qLearning.getAction(currentState.getStateNumber());


        currStateStr = currentState.toString();
        currActionName = MarioAction.getName(actionNumber);
        currActionNumber = actionNumber;

        return MarioAction.getAction(actionNumber);
    }

    /**
     * Importance of this function: the scene observation is THE RESULT after
     * performing some action given the previous state. Therefore we could get
     * information on:
     * 1. prev state x prev action -> current state.
     * 2. get the reward for prev state, prev action pair.
     * <p>
     * The reward function, however, is not provided and has to be customized.
     */
    @Override
    public void integrateObservation(Environment environment) {
        // Update the current state.
        currentState.update(environment);


        if (currentPhase == Phase.INIT && environment.isMarioOnGround()) {
            // Start learning after Mario lands on the ground.
//            ps.giveRewardAmount(0.0);
            currentPhase = Phase.LEARN;
        } else if (currentPhase == Phase.LEARN) {
            // Update the Qvalue entry in the Qtable.


//            double reward = currentState.calcReward();
            qLearning.update(currentState.calcReward(),currentState.getStateNumber());
//            ps.giveRewardAmount(reward);


        }
//        else {
////            ps.giveRewardAmount(0.0);
//        }
    }

    ArrayList<Integer> timesUsed = new ArrayList<>();
    ArrayList<Integer> distances = new ArrayList<>();

    private void learnOnce() {

        if (PRINT_EVAL_INFO) {
            System.out.println("================================================");
            System.out.println("Trial: " + learningTrial);
        }

//        init();

        learningTask.runSingleEpisode(1);
//        learningTask.

        EvaluationInfo evaluationInfo =
                learningTask.getEnvironment().getEvaluationInfo();

        distances.add(evaluationInfo.distancePassedCells);
        if (evaluationInfo.timeSpent == 200 && PRINT_EVAL_INFO) {
            System.out.println("Ran out of time");
        }
        int timeUsed = evaluationInfo.timeSpent;

        int score = evaluationInfo.computeWeightedFitness();
        boolean died = evaluationInfo.marioStatus == Mario.STATUS_DEAD;

        if (died) {
//            timeUsed=201;
            if (evaluationInfo.timeSpent == 200) timesUsed.add(201);
            else timeUsed *= -1;
        }


        timesUsed.add(timeUsed);
        if (PRINT_EVAL_INFO) {
            System.out.println("Intermediate SCORE = " + score + " TimeUsed:" + timeUsed + " Distance:" + evaluationInfo.distancePassedPhys);
            System.out.println(evaluationInfo.toStringSingleLine());
        }

//        System.out.println(learningTrial+" "+evaluationInfo.distancePassedCells);
        learningTrial++;
    }

    public void learnAndStopIfFailing(int epochs) {
        for (int i = 0; i < epochs; i++) {
            learnOnce();

            if (i > 3) {
                if (timesUsed.get(i - 3) >= 200 && timesUsed.get(i - 2) >= 200 && timesUsed.get(i - 1) >= 200 && timesUsed.get(i) >= 200) {
                    System.out.println("\tStopped training since the 2 last " + i + " and " + (i - 1) + " was ran out of time");
                    return;
                }
            }
        }
    }

    public boolean learnUntilSuccess(int maxEpoch){
        int nGamesNeedsToPass=20;
        for (int i = 0; i < maxEpoch; i++) {
            learnOnce();

            if(i>nGamesNeedsToPass) {
               double avg= distances.subList(distances.size() - nGamesNeedsToPass, distances.size()).stream().mapToInt(x -> x).average().getAsDouble();

                if(avg>=256-1) {
                    System.out.println(" Stopped at "+i+" due to "+nGamesNeedsToPass+"  successfull sequential runs.");
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void learn() {
            learnOnce();
    }

    public void learn(int epochs) {
        for (int i = 0; i < epochs; i++) {
            learnOnce();
        }
    }


    @Override
    public void init() {

    }

    @Override
    public void reset() {
        //currentState = new State();
    }

    public void resetAgentForNewSameAgeent() {
//        ps.reset();
//        qLearning = new QLearning(currentState.getNumberOfStates(),MarioAction.TOTAL_ACTIONS);
        qLearning.reset();
        timesUsed.clear();
        distances.clear();
//        System.out.println("NOT DONE");
    }


    public void setOptions(MarioAIOptions options) {
        this.options = options;
    }

    /**
     * Gives access to the evaluator through learningTask.evaluate(Agent).
     */
    @Override
    public void setLearningTask(LearningTask learningTask) {
        this.learningTask = learningTask;
    }

    @Deprecated
    @Override
    public void setEvaluationQuota(long num) {
    }

    @Deprecated
    @Override
    public Agent getBestAgent() {
        return this;
    }

    @Override
    public void setObservationDetails(
            int rfWidth, int rfHeight, int egoRow, int egoCol) {
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Deprecated
    @Override
    public void newEpisode() {
    }

    @Deprecated
    @Override
    public void giveReward(float reward) {
    }

    // This function is completely bogus! intermediateReward is not properly given
    // either modify the intermediate reward calculation or ignore this function
    // and do reward update elsewhere. forexample when integrating observation.
    @Deprecated
    @Override
    public void giveIntermediateReward(float intermediateReward) {
        // TODO Auto-generated method stub
    }


    private enum PlayMode {
        HUMAN, PS, TRAIN, TEST, ACTION_LEARN, DEMO
    }

    public static void main(String[] args) {


        MarioQAgent agent;// = new MarioRSAgentSimple();
        MarioAIOptions marioAIOptions = new MarioAIOptions();

        marioAIOptions.setFPS(24);
        marioAIOptions.setVisualization(false);
        marioAIOptions.setLevelDifficulty(0);
//        marioAIOptions.set
        marioAIOptions.setEnemies("off");
//        marioAIOptions.setGapsCount(false);
        marioAIOptions.setLevelRandSeed(0);

//        marioAIOptions.setFlatLevel(true);


        IState state;
        BasicTask basicTask;

        switch (playMode) {
            case HUMAN:
//                marioAIOptions.setGameViewer(true);
//                playKeyboard(marioAIOptions);
                marioAIOptions.setVisualization(true);
                marioAIOptions.setAgent(new MyHumanAgentDebug());
                basicTask = new BasicTask(marioAIOptions);
                basicTask.setOptionsAndReset(marioAIOptions);
                basicTask.doEpisodes(3, true, 1);
                break;
            case TEST:
                long startTime = System.currentTimeMillis();
                state = new HardCodedState2(RewardMethod.SIMPLE);
//               state= new DynamicState(RewardMethod.SIMPLE_WITH_PUNISH);
                agent = new MarioQAgent(state);
                marioAIOptions.setAgent(agent);
//                marioAIOptions.setFPS(84);

                agent.setOptions(marioAIOptions);
//                marioAIOptions.setMarioInvulnerable(true);
                agent.setLearningTask(new LearningTask(marioAIOptions));
                agent.learn(1000);
                agent.options.setLevelRandSeed(3);
                agent.learn(1000);


                long ellaspedTime = (System.currentTimeMillis()-startTime)/1000;
                System.out.printf("Time used : "+ellaspedTime+" s");

//                System.out.printf("Training with enemies..");

//                marioAIOptions.setMarioInvulnerable(false);
//                agent.setLearningTask(new LearningTask(marioAIOptions));
//                agent.learn(400);

//                agent.distances.stream().forEach(System.out::println);
//                marioAIOptions.setFPS(24);
//
//                marioAIOptions.setVisualization(true);
//                agent.setLearningTask(new LearningTask(marioAIOptions));
//                agent.learn(10);


                EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.distances, agent.toString(), "Distances", "Epoch", "Distance", 0, 300));

                agent.qLearning.toStringTopStatesAndAction(5);

//                agent.qLearning.DEBUG_PRINT=true;
                marioAIOptions.setFPS(44);
//                marioAIOptions.setLevelRandSeed(failedLvlSeed);
                agent.options.setVisualization(true);
                agent.learn(2);


                break;

            case DEMO:
                state = new HardCodedState2(RewardMethod.EXAMPLE_WITH_PUNISH);
//               state= new DynamicState(RewardMethod.SIMPLE_WITH_PUNISH);
                agent = new MarioQAgent(state);
                marioAIOptions.setAgent(agent);


                int[] randomTracks = new int[20];
                for (int i = 0; i < randomTracks.length; i++) {
//                    randomTracks[i] = random.nextInt(Integer.MAX_VALUE);
                    randomTracks[i] = i;

                }
                System.out.println("Random tracks : " + Arrays.toString(randomTracks));


                marioAIOptions.setMarioInvulnerable(false);
                int nGamesPerLvl = 200;
                //random tracks

                for (int randomTrack : randomTracks) {
                    System.out.println();
                    marioAIOptions.setLevelRandSeed(randomTrack);
                    agent.setLearningTask(new LearningTask(marioAIOptions));
                    agent.learn(nGamesPerLvl);

                    boolean success = agent.learnUntilSuccess(nGamesPerLvl);

//                    double avg = agent.distances.subList(agent.distances.size() - nGamesPerLvl, agent.distances.size())
//                            .stream().mapToInt(x -> x).average().getAsDouble();
                    double avgLast20 = agent.distances.subList(agent.distances.size() - 20, agent.distances.size())
                            .stream().mapToInt(x -> x).average().getAsDouble();

                    List<Integer> last20 = agent.distances.subList(agent.distances.size() - 20, agent.distances.size())
                            .stream().collect(Collectors.toList());
                    System.out.println("Level " + randomTrack + "  success: " + success + " avgLast20: " + avgLast20 + " " + last20.toString());


                }


//                System.out.println("Distances "+agent3.distances.size());

                if (agent.timesUsed.size() > 2) {
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.distances, agent.toString(), "Distances", "Epoch", "Distance", 0, 300));
                    EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(agent.timesUsed, agent.toString(), "Timesteps (200= ran out of time)", "Epoch", "Time", 0, 300));
//                   }


                    break;


                }


        }
    }

    private static void playKeyboard(MarioAIOptions marioAIOptions) {
        marioAIOptions.setAgent(new MyHumanAgentDebug());
        final BasicTask basicTask = new BasicTask(marioAIOptions);
        basicTask.setOptionsAndReset(marioAIOptions);
        basicTask.doEpisodes(2, true, 1);
        System.exit(0);
    }




}
