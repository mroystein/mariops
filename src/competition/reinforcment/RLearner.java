package competition.reinforcment;

import competition.oystein.controller.helpers.IMatrix;
import competition.oystein.controller.helpers.MatrixArray;
import competition.oystein.controller.helpers.MatrixHash;
import competition.oystein.controller.helpers.Pair;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 * Generic RL algorithm Agent.
 * Supports SARSA, Q_Learning for Q updates.
 * And E_Greedy, SoftMax for action selection.
 */
public class RLearner implements ILearningAlgorithm {

    //IWorld thisWorld;

    private Random r = new Random(System.currentTimeMillis());
    // Learning types
    public static final int Q_LEARNING = 1;
    public static final int SARSA = 2;
    public static final int Q_LAMBDA = 3; // Good parms were lambda=0.05, damping=0.1, alpha=0.01, epsilon=0.1

    public boolean debugRewardStandard=true;
    // Action selection types



    int learningMethod;
    int actionSelection;

    /**
     * Chance of doing a random action, instead of the 'best'
     */
    double epsilon, epsilonCopy;
    double epsilonMulti = 0.999;
    double temp;

    /**
     * Alpha - Learning rate. 0=not learn anything, 1=only consider new information.
     */
    double alpha;
    /**
     * Gamma - 0=only consider current rewards, 1=strive for long term reward.
     */
    double gamma;
    public double lambda;


    int state;
    int newstate;
    int action;
    double reward;

    int epochs;


    //    Vector trace = new Vector();
    int[] saPair;

    long timer;

    boolean random = false;
//    Runnable a;

    IMatrix table;
    int[]transitons;



    public RLearner(int numStates, int numActions, double alpha, double gamma, double epsilon, double epsilonMulti, int learningMethod, int actionSelection) {
        setParams(alpha, gamma, epsilon, epsilonMulti, learningMethod, actionSelection);
        // Getting the world from the invoking method.
        temp = 1;
        lambda = 0.1;


        // Creating new policy with dimensions to suit the world.
//        table = new MatrixArray(numStates, numActions);
        table = new MatrixHash(numStates, numActions);
        transitons= new int[numStates];
        epsilonCopy = epsilon;


        // Initializing the policy with the initial values defined by the world.
//        policy.initValues(thisWorld.getInitValues());
    }


//    public void epochSARSA() {
//        int newaction;
//        double this_Q;
//        double next_Q;
//        double new_Q;
//
//        action = selectAction(state);
//        while (!thisWorld.hasGameEnded()) {
//
//
//            if (thisWorld.getStepCounter() > 20_000_000) {
//
//                System.out.println("Ran out of time.. fuck it");
//                break;
//            }
//
//            thisWorld.doAction(action);
//            newstate = thisWorld.getState();
//            reward = thisWorld.getReward();
//
//            newaction = selectAction(newstate);
//
//
//            this_Q = table.getAction(state, action);
//            next_Q = table.getAction(newstate, newaction);
//
//            new_Q = this_Q + alpha * (reward + gamma * next_Q - this_Q);
//
//            table.setValue(state, action, new_Q);
//
//            // Set state to the new state and action to the new action.
//            state = newstate;
//            action = newaction;
//        }
//
//
//    }



    LinkedList<Pair> traceList = new LinkedList<>();




//    private void epochQLambdaFastWorkInProgress() {
//        double max_Q;
//        double this_Q;
//        double new_Q;
//        double delta;
//
//        // Remove all eligibility traces.
//
//        traceList.clear();
//
//        while (!thisWorld.hasGameEnded()) {
//
//
//            action = selectAction(state);
//
//
//            // Store state-action pair in eligibility trace.
//
//            Pair p = new Pair(state, action);
//            traceList.add(p);
//            if (traceList.size() >= 11)
//                traceList.removeFirst();
//
//
//            thisWorld.doAction(action);
//            newstate = thisWorld.getState();
//            reward = thisWorld.getReward();
//            //debug, check if standard reward should be 1 or 0 for RL.
//            if(debugRewardStandard && reward==1.0){
//                reward=0.0;
//            }
//
//
//            max_Q = getMaxQValue(newstate);
//            this_Q = table.getAction(state, action);
//
//            // Calculate new Value for Q
//            delta = reward + gamma * max_Q - this_Q;
//            new_Q = this_Q + alpha * delta;
//
//
//            table.setValue(state, action, new_Q);
//
//            // Update values for the trace.
//            Pair pair;
//            Iterator<Pair> traceIte = traceList.iterator();
//            traceIte.next();//skip the recently added
//            int n=1;
//
//            double[][] debugNewQ = new double[traceList.size()-1][2];
//            int c=0;
//            while (traceIte.hasNext()){
//                pair=traceIte.next();
//                state = pair.a;
//                action = pair.b;
//                this_Q = table.getAction(state, action);
////                double myNewQ = this_Q + alpha * delta * Math.pow(damping * lambda, n);
//                new_Q = this_Q + alpha * delta * Math.pow(gamma * lambda, n);
//                debugNewQ[c][0]=new_Q;
//                n++;
//                c++;
//                table.setValue(state, action, new_Q);
//            }
//
//
//
////            if(traceList.size()>9)
////                System.err.println("s");
//
//            if (random) traceList.clear();
//
//            // Set state to the new state.
//            state = newstate;
//
//
//        }
//    }



    private double getMaxQValue(int state) {
        double maxQ = -Double.MAX_VALUE;
        for (double qValue : table.getActions(state)) {
            if (qValue > maxQ)
                maxQ = qValue;

        }
        return maxQ;
    }

//    private void epochQLearning() {
//        double this_Q;
//        double max_Q;
//        double new_Q;
//
//        while (!thisWorld.hasGameEnded()) {
//            // 4 1 1 0 4 4
//
//            action = selectAction(state);
//            thisWorld.doAction(action);
//            newstate = thisWorld.getState();
//            reward = thisWorld.getReward();
//
//
//            this_Q = table.getAction(state, action);
//            max_Q = getMaxQValue(newstate);
//
//            // Calculate new Value for Q
//            new_Q = this_Q + alpha * (reward + gamma * max_Q - this_Q);
//
//            table.setValue(state, action, new_Q);
//
//            // Set state to the new state.
//            state = newstate;
//        }
//
//        //System.out.println(thisWorld.);
//    }

    int prevState;
    int prevAction;
    private void updateQLearning(double reward, int currStateNumber) {

        double[] prevQs = table.getActions(prevState);
        double prevQ = prevQs[prevAction];

        double max_Q = getMaxQValue(currStateNumber);

        double newQ = prevQ+alpha*(reward+gamma*max_Q-prevQ);
        table.setValue(prevState, prevAction, newQ);

    }
    private void updateSARSA(double reward, int currStateNumber) {

        double[] prevQs = table.getActions(prevState);
        double prevQ = prevQs[prevAction];


        double nextQ = table.getAction(currStateNumber, selectAction(currStateNumber));

        double newQ = prevQ+alpha*(reward+gamma*nextQ-prevQ);
        table.setValue(prevState, prevAction, newQ);

    }


    private int selectAction(int state) {
//        epsilon*=epsilonMulti;
        double[] qValues = table.getActions(state);

        switch (actionSelection) {
            case E_GREEDY: {

                return actionSelectEGreedy(qValues,state);
            }
            case SOFTMAX: {
                return actionSelectSoftMax(qValues);
            }
        }
        return -1;
    }

    private int actionSelectSoftMax(double[] qValues) {
        int selectedAction = -1;
        int action;
        double prob[] = new double[qValues.length];
        double sumProb = 0;

        for (action = 0; action < qValues.length; action++) {
            prob[action] = Math.exp(qValues[action] / temp);
            sumProb += prob[action];
        }
        for (action = 0; action < qValues.length; action++)
            prob[action] = prob[action] / sumProb;

        boolean valid = false;
        double rndValue;
        double offset;

        while (!valid) {

            rndValue = r.nextDouble();
            offset = 0;

            for (action = 0; action < qValues.length; action++) {
                if (rndValue > offset && rndValue < offset + prob[action])
                    selectedAction = action;
                offset += prob[action];
                // System.out.println( "Action " + action + " chosen with " + prob[action] );
            }

//                    if (thisWorld.validAction(selectedAction))
            valid = true;
        }
        return selectedAction;
    }

    private int actionSelectEGreedy(double[] qValues, int state) {
        int selectedAction = -1;
        double maxQ = -Double.MAX_VALUE;
        int[] doubleValues = new int[qValues.length];
        int maxDV = 0;
        //can be replaced with MyUtils!!
        //Explore

        double epsilonNew = epsilon/(transitons[state]+1.0);
        if (r.nextDouble() < epsilonNew) {
            random = true;
            return (int) (r.nextDouble() * qValues.length);
        } else {
            random = false;
            for (int action = 0; action < qValues.length; action++) {

                if (qValues[action] > maxQ) {
                    selectedAction = action;
                    maxQ = qValues[action];
                    maxDV = 0;
                    doubleValues[maxDV] = selectedAction;
                } else if (qValues[action] == maxQ) {
                    maxDV++;
                    doubleValues[maxDV] = action;
                }
            }

            if (maxDV > 0) {
                int randomIndex = (int) (r.nextDouble() * (maxDV + 1));
                selectedAction = doubleValues[randomIndex];
            }
        }

        // Select random action if all qValues == 0 or exploring.
        if (selectedAction == -1) {
            random = true;
            selectedAction = (int) (r.nextDouble() * qValues.length);
        }

        return selectedAction;
    }


    public void setAlpha(double a) {
        if (a >= 0 && a < 1)
            alpha = a;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setGamma(double g) {
        if (g > 0 && g < 1)
            gamma = g;
    }

    public double getGamma() {
        return gamma;
    }

    public void setEpsilon(double e) {
        if (e > 0 && e < 1)
            epsilon = e;
    }

    public double getEpsilon() {
        return epsilon;
    }

    public void setEpisodes(int e) {
        if (e > 0)
            epochs = e;
    }

    public int getEpisodes() {
        return epochs;
    }

    public void setActionSelection(int as) {
        switch (as) {
            case SOFTMAX: {
                actionSelection = SOFTMAX;
                break;
            }
            case E_GREEDY:
                actionSelection = E_GREEDY;
                break;
            default: {
                actionSelection = E_GREEDY;
            }
        }
    }

    public int getActionSelection() {
        return actionSelection;
    }

    public void setLearningMethod(int lm) {
        switch (lm) {
            case SARSA: {
                learningMethod = SARSA;
                break;
            }
            case Q_LAMBDA: {
                learningMethod = Q_LAMBDA;
                break;
            }
            case Q_LEARNING:
            default: {
                learningMethod = Q_LEARNING;
            }
        }
    }

    @Override
    public String toString() {
        String lm = "";
        switch (learningMethod) {
            case SARSA:
                lm = "SARSA";
                break;
            case Q_LAMBDA:
                lm = "Q_LAMDBDA λ="+lambda;
                break;
            case Q_LEARNING:
                lm = "Q_LEARNING";
                break;
        }

        String as =ILearningAlgorithm.toStringActionSelection(actionSelection);

        return String.format("RL {a=%.4f, γ=%.4f, ε=%.4f (εStart=%.4f * %.4f), %s, %s }", alpha, gamma, epsilon, epsilonCopy, epsilonMulti, lm, as);

//        return String.format("RLeaner {alpha=%.4f, damping=%.4f, lambda=%.2f, epsilon=%.4f, %s, %s", alpha, damping, lambda, epsilonCopy, lm, as);

//        return "RLearner {" +
//                "alpha=" + alpha +
//                ", damping=" + damping +
//                ", lambda=" + lambda +
//                s + '}';
    }

    public int getLearningMethod() {

        return learningMethod;
    }


    @Override
    public int getAction(int stateNumber) {
        prevState=stateNumber;
        action = selectAction(stateNumber);
        prevAction=action;
        return action;
    }


    @Override
    public void update(double reward, int currentStateNumber) {
        switch (learningMethod) {
            case Q_LEARNING: {
               updateQLearning(reward,currentStateNumber);
                break;
            }
            case SARSA: {
                updateSARSA(reward,currentStateNumber);
                break;
            }

            case Q_LAMBDA: {
//                epochQLambdaFastWorkInProgress();
                System.err.println("not implemented");
                break;

            } // case
        } // switch

    }

    public void setParams(double alpha, double gamma, double epsilon, double epsilonMulti, int learningMethod, int actionSelection) {
        this.alpha = alpha;
        this.gamma = gamma;
        this.epsilon = epsilon;
        this.epsilonCopy = epsilon;
        this.epsilonMulti = epsilonMulti;
        this.learningMethod = learningMethod;
        this.actionSelection = actionSelection;
    }



    @Override
    public void reset() {
        epsilon = epsilonCopy;
        table.reset();
    }



    public void setParameter(String parameter, double value) {
        switch (parameter.toLowerCase()) {
            case "alpha":
                alpha = value;
                break;
            case "gamma":
                gamma = value;
                break;
            case "epsilon":
                epsilon = value;
                epsilonCopy = value;
                break;
            case "epsilonmulti":
                epsilonMulti = value;
                break;
            case "learningMethod":
                learningMethod = (int) value;
                break;
            case "actionSelection":
                actionSelection = (int) value;
                break;
            default:
                System.err.println("Unknown parameter " + parameter);
        }
    }


    public double getValue(int state, int action) {
        return table.getAction(state,action);
    }


    public static void main(String[] args) {
        double epsilon=0.01;

        for (int trans = 0; trans < 100; trans+=10) {
//            System.out.println(Math.pow(epsilon,trans));
            System.out.println(trans+ " : "+epsilon/(trans+1));
        }


    }
}

