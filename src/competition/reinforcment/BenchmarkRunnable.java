package competition.reinforcment;

import ch.idsia.benchmark.tasks.LearningTask;
import ch.idsia.tools.MarioAIOptions;
import competition.oystein.MarioRSAgentSimple;
import competition.oystein.controller.MarioAction;
import competition.oystein.controller.helpers.ConfigResultsScore;
import competition.oystein.controller.helpers.StoredPSConfig;
import competition.oystein.controller.states.DynamicState;
import competition.oystein.controller.states.HardCodedState;
import competition.oystein.controller.states.HardCodedState2;
import competition.oystein.controller.states.IState;
import competition.oystein.controller.states.IState.RewardMethod;
import competition.oystein.gui.BarChartGUI;
import competition.oystein.gui.SimpleSwingGraphGUI;
import competition.oystein.ps.PSMario;
import competition.oystein.ps.PSMarioExtenable;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Oystein on 04/03/15.
 */
public class BenchmarkRunnable {

    private MarioAIOptions marioAIOptions;

    public BenchmarkRunnable() {
        marioAIOptions = new MarioAIOptions();
        // marioAIOptions.setAgent(agent);
        marioAIOptions.setFPS(24);
        marioAIOptions.setVisualization(false);
        marioAIOptions.setLevelDifficulty(0);
//        marioAIOptions.setEnemies("off");
        marioAIOptions.setGapsCount(false);
//        marioAIOptions.setFlatLevel(true);

        marioAIOptions.setLevelRandSeed(0);
        marioAIOptions.setMarioInvulnerable(true);

    }


    public void test() {
        long startTime = System.currentTimeMillis();

        ArrayList<XYSeries> serieses = new ArrayList<>();
        int numAgents = 5;
//
        int numGamesInvurable = 500;//but will not get hurt
        int numGamesNormal = 500;
        int numGames = numGamesInvurable + numGamesNormal;
        int reflectionTime = 1;


        //How much value should next state count. 1=All, 0=Nothing
        double[] gammas = {
                0.0,
                0.2,
                0.5,
                0.8

        };
        //How much should a state->action pair count after n times.
        // 1->20
        double[] learningScales = {
                1.0,
                5.0,
                10.0,
                20.0
        };

        IState[] states = {
//                new DynamicState(),
//                new HardCodedState(),
                new HardCodedState2()
        };

        RewardMethod[] rewardMethods =
                {
                        RewardMethod.SIMPLE_WITH_PUNISH
//                        ,RewardMethod.EXAMPLE_WITH_SMALLER_PUNISH
//                        , RewardMethod.EXAMPLE_WITH_PUNISH
                };


        ArrayList<ConfigResultsScore> configResultsScores = new ArrayList<>();

        int agentCount = gammas.length * learningScales.length * rewardMethods.length * states.length;
        String headerName = "#Agents to test: " + agentCount + " #games: " + numGames + " #agents: " + numAgents + " -> " + (numGames * numAgents * agentCount) + " total games.";

        System.out.println(headerName);
        StoredPSConfig bestStoredPSConfig = null;
        double bestAvg = 0;

        int[] levelSeeds = new int[]{1};
        System.out.println("LevelSeeds " + Arrays.toString(levelSeeds));


        for (double gamma : gammas) {
            for (double learningScale : learningScales) {
                for (RewardMethod rewardMethod : rewardMethods) {
                    for (IState state : states) {

                        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
                        ConfigResultsScore configResultsScoreAverage = null;
                        String datasetName = "";
                        for (int i = 0; i < numAgents; i++) {


                            state.setRewardMethod(rewardMethod);


                            MarioQAgent agent = new MarioQAgent(state, gamma, learningScale);
                            StoredPSConfig storedPSConfig = new StoredPSConfig(agent);

//                            XYSeries seriesTimeUsed = trainAgent(numAgents, numGames, agent);
                            XYSeries[] series = trainAgentWithEnemies(1, numGamesInvurable, numGamesNormal, agent, levelSeeds);
                            ConfigResultsScore configResultsScore = new ConfigResultsScore(series[0], series[1], storedPSConfig);

                            if (configResultsScoreAverage == null)
                                configResultsScoreAverage = configResultsScore;
                            else configResultsScoreAverage.add(configResultsScore);

                            agentCount++;
                            configResultsScore.addInfoToDataSetForCharBar(dataset, i);

                            if (configResultsScore.successes > bestAvg) {
                                bestAvg = configResultsScore.successes;
                                bestStoredPSConfig = storedPSConfig;
                            }

                            datasetName = storedPSConfig.name;
                        }
                        configResultsScoreAverage.divideBy(numAgents);
                        configResultsScores.add(configResultsScoreAverage);
//                                startBarCharGUI(dataset, datasetName);


                    }
                }
            }
        }

        long ellasped = System.currentTimeMillis() - startTime;


        Collections.sort(configResultsScores, ConfigResultsScore.byMostSuccesses);
        ArrayList<StoredPSConfig> storedPSConfigs = new ArrayList<>();

        //10 or less if the list is shorter.
        int itemsToShow = 20;
        itemsToShow = configResultsScores.size() < itemsToShow ? configResultsScores.size() : itemsToShow;

        ArrayList<XYSeries> seriesesDistance = new ArrayList<>();

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        System.out.println("-------------------- Done --------------------");
        System.out.println("Top configs: " + itemsToShow);
        for (int i = 0; i < itemsToShow; i++) {
            ConfigResultsScore crs = configResultsScores.get(i);
            System.out.println(crs);
            storedPSConfigs.add(crs.storedPSConfig);

//            serieses.add(crs.seriesTimeUsed);
//            seriesesDistance.add(crs.seriesDistance);
            crs.addInfoToDataSetForCharBar(dataset);
        }

        //only add n best to chart, else too messy
        for (int i = 0; i < 3; i++) {
            ConfigResultsScore crs = configResultsScores.get(i);
            serieses.add(crs.seriesTimeUsed);
            seriesesDistance.add(crs.seriesDistance);
        }

        //failers
        System.out.printf("\n-------------------- Failers  %d --------------------", (configResultsScores.size() - itemsToShow));
        for (int i = itemsToShow; i < configResultsScores.size(); i++) {
            System.out.println(configResultsScores.get(i));
        }


//        StoredPSConfig.saveToFile(storedPSConfigs, filename);
//        PSMario psMario9 =new PSMario(ds.getNumberOfStates(), MarioAction.TOTAL_ACTIONS,0.0001,1,-1,0.990);
//        serieses.add(trainAgent(numAgents,numGames,psMario9));
//
//        PSMario psMario10 =new PSMario(currentState.getNumberOfStates(), MarioAction.TOTAL_ACTIONS, 0.0001, -1.0, 1, 0.8);
//        serieses.add(trainAgent(numAgents,numGames,psMario10));

//        int best=201;
//        for (int i = 0; i < serieses.size(); i++) {
//            int lastScore =serieses.get(i).getY(serieses.get(i).getItemCount()-1).intValue();
//            if(lastScore)
//        }


//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(serieses, headerName, "Game", "TimeUsed", -220, 220));
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(seriesesDistance, headerName, "Game", "Distances", 0, 260));

        EventQueue.invokeLater(() -> new BarChartGUI(dataset, headerName));
//
        if (numGames*levelSeeds.length > 200) {
            DefaultCategoryDataset datasetLastRounds = new DefaultCategoryDataset();
            int start = numGames - 100;
            for (int i = 0; i < itemsToShow; i++) {
                configResultsScores.get(i).addInfoToDataSetForCharBar(datasetLastRounds, start, numGames);
            }
            EventQueue.invokeLater(() -> new BarChartGUI(datasetLastRounds, "Last 100 games. " + headerName));
        }
//
//        visualizeAgent(bestPS, IState.getStateFromStateNum(bestStoredPSConfig.stateClass, bestStoredPSConfig.rewardMethod));


    }


    public XYSeries[] trainAgentWithEnemies(int numAgents, int numGamesInvurable, int numGamesNormal, MarioQAgent agent, int[] levelSeeds) {
        marioAIOptions.setAgent(agent);
//        agent.setLearningTask(new LearningTask(marioAIOptions));

        LearningTask learningTask = new LearningTask(marioAIOptions);
        System.out.println("Training agentPs " + agent.toString());

        double[] avg = new double[(numGamesInvurable + numGamesNormal) * levelSeeds.length];
        double[] avgDistance = new double[(numGamesInvurable + numGamesNormal) * levelSeeds.length];

        for (int i = 0; i < numAgents; i++) {

            agent.resetAgentForNewSameAgeent();//resets ps, and timesteplist


            for (int lvlSeed : levelSeeds) {

                marioAIOptions.setMarioInvulnerable(true);
                marioAIOptions.setLevelRandSeed(lvlSeed);
                learningTask.reset(marioAIOptions);
                agent.setLearningTask(learningTask);
                agent.learn(numGamesInvurable);

                marioAIOptions.setMarioInvulnerable(false);
                marioAIOptions.setLevelRandSeed(lvlSeed);
                learningTask.reset(marioAIOptions);
                agent.setLearningTask(learningTask);
                agent.learn(numGamesNormal);

            }


            for (int j = 0; j < avg.length; j++) {
                avg[j] += agent.timesUsed.get(j);
                avgDistance[j] += agent.distances.get(j);
            }

        }

        //divide all by the number of agents.
        XYSeries series = new XYSeries(agent.toString());
        XYSeries seriesDistance = new XYSeries(agent.toString());
        //divide all by the number of agents.
        for (int i = 0; i < avg.length; i++) {
            series.add(i, avg[i] / (double) numAgents);
            seriesDistance.add(i, avgDistance[i] / (double) numAgents);
        }

        return new XYSeries[]{series, seriesDistance};
    }

    public static void main(String[] args) {

        BenchmarkRunnable benchmark = new BenchmarkRunnable();
        benchmark.test();


    }

}
