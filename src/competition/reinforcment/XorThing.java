package competition.reinforcment;

import competition.oystein.ps.PS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by Oystein on 12/03/15.
 */
public class XorThing {

    public XorThing() {

        Random r = new Random();

        List<Boolean[]> perceptions = Arrays.asList(
                new Boolean[][]{
                        {false, false}, {false, true},
                        {true, false}, {false, false}});

        List<Boolean> actions = Arrays.asList(false, true);

        PS<Boolean[], Boolean> ps = new PS<>(perceptions, actions, 0.0, 1.0, 1, 1.0);

        for (int i = 0; i < 50; i++) {
            Boolean[] perception = perceptions.get(r.nextInt(perceptions.size()));
            Boolean action = ps.getAction(perception);
            boolean correct = (perception[0] ^ perception[1]) == action;
            ps.giveReward(correct);
        }

        perceptions.stream().forEach(p ->
                System.out.printf("\n%s : %s", print(p), print(ps.getAction(p))));


    }

    public static String print(Boolean[] a) {
        return print(a[0]) + "" + print(a[1]);
    }

    public static String print(Boolean a) {
        return a ? "1" : "0";
    }


    public static void main(String[] args) {
        new XorThing();
    }
}
